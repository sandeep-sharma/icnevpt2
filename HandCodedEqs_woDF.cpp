/*
Developed by Sandeep Sharma and Gerald Knizia, 2016
Copyright (c) 2016, Sandeep Sharma
*/
#include "CxNumpyArray.h"
#include "CxAlgebra.h"
#include "CxMemoryStack.h"
#include "CxPodArray.h"
#include "BlockContract.h"
#include "icpt.h"
#ifdef _OPENMP
#include <omp.h>
#endif

/* track domains throughout the contractions */
extern bool track_contract;
extern char *track_domains;
extern bool in_handcoded;

using ct::FMemoryStack2;

FNdArrayView ViewFromNpy(ct::FArrayNpy &A);

//=============================================================================
void FJobContext::ExecHandCoded_woDF(FMemoryStack2 *Mem){
//-----------------------------------------------------------------------------
    /*!
      Some equations are hand coded here rather then automatically produced by SQA
      It can be for W:eeee or for E3.
    */
//-----------------------------------------------------------------------------
  in_handcoded=true;
  FJobContext Job;
  int nact = m_Domains['a'].nSize;
  int nvir = m_Domains['e'].nSize;
  int ncor = m_Domains['c'].nSize;
  int naux = m_Domains['L'].nSize;

  FScalar one=1.0, two=2.0, zero=0.0, four=4.0, eight=8.0, sixteen=16.0;
  char T='t', N='n';


  // PART1: Handle "W:eee" -----------------------------
  if (Method.Whandcoded_if_zero==0) {

  double timing;
  FArrayOffset nvir2 = nvir*nvir, nact2 = nact*nact, ncor2 = ncor*ncor;
  timespec tclock;
  clock_gettime(CLOCK_MONOTONIC, &tclock);
  timing=-(tclock.tv_sec + 1e-9 * tclock.tv_nsec);
  if (track_contract) printf("**** HAND_W %s",std::string(59,'*').c_str());
  std::cout<<std::flush;

  if((0 == strcmp(MethodName.c_str(), "MRLCC_AAVV"     ) )
  or (0 == strcmp(MethodName.c_str(), "MRLCC_AAVV_CUMU") )){
    void *pBaseOfMemory = Mem[omprank].Alloc(0);
    FScalar *intermed = Mem[0].AllocN(nvir2*nact2, two);
#ifdef _SINGLE_PRECISION
    // Prepare: "intermed" = E2.p
    sgemm_(N, T, nvir2, nact2, nact2, two,
           TND("p:eeaa")->pData, nvir2,
           TND("E2:aaaa")->pData, nact2, zero,
           intermed, nvir2);
    // Ap += "W:e"."intermed"
    sgemm_(T, N, nvir2, nact2, nvir2, one,
           TND("W:e")->pData, nvir2,
           intermed, nvir2, one,
           TND("Ap:eeaa")->pData, nvir2);
#else
    // Prepare: "intermed" = E2.p
    dgemm_(N, T, nact2, nvir2, nact2, two,
           TND("E2:aaaa")->pData, nact2,
           TND("p:eeaa")->pData, nvir2, zero,
           intermed, nact2);

    SplitStackmem(Mem);
#pragma omp parallel
    {
      void *pBaseOfMemoryLocal = Mem[omprank].Alloc(0);
      double *Wslice = Mem[omprank].AllocN(nvir*nvir*nvir, two);
      for (int b=0; b<nvir; b++) {
        if (b%omp_get_num_threads() != omprank) continue;

        // Prepare: "Wslice" = W:(e)eee
        char filename[700];
        sprintf(filename, "int/W:eeee%04d", b);
        FILE* f = fopen(filename, "rb");
        fread(Wslice, sizeof(two), nvir2*nvir, f);

        // The contraction itself:
        // Ap[abcd] W[abef] p[efgh] E2[cdgh]
        dgemm_(N, T, nvir, nact2, nvir2, one,
               Wslice, nvir,
               intermed, nact2, one,
               &TND("Ap:eeaa")->operator()(0,b,0,0), nvir2);
        fclose(f);
      }
      Mem[omprank].Free(pBaseOfMemoryLocal);
    }
    MergeStackmem(Mem);
#endif
    Mem[omprank].Free(pBaseOfMemory);
  }


  if((0 == strcmp(MethodName.c_str(), "MRLCC_CCVV"     ) )
  or (0 == strcmp(MethodName.c_str(), "MRLCC_CCVV_CUMU") )){
//PART1
{
    std::cout<<"      part1_begin\n"<<std::flush;
    void *pBaseOfMemory = Mem[omprank].Alloc(0);

    // Prepare: "intermed" = "p:eecc"^Transpose
    FScalar *intermed = Mem[0].AllocN(nvir2*ncor2, -four);
    for (int i=0; i<ncor; i++)
    for (int j=0; j<ncor; j++)
    for (int a=0; a<nvir; a++)
    for (int b=0; b<nvir; b++)
      intermed[a+b*nvir+i*nvir*nvir+j*nvir*nvir*ncor] = TND("p:eecc")->operator()(a,b,i,j)
                                                   -0.5*TND("p:eecc")->operator()(a,b,j,i);
    //BMstd::cout<<"      post_intermed\n"<<std::flush;

    SplitStackmem(Mem);
#pragma omp parallel
    {
      void *pBaseOfMemoryLocal = Mem[omprank].Alloc(0);
      double *Wslice = Mem[omprank].AllocN(nvir2*nvir, -four);
      for (int b=0; b<nvir; b++) {
        if (b%omp_get_num_threads() != omprank) continue;

        // Prepare: "Wslice" = W:(e)eee
        char filename[700];
        sprintf(filename, "int/W:eeee%04d", b);
        FILE* f = fopen(filename, "rb");
        fread(Wslice, sizeof(-four), nvir2*nvir, f);
        //BMstd::cout<<"("<<omprank<<"/"<<b<<")       CCVV post_dgemm1"<<std::endl<<std::flush;

        // The contraction itself:
        //    Ap[a(b)cd] =  8.0 W[Lae] W[Lf(b)] (p[efcd]-0.5*p[efdc])
        dgemm_(N, N, nvir, ncor2, nvir2, eight,
               Wslice, nvir,
               intermed, nvir2, one,
               &TND("Ap:eecc")->operator()(0,b,0,0), nvir2);
        fclose(f);
        //BMstd::cout<<"("<<omprank<<"/"<<b<<")       CCVV post_dgemm2"<<std::endl<<std::flush;
      }
      Mem[omprank].Free(pBaseOfMemoryLocal);
    }
    MergeStackmem(Mem);
    Mem[omprank].Free(pBaseOfMemory);
    std::cout<<"      part1_end\n"<<std::flush;
}
  }


  if((0 == strcmp(MethodName.c_str(), "MRLCC_ACVV"     ) )
  or (0 == strcmp(MethodName.c_str(), "MRLCC_ACVV_CUMU") )){
    void *pBaseOfMemory = Mem[omprank].Alloc(0);

    // Prepare: "Inter" = p.E1
    // Prepare: "intermed" = p.E1^Transpose
    FScalar *intermed = Mem[0].AllocN(nvir2*ncor*nact, -one);
    for (int p=0; p<nact; p++)
    for (int j=0; j<ncor; j++)
    for (int b=0; b<nvir; b++)
    for (int a=0; a<nvir; a++)
      intermed[a+b*nvir+j*nvir*nvir+p*nvir*nvir*ncor] = TND("p:eeca")->operator()(a,b,j,p)
                                                   -0.5*TND("p:eeca")->operator()(b,a,j,p);
    FNdArrayView Inter; Job.CreateTensorFromShape(Inter, "eeca", "", m_Domains);
    double* InterData = Mem[omprank].AllocN(nvir*nvir*ncor*nact, one);
    for (size_t i=0; i<nvir*nvir*ncor*nact; i++)
      InterData[i] = 0.0;
    Inter.pData = InterData;
    dgemm_(N, N, nvir*nvir*ncor, nact, nact, one,
           intermed, nvir*nvir*ncor,
           TND("E1:aa")->pData, nact, zero,
           Inter.pData, nvir*nvir*ncor);

    SplitStackmem(Mem);
#pragma omp parallel
    {
      void *pBaseOfMemoryLocal = Mem[omprank].Alloc(0);
      double *Wslice = Mem[omprank].AllocN(nvir*nvir*nvir, -one);
      for (int b=0; b<nvir; b++) {
        if (b%omp_get_num_threads() != omprank) continue;

        // Prepare "Wslice" = W:(e)eee
        char filename[700];
        sprintf(filename, "int/W:eeee%04d", b);
        FILE* f = fopen(filename, "rb");
        fread(Wslice, sizeof(-one), nvir2*nvir, f);

        // The contraction itself:
        //   Ap[abcd] +=  2.0 W[Lae] W[Lbf] p[efcg] E1[dg]
        //   Ap[abcd] += -1.0 W[Lae] W[Lbf] p[fecg] E1[dg]
        dgemm_(N, N, nvir, ncor*nact, nvir2, two,
               Wslice, nvir,
               Inter.pData, nvir2, one,
               &TND("Ap:eeca")->operator()(0,b,0,0), nvir2);
        fclose(f);
      }
      Mem[omprank].Free(pBaseOfMemoryLocal);
    }
    MergeStackmem(Mem);
    Mem[omprank].Free(pBaseOfMemory);
  }

  clock_gettime(CLOCK_MONOTONIC, &tclock);
  timing+=tclock.tv_sec + 1e-9 * tclock.tv_nsec;
  if (track_contract) printf("%9.1f\n",timing);
  std::cout<<std::flush;

  } // end_of "Whandcoded_if_zero"


  // PART2: Handle "E3:aaaaaa" -------------------------
  if (Method.E3handcoded_if_zero==0) {

  double timing;
  timespec tclock;
  clock_gettime(CLOCK_MONOTONIC, &tclock);
  timing=-(tclock.tv_sec + 1e-9 * tclock.tv_nsec);
  if (track_contract) printf("**** HAND_E3 %s",std::string(58,'*').c_str());
  std::cout<<std::flush;

  if((0 == strcmp(MethodName.c_str(), "MRLCC_AAVV"     ) )
  or (0 == strcmp(MethodName.c_str(), "MRLCC_AAVV_CUMU") )){
    //// Prepare "W:aeae" and "W:aeea"
    //for (int i=0; i<Method.nTensorDecls; ++i)
    //  if (Method.pTensorDecls[i].pName=="W")
    //    if (Method.pTensorDecls[i].pDomain=="aeae") FillData(i, Mem[0]);
    //for (int i=0; i<Method.nTensorDecls; ++i)
    //  if (Method.pTensorDecls[i].pName=="W")
    //    if (Method.pTensorDecls[i].pDomain=="aeea") FillData(i, Mem[0]);
    //for (int i=0; i<Method.nTensorDecls; ++i)
    //  if (Method.pTensorDecls[i].pName=="W")
    //    if (Method.pTensorDecls[i].pDomain=="aaaa") FillData(i, Mem[0]);
    SplitStackmem(Mem);
#pragma omp parallel
    {
      void *pBaseOfMemoryLocal = Mem[omprank].Alloc(0);

      // Need E3.npy
      char Header0[10], Header[256];
      FILE *f = fopen("int/E3.npy", "rb");
      fread(&Header0[0], 1, 10, f);
      uint16_t HeaderSize = (uint16_t)Header0[8] + (((uint16_t)Header0[9]) << 8);

      // Prepare E3slice and Apslice
      FNdArrayView E3slice; Job.CreateTensorFromShape(E3slice, "aaaa", "", m_Domains);
      FNdArrayView Apslice; Job.CreateTensorFromShape(Apslice, "ee",   "", m_Domains);

      // Prepare pTsN to do:
      //   Ap[ab"cd"] += -4.0 W[efgh] p[abei] E3["cd"higf]
      //   Ap[ab"cd"] +=  4.0 W[eafg] p[bfhi] E3["cd"ihge]
      //   Ap[ab"cd"] +=  4.0 W[eafg] p[bghi] E3["cd"fhie]
      FNdArrayView *pTs1[4] = {&Apslice,TND("W:aeae"),TND("p:eeaa"),&E3slice};
      FNdArrayView *pTs2[4] = {&Apslice,TND("W:aeea"),TND("p:eeaa"),&E3slice};
      FNdArrayView *pTs3[4] = {&Apslice,TND("W:aaaa"),TND("p:eeaa"),&E3slice};
      char* Decl1 = "ab,eafg,bghi,fhie";
      char* Decl2 = "ab,eafg,bfhi,ihge";
      char* Decl3 = "ab,efgh,abei,higf";
      char* Dom1  = "ee=aeae.eeaa.aaaa";
      char* Dom2  = "ee=aeea.eeaa.aaaa";
      char* Dom3  = "ee=aaaa.eeaa.aaaa";

      size_t nact4 = nact*nact*nact*nact, nact2=nact*nact;
      double * E3slicedata = Mem[omprank].AllocN(nact4, one);
      for (size_t cd=0; cd<nact*nact; cd++) {
        if (cd%omp_get_num_threads() != omprank) continue;
        size_t c=cd/nact, d=cd%nact;

        // The contraction itself ("cd"-sliced)
        fseek(f, HeaderSize+10+(d+c*nact)*nact4*sizeof(double), SEEK_SET);
        fread(E3slicedata, sizeof(double), nact4, f);
        Apslice.pData = &TND("Ap:eeaa")->operator()(0,0,c,d) ;
        E3slice.pData = E3slicedata;
        track_domains=Dom1;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl1  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs1,  Decl1 , four, true, Mem[omprank]);
        track_domains=Dom2;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl2  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs2,  Decl2 , four, true, Mem[omprank]);
        track_domains=Dom3;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl3  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs3,  Decl3 ,-four, true, Mem[omprank]);
      }
      fclose(f);
      Mem[omprank].Free(pBaseOfMemoryLocal);
    }
    MergeStackmem(Mem);
    //DeAllocate("W:aaaa", Mem[0]);
    //DeAllocate("W:aeea", Mem[0]);
    //DeAllocate("W:aeae", Mem[0]);
  }


  if((0 == strcmp(MethodName.c_str(), "NEVPT_AAVV"     ) )
  or (0 == strcmp(MethodName.c_str(), "NEVPT_AAVV_CUMU") )){
    SplitStackmem(Mem);
#pragma omp parallel
    {
      void *pBaseOfMemoryLocal = Mem[omprank].Alloc(0);

      // Need E3.npy
      char Header0[10], Header[256];
      FILE *f = fopen("int/E3.npy", "rb");
      fread(&Header0[0], 1, 10, f);
      uint16_t HeaderSize = (uint16_t)Header0[8] + (((uint16_t)Header0[9]) << 8);

      // Prepare E3slice and Apslice
      FNdArrayView E3slice; Job.CreateTensorFromShape(E3slice, "aaaa", "", m_Domains);
      FNdArrayView Apslice; Job.CreateTensorFromShape(Apslice, "ee",   "", m_Domains);

      // Prepare pTsN to do:
      //   Ap[ab"cd"] = -4.0 W[efgh] p[abei] E3["cd"fgih]
      FNdArrayView *pTs1[4] = {&Apslice,TND("W:aaaa"),TND("p:eeaa"),&E3slice};
      char* Decl1 = "ab,efgh,abei,higf";
      char* Dom1  = "ee,aaaa,eeaa,aaaa";

      size_t nact4 = nact*nact*nact*nact, nact2=nact*nact;
      double * E3slicedata = Mem[omprank].AllocN(nact4, one);
      for (size_t cd=0; cd<nact*nact; cd++) {
        if (cd%omp_get_num_threads() != omprank) continue;
        size_t c=cd/nact, d=cd%nact;

        // The contraction itself ("cd"-sliced):
        fseek(f, HeaderSize+10+(d+c*nact)*nact4*sizeof(double), SEEK_SET);
        fread(E3slicedata, sizeof(double), nact4, f);
        Apslice.pData = &TND("Ap:eeaa")->operator()(0,0,c,d) ;
        E3slice.pData = E3slicedata;
        track_domains=Dom1;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl1  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs1,  Decl1 ,-four, true, Mem[omprank]);
      }
      fclose(f);
      Mem[omprank].Free(pBaseOfMemoryLocal);
    }
    MergeStackmem(Mem);
  }


  if((0 == strcmp(MethodName.c_str(), "MRLCC_CAAV"     ) )
  or (0 == strcmp(MethodName.c_str(), "MRLCC_CAAV_CUMU") )){
    SplitStackmem(Mem);
#pragma omp parallel
    {
      void *pBaseOfMemoryLocal = Mem[omprank].Alloc(0);

      // Need E3B.npy and E3C.npy
      char AHeader0[10], AHeader[256];
      FILE *fA = fopen("int/E3B.npy", "rb");
      fread(&AHeader0[0], 1, 10, fA);
      size_t AHeaderSize = (uint16_t)AHeader0[8] + (((uint16_t)AHeader0[9]) << 8);

      char BHeader0[10], BHeader[256];
      FILE *fB = fopen("int/E3C.npy", "rb");
      fread(&BHeader0[0], 1, 10, fB);
      size_t BHeaderSize = (uint16_t)BHeader0[8] + (((uint16_t)BHeader0[9]) << 8);

      // Prepare E3slice1-2, Ap1slice and Ap2slice
      FNdArrayView E3slice1; Job.CreateTensorFromShape(E3slice1, "aaaa", "", m_Domains);
      FNdArrayView E3slice2; Job.CreateTensorFromShape(E3slice2, "aaaa", "", m_Domains);
      FNdArrayView Ap1slice; Job.CreateTensorFromShape(Ap1slice, "ce",   "", m_Domains);
      FNdArrayView Ap2slice; Job.CreateTensorFromShape(Ap2slice, "ce",   "", m_Domains);
      FNdArrayView Ap1;      Job.CreateTensorFromShape(Ap1,      "ceaa", "", m_Domains);
      FNdArrayView Ap2;      Job.CreateTensorFromShape(Ap2,      "ceaa", "", m_Domains);

      // Prepare pTsN to do (sliced):
      //   Ap1[abcd] +=  2.0 W[efgh] E3[cfgbhi] p1[aeid]
      //   Ap1[abcd] += -2.0 W[efgh] E3[cfibhg] p1[aied]
      //   Ap1[abcd] +=  2.0 W[edfg] E3[cehbfi] p1[ahig]
      //   Ap1[abcd] += -1.0 W[edfg] E3[cehbgi] p1[ahif]
      //   Ap1[abcd] += -2.0 W[aefg] E3[cehbgi] p1[fhid]
      //   Ap1[abcd] +=  1.0 W[efga] E3[cfhbgi] p1[ehid]
      //   Ap1[abcd] += -1.0 W[efgh] E3[cfgbhi] p2[aeid]
      //   Ap1[abcd] +=  1.0 W[efgh] E3[cfibhg] p2[aied]
      //   Ap1[abcd] += -1.0 W[edfg] E3[cehbfi] p2[ahig]
      //   Ap1[abcd] += -1.0 W[edfg] E3[cehbig] p2[ahif]
      //   Ap1[abcd] +=  1.0 W[aefg] E3[cehbgi] p2[fhid]
      //   Ap1[abcd] +=  1.0 W[efga] E3[cfhbig] p2[ehid]
      //   Ap2[abcd] += -1.0 W[efgh] E3[cfgbhi] p1[aeid]
      //   Ap2[abcd] +=  1.0 W[efgh] E3[cfibhg] p1[aied]
      //   Ap2[abcd] += -1.0 W[edfg] E3[cehbfi] p1[ahig]
      //   Ap2[abcd] += -1.0 W[edfg] E3[cehgbi] p1[ahif]
      //   Ap2[abcd] +=  1.0 W[aefg] E3[cehbgi] p1[fhid]
      //   Ap2[abcd] +=  1.0 W[efga] E3[cfhgbi] p1[ehid]
      //   Ap2[abcd] += -1.0 W[efgh] E3[cfgihb] p2[aeid]
      //   Ap2[abcd] +=  1.0 W[efgh] E3[cfighb] p2[aied]
      //   Ap2[abcd] += -1.0 W[edfg] E3[cehifb] p2[ahig]
      //   Ap2[abcd] += -1.0 W[edfg] E3[cehgib] p2[ahif]
      //   Ap2[abcd] +=  1.0 W[aefg] E3[cehigb] p2[fhid]
      //   Ap2[abcd] +=  1.0 W[efga] E3[cfhibg] p2[ehid]
      FNdArrayView *pTs1[4]  = {&Ap1slice,TND("W:aeae"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs2[4]  = {&Ap1slice,TND("W:aeea"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs3[4]  = {&Ap1slice,TND("W:caca"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs4[4]  = {&Ap1slice,TND("W:caac"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs5[4]  = {&Ap1slice,TND("W:aaaa"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs6[4]  = {&Ap1slice,TND("W:aaaa"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs7[4]  = {&Ap1slice,TND("W:aeae"),&E3slice1,TND("p2:caae")};
      FNdArrayView *pTs8[4]  = {&Ap1slice,TND("W:aeea"),&E3slice1,TND("p2:caae")};
      FNdArrayView *pTs9[4]  = {&Ap1slice,TND("W:caca"),&E3slice1,TND("p2:caae")};
      FNdArrayView *pTs10[4] = {&Ap1slice,TND("W:caac"),&E3slice1,TND("p2:caae")};
      FNdArrayView *pTs11[4] = {&Ap1slice,TND("W:aaaa"),&E3slice1,TND("p2:caae")};
      FNdArrayView *pTs12[4] = {&Ap1slice,TND("W:aaaa"),&E3slice1,TND("p2:caae")};
      FNdArrayView *pTs13[4] = {&Ap2slice,TND("W:caca"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs14[4] = {&Ap2slice,TND("W:aaaa"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs15[4] = {&Ap2slice,TND("W:aaaa"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs16[4] = {&Ap2slice,TND("W:aeae"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs17[4] = {&Ap2slice,TND("W:caac"),&E3slice2,TND("p1:caae")};
      FNdArrayView *pTs18[4] = {&Ap2slice,TND("W:aeea"),&E3slice2,TND("p1:caae")};
      FNdArrayView *pTs19[4] = {&Ap2slice,TND("W:aeae"),&E3slice2,TND("p2:caae")};
      FNdArrayView *pTs20[4] = {&Ap2slice,TND("W:aeea"),&E3slice2,TND("p2:caae")};
      FNdArrayView *pTs21[4] = {&Ap2slice,TND("W:caca"),&E3slice2,TND("p2:caae")};
      FNdArrayView *pTs22[4] = {&Ap2slice,TND("W:aaaa"),&E3slice2,TND("p2:caae")};
      FNdArrayView *pTs23[4] = {&Ap2slice,TND("W:caac"),&E3slice2,TND("p2:caae")};
      FNdArrayView *pTs24[4] = {&Ap2slice,TND("W:aaaa"),&E3slice2,TND("p2:caae")};
      char* Decl1  = "JB,aAbB,QPba,JPQA";
      char* Decl2  = "JB,aBAb,QPba,JPQA";
      char* Decl3  = "JB,IaJb,QPba,IPQB";
      char* Decl4  = "JB,IabJ,QPba,IPQB";
      char* Decl5  = "JB,Pabc,caQb,JPQB";
      char* Decl6  = "JB,Qabc,bPca,JPQB";
      char* Decl7  = "JB,aAbB,QPba,JPQA";
      char* Decl8  = "JB,aBAb,bPQa,JPQA";
      char* Decl9  = "JB,IaJb,QPba,IPQB";
      char* Decl10 = "JB,IabJ,bPQa,IPQB";
      char* Decl11 = "JB,Pabc,caQb,JPQB";
      char* Decl12 = "JB,Qabc,bPca,JPQB";
      char* Decl13 = "JB,IaJb,QPba,IPQB";
      char* Decl14 = "JB,Pabc,caQb,JPQB";
      char* Decl15 = "JB,Qabc,bPca,JPQB";
      char* Decl16 = "JB,aAbB,QPba,JPQA";
      char* Decl17 = "JB,IabJ,aQPb,IPQB";
      char* Decl18 = "JB,aBAb,aQPb,JPQA";
      char* Decl19 = "JB,aAbB,PbaQ,JPQA";
      char* Decl20 = "JB,aBAb,PQab,JPQA";
      char* Decl21 = "JB,IaJb,PbaQ,IPQB";
      char* Decl22 = "JB,Qabc,Pcab,JPQB";
      char* Decl23 = "JB,IabJ,abPQ,IPQB";
      char* Decl24 = "JB,Pabc,bcaQ,JPQB";
      char* Dom1   = "ce,aeae,aaaa,caae";
      char* Dom2   = "ce,aeea,aaaa,caae";
      char* Dom3   = "ce,caca,aaaa,caae";
      char* Dom4   = "ce,caac,aaaa,caae";
      char* Dom5   = "ce,aaaa,aaaa,caae";
      char* Dom6   = "ce,aaaa,aaaa,caae";
      char* Dom7   = "ce,aeae,aaaa,caae";
      char* Dom8   = "ce,aeea,aaaa,caae";
      char* Dom9   = "ce,caca,aaaa,caae";
      char* Dom10  = "ce,caac,aaaa,caae";
      char* Dom11  = "ce,aaaa,aaaa,caae";
      char* Dom12  = "ce,aaaa,aaaa,caae";
      char* Dom13  = "ce,caca,aaaa,caae";
      char* Dom14  = "ce,aaaa,aaaa,caae";
      char* Dom15  = "ce,aaaa,aaaa,caae";
      char* Dom16  = "ce,aeae,aaaa,caae";
      char* Dom17  = "ce,caac,aaaa,caae";
      char* Dom18  = "ce,aeea,aaaa,caae";
      char* Dom19  = "ce,aeae,aaaa,caae";
      char* Dom20  = "ce,aeea,aaaa,caae";
      char* Dom21  = "ce,caca,aaaa,caae";
      char* Dom22  = "ce,aaaa,aaaa,caae";
      char* Dom23  = "ce,caac,aaaa,caae";
      char* Dom24  = "ce,aaaa,aaaa,caae";

      double* E3slicedata = Mem[omprank].AllocN(nact*nact*nact*nact, one);
      double* Ap1data     = Mem[omprank].AllocN(ncor*nvir*nact*nact, one);
      double* Ap2data     = Mem[omprank].AllocN(ncor*nvir*nact*nact, one);
      for (size_t i=0; i<nact*nact*ncor*nvir; i++) Ap1data[i] = 0.0;
      for (size_t i=0; i<nact*nact*ncor*nvir; i++) Ap2data[i] = 0.0;
      Ap1.pData = Ap1data;
      Ap2.pData = Ap2data;
      for (size_t RS=0; RS<nact*nact; RS++){
        if (RS%omp_get_num_threads() != omprank) continue;
        size_t R = (RS)/nact;
        size_t S = (RS)%nact;

  		  // The contraction itself ("RS"-sliced)
        // E3B
        fseek(fA, AHeaderSize+10+(R+S*nact)*nact*nact*nact*nact*sizeof(double), SEEK_SET);
        fread(E3slicedata, sizeof(double), nact*nact*nact*nact, fA);
        Ap1slice.pData = &Ap1.operator()(0,0,R,S);
        Ap2slice.pData = &Ap2.operator()(0,0,R,S);
        E3slice1.pData = E3slicedata;
        track_domains=Dom1;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl1  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs1,  Decl1 , two, true, Mem[omprank]);
        track_domains=Dom2;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl2  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs2,  Decl2 ,-one, true, Mem[omprank]);
        track_domains=Dom3;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl3  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs3,  Decl3 ,-two, true, Mem[omprank]);
        track_domains=Dom4;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl4  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs4,  Decl4 , one, true, Mem[omprank]);
        track_domains=Dom5;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl5  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs5,  Decl5 , two, true, Mem[omprank]);
        track_domains=Dom6;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl6  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs6,  Decl6 ,-two, true, Mem[omprank]);
        track_domains=Dom7;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl7  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs7,  Decl7 ,-one, true, Mem[omprank]);
        track_domains=Dom8;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl8  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs8,  Decl8 ,-one, true, Mem[omprank]);
        track_domains=Dom9;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl9  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs9,  Decl9 , one, true, Mem[omprank]);
        track_domains=Dom10;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl10 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs10, Decl10, one, true, Mem[omprank]);
        track_domains=Dom11;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl11 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs11, Decl11,-one, true, Mem[omprank]);
        track_domains=Dom12;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl12 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs12, Decl12, one, true, Mem[omprank]);
        track_domains=Dom13;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl13 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs13, Decl13, one, true, Mem[omprank]);
        track_domains=Dom14;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl14 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs14, Decl14,-one, true, Mem[omprank]);
        track_domains=Dom15;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl15 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs15, Decl15, one, true, Mem[omprank]);
        track_domains=Dom16;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl16 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs16, Decl16,-one, true, Mem[omprank]);

        // E3C
        fseek(fB, BHeaderSize+10+(R+S*nact)*nact*nact*nact*nact*sizeof(double), SEEK_SET);
        fread(E3slicedata, sizeof(double), nact*nact*nact*nact, fB);
        E3slice2.pData = E3slicedata;
        track_domains=Dom17;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl17 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs17, Decl17, one, true, Mem[omprank]);
        track_domains=Dom18;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl18 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs18, Decl18,-one, true, Mem[omprank]);
        track_domains=Dom19;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl19 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs19, Decl19,-one, true, Mem[omprank]);
        track_domains=Dom20;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl20 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs20, Decl20,-one, true, Mem[omprank]);
        track_domains=Dom21;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl21 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs21, Decl21, one, true, Mem[omprank]);
        track_domains=Dom22;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl22 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs22, Decl22, one, true, Mem[omprank]);
        track_domains=Dom23;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl23 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs23, Decl23, one, true, Mem[omprank]);
        track_domains=Dom24;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl24 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs24, Decl24,-one, true, Mem[omprank]);
      }

      // Accumulation into Ap1 and Ap2
#pragma omp critical
      {
        for (size_t i=0; i<ncor; i++)
        for (size_t j=0; j<nact; j++)
        for (size_t k=0; k<nact; k++)
        for (size_t l=0; l<nvir; l++) {
          TND("Ap1:caae")->operator()(i,j,k,l) += Ap1.operator()(i,l,j,k);
          TND("Ap2:caae")->operator()(i,j,k,l) += Ap2.operator()(i,l,j,k);
        }
      }
      fclose(fA);
      fclose(fB);
      Mem[omprank].Free(pBaseOfMemoryLocal);
    }
    MergeStackmem(Mem);
  }


  if((0 == strcmp(MethodName.c_str(), "NEVPT_CAAV"     ) )
  or (0 == strcmp(MethodName.c_str(), "NEVPT_CAAV_CUMU") )){
    SplitStackmem(Mem);
#pragma omp parallel
    {
      void *pBaseOfMemoryLocal = Mem[omprank].Alloc(0);

      // Need E3B.npy and E3C.npy
      char AHeader0[10], AHeader[256];
      FILE *fA = fopen("int/E3B.npy", "rb");
      fread(&AHeader0[0], 1, 10, fA);
      size_t AHeaderSize = (uint16_t)AHeader0[8] + (((uint16_t)AHeader0[9]) << 8);

      char BHeader0[10], BHeader[256];
      FILE *fB = fopen("int/E3C.npy", "rb");
      fread(&BHeader0[0], 1, 10, fB);
      size_t BHeaderSize = (uint16_t)BHeader0[8] + (((uint16_t)BHeader0[9]) << 8);

      // Prepare E3slice1-2, Ap1slice and Ap2slice
      FNdArrayView E3slice1; Job.CreateTensorFromShape(E3slice1, "aaaa", "", m_Domains);
      FNdArrayView E3slice2; Job.CreateTensorFromShape(E3slice2, "aaaa", "", m_Domains);
      FNdArrayView Ap1slice; Job.CreateTensorFromShape(Ap1slice, "ce",   "", m_Domains);
      FNdArrayView Ap2slice; Job.CreateTensorFromShape(Ap2slice, "ce",   "", m_Domains);
      FNdArrayView Ap1;      Job.CreateTensorFromShape(Ap1,      "ceaa", "", m_Domains);
      FNdArrayView Ap2;      Job.CreateTensorFromShape(Ap2,      "ceaa", "", m_Domains);

      // Prepare pTsN to do ("RS"-sliced):
      //  Ap1[J"RS"B] +=  2.0 W[Pabc] E3[SabRcQ] p1[JPQB]
      //  Ap1[J"RS"B] += -2.0 W[Qabc] E3[PSabRc] p1[JPQB]
      //  Ap1[J"RS"B] += -1.0 W[Pabc] E3[SabRcQ] p2[JPQB]
      //  Ap1[J"RS"B] +=  1.0 W[Qabc] E3[PSabRc] p2[JPQB]
      //  Ap2[J"RS"B] += -1.0 W[Pabc] E3[SabRcQ] p1[JPQB]
      //  Ap2[J"RS"B] +=  1.0 W[Qabc] E3[PSabRc] p1[JPQB]
      //  Ap2[J"RS"B] += -1.0 W[Pabc] E3[SabQcR] p2[JPQB]
      //  Ap2[J"RS"B] +=  1.0 W[Qabc] E3[PSaRbc] p2[JPQB]
      FNdArrayView *pTs1[4] = {&Ap1slice,TND("W:aaaa"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs2[4] = {&Ap1slice,TND("W:aaaa"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs3[4] = {&Ap1slice,TND("W:aaaa"),&E3slice1,TND("p2:caae")};
      FNdArrayView *pTs4[4] = {&Ap1slice,TND("W:aaaa"),&E3slice1,TND("p2:caae")};
      FNdArrayView *pTs5[4] = {&Ap2slice,TND("W:aaaa"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs6[4] = {&Ap2slice,TND("W:aaaa"),&E3slice1,TND("p1:caae")};
      FNdArrayView *pTs7[4] = {&Ap2slice,TND("W:aaaa"),&E3slice2,TND("p2:caae")};
      FNdArrayView *pTs8[4] = {&Ap2slice,TND("W:aaaa"),&E3slice2,TND("p2:caae")};
      char* Decl1 = "JB,Pabc,caQb,JPQB";
      char* Decl2 = "JB,Qabc,bPca,JPQB";
      char* Decl3 = "JB,Pabc,caQb,JPQB";
      char* Decl4 = "JB,Qabc,bPca,JPQB";
      char* Decl5 = "JB,Pabc,caQb,JPQB";
      char* Decl6 = "JB,Qabc,bPca,JPQB";
      char* Decl7 = "JB,Qabc,Pcab,JPQB";
      char* Decl8 = "JB,Pabc,bcaQ,JPQB";
      char* Dom1  = "ce,aaaa,aaaa,caae";
      char* Dom2  = "ce,aaaa,aaaa,caae";
      char* Dom3  = "ce,aaaa,aaaa,caae";
      char* Dom4  = "ce,aaaa,aaaa,caae";
      char* Dom5  = "ce,aaaa,aaaa,caae";
      char* Dom6  = "ce,aaaa,aaaa,caae";
      char* Dom7  = "ce,aaaa,aaaa,caae";
      char* Dom8  = "ce,aaaa,aaaa,caae";

      double* E3slicedata = Mem[omprank].AllocN(nact*nact*nact*nact, one);
      double* Ap1data     = Mem[omprank].AllocN(ncor*nvir*nact*nact, one);
      double* Ap2data     = Mem[omprank].AllocN(ncor*nvir*nact*nact, one);
      for (size_t i=0; i<nact*nact*ncor*nvir; i++) Ap1data[i] = 0.0;
      for (size_t i=0; i<nact*nact*ncor*nvir; i++) Ap2data[i] = 0.0;
      Ap1.pData = Ap1data;
      Ap2.pData = Ap2data;
      for (size_t RS=0; RS<nact*nact; RS++){
        if (RS%omp_get_num_threads() != omprank) continue;
        size_t R = (RS)/nact;
        size_t S = (RS)%nact;

        // The contraction itself ("RS"-sliced):
        // E3B
        fseek(fA, AHeaderSize+10+(R+S*nact)*nact*nact*nact*nact*sizeof(double), SEEK_SET);
        fread(E3slicedata, sizeof(double), nact*nact*nact*nact, fA);
        Ap1slice.pData = &Ap1.operator()(0,0,R,S);
        Ap2slice.pData = &Ap2.operator()(0,0,R,S);
        E3slice1.pData = E3slicedata;
        track_domains=Dom1;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl1  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs1,  Decl1 , two, true, Mem[omprank]);
        track_domains=Dom2;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl2  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs2,  Decl2 ,-two, true, Mem[omprank]);
        track_domains=Dom3;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl3  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs3,  Decl3 ,-one, true, Mem[omprank]);
        track_domains=Dom4;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl4  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs4,  Decl4 , one, true, Mem[omprank]);
        track_domains=Dom5;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl5  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs5,  Decl5 ,-one, true, Mem[omprank]);
        track_domains=Dom6;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl6  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs6,  Decl6 , one, true, Mem[omprank]);

        // E3C
        fseek(fB, BHeaderSize+10+(R+S*nact)*nact*nact*nact*nact*sizeof(double), SEEK_SET);
        fread(E3slicedata, sizeof(double), nact*nact*nact*nact, fB);
        E3slice2.pData = E3slicedata;
        track_domains=Dom7;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl7  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs7,  Decl7 , one, true, Mem[omprank]);
        track_domains=Dom8;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl8  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs8,  Decl8 ,-one, true, Mem[omprank]);
      }

      // Accumulation into Ap1 and Ap2
#pragma omp critical
      {
        for (size_t i=0; i<ncor; i++)
        for (size_t j=0; j<nact; j++)
        for (size_t k=0; k<nact; k++)
        for (size_t l=0; l<nvir; l++) {
          TND("Ap1:caae")->operator()(i,j,k,l) += Ap1.operator()(i,l,j,k);
          TND("Ap2:caae")->operator()(i,j,k,l) += Ap2.operator()(i,l,j,k);
        }
      }
      fclose(fA);
      fclose(fB);
      Mem[omprank].Free(pBaseOfMemoryLocal);
    }
    MergeStackmem(Mem);
  }


  if((0 == strcmp(MethodName.c_str(), "MRLCC_CCAA"     ) )
  or (0 == strcmp(MethodName.c_str(), "MRLCC_CCAA_CUMU") )){
    SplitStackmem(Mem);
#pragma omp parallel
    {
      void *pBaseOfMemoryLocal = Mem[omprank].Alloc(0);

      // Need E3.npy
      char Header0[10], Header[256];
      FILE *f = fopen("int/E3.npy", "rb");
      fread(&Header0[0], 1, 10, f);
      uint16_t HeaderSize = (uint16_t)Header0[8] + (((uint16_t)Header0[9]) << 8);

      // Prepare E3slice, Apslice1-6 and Wslice1-4
      FNdArrayView E3slice;  Job.CreateTensorFromShape(E3slice,  "aaa",  "", m_Domains);
      FNdArrayView Apslice1; Job.CreateTensorFromShape(Apslice1, "cc",   "", m_Domains);
      FNdArrayView Apslice2; Job.CreateTensorFromShape(Apslice2, "cc",   "", m_Domains);
      FNdArrayView Apslice3; Job.CreateTensorFromShape(Apslice3, "cc",   "", m_Domains);
      FNdArrayView Apslice4; Job.CreateTensorFromShape(Apslice4, "cc",   "", m_Domains);
      FNdArrayView Apslice5; Job.CreateTensorFromShape(Apslice5, "cc",   "", m_Domains);
      FNdArrayView Apslice6; Job.CreateTensorFromShape(Apslice6, "cc",   "", m_Domains);
      FNdArrayView Wslice1;  Job.CreateTensorFromShape(Wslice1,  "cac",  "", m_Domains);
      FNdArrayView Wslice2;  Job.CreateTensorFromShape(Wslice2,  "aaa",  "", m_Domains);
      FNdArrayView Wslice3;  Job.CreateTensorFromShape(Wslice3,  "cca",  "", m_Domains);
      FNdArrayView Wslice4;  Job.CreateTensorFromShape(Wslice4,  "cca",  "", m_Domains);
      FNdArrayView Ap;       Job.CreateTensorFromShape(Ap,       "ccaa", "", m_Domains);

      // Prepare pTsN to do:
      //  Ap[abcd] +=  1.0 W[efgh] p[abei] E3[fgihcd]
      //  Ap[abcd] +=  1.0 W[efgh] p[abie] E3[fgihdc]
      //  Ap[abcd] +=  1.0 W[efgh] p[baei] E3[fgihdc]
      //  Ap[abcd] +=  1.0 W[efgh] p[baie] E3[fgihcd]
      //  Ap[abcd] += -1.0 W[aefg] p[bfhi] E3[ehigdc]
      //  Ap[abcd] += -1.0 W[aefg] p[fbhi] E3[ehigcd]
      //  Ap[abcd] += -1.0 W[befg] p[afhi] E3[ehigcd]
      //  Ap[abcd] += -1.0 W[befg] p[fahi] E3[ehigdc]
      //  Ap[abcd] += -1.0 W[efga] p[behi] E3[fhicdg]
      //  Ap[abcd] += -1.0 W[efga] p[ebhi] E3[fhicgd]
      //  Ap[abcd] += -1.0 W[efgb] p[aehi] E3[fhidcg]
      //  Ap[abcd] += -1.0 W[efgb] p[eahi] E3[fhidgc]
      FNdArrayView *pTs1[4]  = {&Apslice1,&Wslice1,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs2[4]  = {&Apslice2,&Wslice1,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs3[4]  = {&Apslice2,&Wslice1,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs4[4]  = {&Apslice1,&Wslice1,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs5[4]  = {&Apslice3,&Wslice3,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs6[4]  = {&Apslice4,&Wslice3,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs7[4]  = {&Apslice5,&Wslice4,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs8[4]  = {&Apslice6,&Wslice4,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs9[4]  = {&Apslice5,&Wslice2,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs10[4] = {&Apslice6,&Wslice2,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs11[4] = {&Apslice6,&Wslice2,&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs12[4] = {&Apslice5,&Wslice2,&E3slice,TND("p:ccaa")};
      char* Decl1  = "KL,IaK,PQa,ILPQ";
      char* Decl2  = "KL,IaL,PQa,IKPQ";
      char* Decl3  = "KL,JaK,PQa,LJPQ";
      char* Decl4  = "KL,JaL,PQa,KJPQ";
      char* Decl5  = "KL,KIa,PQa,ILPQ";
      char* Decl6  = "KL,LIa,PQa,IKPQ";
      char* Decl7  = "KL,KJa,PQa,LJPQ";
      char* Decl8  = "KL,LJa,PQa,KJPQ";
      char* Decl9  = "KL,Pab,Qab,KLPQ";
      char* Decl10 = "KL,Pab,Qab,LKPQ";
      char* Decl11 = "KL,Qab,Pab,KLPQ";
      char* Decl12 = "KL,Qab,Pab,LKPQ";
      char* Dom1   = "cc,cac,aaa,ccaa";
      char* Dom2   = "cc,cac,aaa,ccaa";
      char* Dom3   = "cc,cac,aaa,ccaa";
      char* Dom4   = "cc,cac,aaa,ccaa";
      char* Dom5   = "cc,cca,aaa,ccaa";
      char* Dom6   = "cc,cca,aaa,ccaa";
      char* Dom7   = "cc,cca,aaa,ccaa";
      char* Dom8   = "cc,cca,aaa,ccaa";
      char* Dom9   = "cc,aaa,aaa,ccaa";
      char* Dom10  = "cc,aaa,aaa,ccaa";
      char* Dom11  = "cc,aaa,aaa,ccaa";
      char* Dom12  = "cc,aaa,aaa,ccaa";

      double* E3slicedata = Mem[omprank].AllocN(nact*nact*nact,      one);
      double* Apdata      = Mem[omprank].AllocN(ncor*ncor*nact*nact, one);
      for (size_t i=0; i<ncor*ncor*nact*nact; i++) Apdata[i] = 0.0;
      Ap.pData = Apdata;
      for (size_t RSB=0; RSB<nact*nact*nact; RSB++){
        if (RSB%omp_get_num_threads() != omprank) continue;
        size_t R = RSB/nact/nact;
        size_t S = (RSB%(nact*nact))/nact;
        size_t b = (RSB%(nact*nact))%nact;

        // The contraction itself ("RS"-sliced):
        fseek(f, HeaderSize+10+(R+S*nact+b*nact*nact)*nact*nact*nact*sizeof(double), SEEK_SET);
        fread(E3slicedata, sizeof(double), nact*nact*nact, f);
        Apslice1.pData=&Ap.operator()(0,0,R,S);
        Apslice2.pData=&Ap.operator()(0,0,S,R);
        Apslice3.pData=&Ap.operator()(0,0,b,S);
        Apslice4.pData=&Ap.operator()(0,0,S,b);
        Apslice5.pData=&Ap.operator()(0,0,b,R);
        Apslice6.pData=&Ap.operator()(0,0,R,b);
        Wslice1.pData =&TND("W:caca") ->operator()(0,0,0,b);
        Wslice2.pData =&TND("W:aaaa") ->operator()(0,0,0,S);
        Wslice3.pData =&TND("W:ccaa") ->operator()(0,0,0,R);
        Wslice4.pData =&TND("W:ccaa") ->operator()(0,0,0,S);
        E3slice.pData =E3slicedata;
        track_domains=Dom1;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl1  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs1,  Decl1 ,-one, true, Mem[omprank]);
        track_domains=Dom2;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl2  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs2,  Decl2 ,-one, true, Mem[omprank]);
        track_domains=Dom3;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl3  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs3,  Decl3 ,-one, true, Mem[omprank]);
        track_domains=Dom4;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl4  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs4,  Decl4 ,-one, true, Mem[omprank]);
        track_domains=Dom5;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl5  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs5,  Decl5 ,-one, true, Mem[omprank]);
        track_domains=Dom6;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl6  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs6,  Decl6 ,-one, true, Mem[omprank]);
        track_domains=Dom7;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl7  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs7,  Decl7 ,-one, true, Mem[omprank]);
        track_domains=Dom8;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl8  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs8,  Decl8 ,-one, true, Mem[omprank]);
        track_domains=Dom9;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl9  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs9,  Decl9 , one, true, Mem[omprank]);
        track_domains=Dom10;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl10 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs10, Decl10, one, true, Mem[omprank]);
        track_domains=Dom11;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl11 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs11, Decl11, one, true, Mem[omprank]);
        track_domains=Dom12;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl12 ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs12, Decl12, one, true, Mem[omprank]);
      }

      // Accumulation into Ap
#pragma omp critical
      {
        for (size_t i=0; i<ncor; i++)
        for (size_t j=0; j<ncor; j++)
        for (size_t p=0; p<nact; p++)
        for (size_t q=0; q<nact; q++)
          TND("Ap:ccaa")->operator()(i,j,p,q) += Ap.operator()(i,j,p,q);
      }
      fclose(f);
      Mem[omprank].Free(pBaseOfMemoryLocal);
    }
    MergeStackmem(Mem);
  }


  if((0 == strcmp(MethodName.c_str(), "NEVPT_CCAA"     ) )
  or (0 == strcmp(MethodName.c_str(), "NEVPT_CCAA_CUMU") )){
    SplitStackmem(Mem);
#pragma omp parallel
    {
      void *pBaseOfMemoryLocal = Mem[omprank].Alloc(0);

      // Need E3.npy
      char Header0[10], Header[256];
      FILE *f = fopen("int/E3.npy", "rb");
      fread(&Header0[0], 1, 10, f);
      size_t HeaderSize = (uint16_t)Header0[8] + (((uint16_t)Header0[9]) << 8);

      // Prepare E3slice and Apslice1-2
      FNdArrayView E3slice;  Job.CreateTensorFromShape(E3slice,  "aaaa", "", m_Domains);
      FNdArrayView Apslice1; Job.CreateTensorFromShape(Apslice1, "cc",   "", m_Domains);
      FNdArrayView Apslice2; Job.CreateTensorFromShape(Apslice2, "cc",   "", m_Domains);

      // Prepare pTsN:
      //  Ap[KL"RS"] += 1.0 W[Pabc] E3[Qab"S"c"R"] p[KLPQ]
      //  Ap[KL"RS"] += 1.0 W[Pabc] E3[Qab"R"c"S"] p[LKPQ]
      //  Ap[KL"RS"] += 1.0 W[Qabc] E3[Pab"R"c"S"] p[KLPQ]
      //  Ap[KL"RS"] += 1.0 W[Qabc] E3[Pab"S"c"R"] p[LKPQ]
      FNdArrayView *pTs1[4] = {&Apslice1,TND("W:aaaa"),&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs2[4] = {&Apslice2,TND("W:aaaa"),&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs3[4] = {&Apslice2,TND("W:aaaa"),&E3slice,TND("p:ccaa")};
      FNdArrayView *pTs4[4] = {&Apslice1,TND("W:aaaa"),&E3slice,TND("p:ccaa")};
      char* Decl1 = "KL,Pabc,aQbc,KLPQ";
      char* Decl2 = "KL,Pabc,aQbc,LKPQ";
      char* Decl3 = "KL,Qabc,aPbc,KLPQ";
      char* Decl4 = "KL,Qabc,aPbc,LKPQ";
      char* Dom1  = "cc,aaaa,aaaa,ccaa";
      char* Dom2  = "cc,aaaa,aaaa,ccaa";
      char* Dom3  = "cc,aaaa,aaaa,ccaa";
      char* Dom4  = "cc,aaaa,aaaa,ccaa";

      double* E3slicedata = Mem[omprank].AllocN(nact*nact*nact*nact, one);
      for (size_t RS=0; RS<nact*nact; RS++){
        if (RS%omp_get_num_threads() != omprank) continue;
        size_t R = (RS)/nact;
        size_t S = (RS)%nact;

        // The contraction itself ("RS"-sliced):
        fseek(f, HeaderSize+10+(S+R*nact)*nact*nact*nact*nact*sizeof(double), SEEK_SET);
        fread(E3slicedata, sizeof(double), nact*nact*nact*nact, f);
        Apslice1.pData=&TND("Ap:ccaa")->operator()(0,0,R,S);
        Apslice2.pData=&TND("Ap:ccaa")->operator()(0,0,S,R);
        E3slice.pData =E3slicedata;
        track_domains=Dom1;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl1  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs1,  Decl1 , one, true, Mem[omprank]);
        track_domains=Dom2;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl2  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs2,  Decl2 , one, true, Mem[omprank]);
        track_domains=Dom3;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl3  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs3,  Decl3 , one, true, Mem[omprank]);
        track_domains=Dom4;
        //if (track_contract) printf("*HH* %-20s %-20s %s\n",Decl4  ,track_domains,std::string(29,'*').c_str());
        ContractN(pTs4,  Decl4 , one, true, Mem[omprank]);
      }
      fclose(f);
      Mem[omprank].Free(pBaseOfMemoryLocal);
    }
    MergeStackmem(Mem);
  }

  clock_gettime(CLOCK_MONOTONIC, &tclock);
  timing+=tclock.tv_sec + 1e-9 * tclock.tv_nsec;
  if (track_contract) printf("%9.1f\n",timing);
  std::cout<<std::flush;

  } // end_of "E3handcoded_if_zero"

  //zero out all intermediates
  for (uint i = 0; i != Method.nTensorDecls; ++i) {
    FTensorDecl const
      &Decl = Method.pTensorDecls[i];
    if (Decl.Usage == USAGE_Intermediate)
      memset(m_Tensors[i].pData, 0, m_Tensors[i].nValues() *sizeof(m_Tensors[i].pData[0]));
  }
  //BMstd::cout<<"   ExecHandCoded after _ExecHandCoded\n"<<std::flush;

} // end FJobContext::ExecHandCoded
