#!/usr/bin/env python
#
# Author: Sandeep Sharma <sanshar@gmail.com>
#         Sheng Guo <shengg@princeton.edu>
#         Qiming Sun <osirpt.sun@gmail.com>
#

import ctypes
import os, sys
import time
import numpy
import pyscf.lib

libE3unpack = pyscf.lib.load_library('libicmpspt')
#libE3unpack = cdll.LoadLibrary("libE3unpack.so")

'''
DMRG solver for CASSCF.

DMRGCI.kernel function has a kerword fciRestart to control the Block solver
restart from previous calculation
'''

try:
    from pyscf.dmrgscf import settings
except ImportError:
    msg = '''settings.py not found.  Please create %s
''' % os.path.join(os.path.dirname(__file__), 'settings.py')
    sys.stderr.write(msg)
    raise ImportError


class DMRGCI(object):
    def __init__(self, maxM=None, tol=None, num_thrds=1, memory=None):
        self.outputlevel = 2

        self.executable = settings.BLOCKEXE
        self.scratchDirectory = settings.BLOCKSCRATCHDIR
        self.mpiprefix = settings.MPIPREFIX
        self.memory = memory

        self.integralFile = "FCIDUMP"
        self.configFile = "dmrg.conf"
        self.outputFile = "dmrg.out"
        self.twopdm = True
        self.maxIter = 20
        self.approx_maxIter = 4
        self.twodot_to_onedot = 15
        self.dmrg_switch_tol = 1e-3
        self.nroots = 1
        self.nevpt_state_num = 0
        self.weights = []
        self.wfnsym = 1

        if tol is None:
            self.tol = 1e-8
        else:
            self.tol = tol/10
        if maxM is None:
            self.maxM = 1000
        else:
            self.maxM = maxM

        self.num_thrds= num_thrds
        self.startM =  None
        self.restart = False
        self.force_restart = False
        self.nonspinAdapted = False
        self.mps_nevpt = False
        self.scheduleSweeps = []
        self.scheduleMaxMs  = []
        self.scheduleTols   = []
        self.scheduleNoises = []

        self.generate_schedule()
        self.has_threepdm = False
        self.has_nevpt = False
        self.onlywriteIntegral = False
        self.extraline = []
        self.dmrg_switch_tol = 1.0e-3

        self._keys = set(self.__dict__.keys())


    def generate_schedule(self):

        if self.startM == None:
            self.startM = 200
        if len(self.scheduleSweeps) == 0:
            startM = self.startM
            N_sweep = 0
            if self.restart or self.force_restart :
                Tol = self.tol/10.0
            else:
                Tol = 1.0e-4
            Noise = Tol
            while startM <= self.maxM:
                self.scheduleSweeps.append(N_sweep)
                N_sweep +=4
                self.scheduleMaxMs.append(startM)
                startM *=2
                self.scheduleTols.append(Tol) 
                self.scheduleNoises.append(Noise) 
            while Tol > self.tol:
                self.scheduleSweeps.append(N_sweep)
                N_sweep +=2
                self.scheduleMaxMs.append(self.maxM)
                self.scheduleTols.append(Tol)
                Tol /=10.0
                Noise /=10.0
                self.scheduleNoises.append(Noise)
            self.scheduleSweeps.append(N_sweep)
            N_sweep +=2
            self.scheduleMaxMs.append(self.maxM)
            self.scheduleTols.append(self.tol)
            self.scheduleNoises.append(0.0)
            self.twodot_to_onedot = N_sweep+2
            self.maxIter = self.twodot_to_onedot+20


    def dump_flags(self, verbose=None):
        if verbose is None:
            verbose = self.verbose
        log = logger.Logger(self.stdout, verbose)
        log.info('******** Block flags ********')
        log.info('scratchDirectory = %s', self.scratchDirectory)
        log.info('integralFile = %s', self.integralFile)
        log.info('configFile = %s', self.configFile)
        log.info('outputFile = %s', self.outputFile)
        log.info('maxIter = %d', self.maxIter)
        log.info('scheduleSweeps = %s', str(self.scheduleSweeps))
        log.info('scheduleMaxMs = %s', str(self.scheduleMaxMs))
        log.info('scheduleTols = %s', str(self.scheduleTols))
        log.info('scheduleNoises = %s', str(self.scheduleNoises))
        log.info('twodot_to_onedot = %d', self.twodot_to_onedot)
        log.info('tol = %g', self.tol)
        log.info('maxM = %d', self.maxM)
        log.info('fullrestart = %s', str(self.restart or self.force_restart))
        log.info('dmrg switch tol =%s', self.dmrg_switch_tol)
        log.info('wfnsym = %s', self.wfnsym)
        log.info('num_thrds = %d', self.num_thrds)

    def make_rdm1(self, state, norb, nelec, link_index=None, **kwargs):
        nelectrons = 0
        if isinstance(nelec, (int, numpy.integer)):
            nelectrons = nelec
        else:
            nelectrons = nelec[0]+nelec[1]

        import os
        f = open(os.path.join('%s/%s/'%(self.scratchDirectory,"node0"), "spatial_twopdm.%d.%d.txt" %(state, state)), 'r')

        twopdm = numpy.zeros( (norb, norb, norb, norb) )
        norb_read = int(f.readline().split()[0])
        assert(norb_read == norb)

        for line in f.readlines():
            linesp = line.split()
            twopdm[int(linesp[0]),int(linesp[3]),int(linesp[1]),int(linesp[2])] = 2.0*float(linesp[4])

        onepdm = numpy.einsum('ikjj->ik', twopdm)
        onepdm /= (nelectrons-1)

        return onepdm

    def make_rdm12(self, state, norb, nelec, link_index=None, **kwargs):
        nelectrons = 0
        if isinstance(nelec, (int, numpy.integer)):
            nelectrons = nelec
        else:
            nelectrons = nelec[0]+nelec[1]

        import os
        f = open(os.path.join('%s/%s/'%(self.scratchDirectory,"node0"), "spatial_twopdm.%d.%d.txt" %(state, state)), 'r')

        twopdm = numpy.zeros( (norb, norb, norb, norb) )
        norb_read = int(f.readline().split()[0])
        assert(norb_read == norb)

        for line in f.readlines():
            linesp = line.split()
            twopdm[int(linesp[0]),int(linesp[3]),int(linesp[1]),int(linesp[2])] = 2.0*float(linesp[4])

        onepdm = numpy.einsum('ijkk->ij', twopdm)
        onepdm /= (nelectrons-1)
        return onepdm, twopdm

    def make_rdm123(self, state, norb, nelec,  link_index=None, **kwargs):
        import os
        if self.has_threepdm == False:
            self.twopdm = False
            self.extraline.append('restart_threepdm')
            if isinstance(nelec, (int, numpy.integer)):
                neleca = nelec//2 + nelec%2
                nelecb = nelec - neleca
            else :
                neleca, nelecb = nelec
            writeDMRGConfFile(neleca, nelecb, True, self)
            if self.verbose >= logger.DEBUG1:
                inFile = self.configFile
                #inFile = os.path.join(self.scratchDirectory,self.configFile)
                logger.debug1(self, 'Block Input conf')
                logger.debug1(self, open(inFile, 'r').read())
            executeBLOCK(self)
            if self.verbose >= logger.DEBUG1:
                outFile = self.outputFile
                #outFile = os.path.join(self.scratchDirectory,self.outputFile)
                logger.debug1(self, open(outFile).read())
            self.has_threepdm = True
            self.extraline.pop()

        nelectrons = 0
        if isinstance(nelec, (int, numpy.integer)):
            nelectrons = nelec
        else:
            nelectrons = nelec[0]+nelec[1]

        f = open(os.path.join('%s/%s/'%(self.scratchDirectory,"node0"), "spatial_threepdm.%d.%d.txt" %(state, state)), 'r')

        threepdm = numpy.zeros( (norb, norb, norb, norb, norb, norb) )
        norb_read = int(f.readline().split()[0])
        assert(norb_read == norb)

        for line in f.readlines():
            linesp = line.split()
            threepdm[int(linesp[0]),int(linesp[1]),int(linesp[2]),int(linesp[3]),int(linesp[4]),int(linesp[5])] = float(linesp[6])

        twopdm = numpy.einsum('ijkklm->ijlm',threepdm)
        twopdm /= (nelectrons-2)
        onepdm = numpy.einsum('ijjk->ik', twopdm)
        onepdm /= (nelectrons-1)

        threepdm = numpy.einsum('jk,lm,in->ijklmn',numpy.identity(norb),numpy.identity(norb),onepdm)\
                 + numpy.einsum('jk,miln->ijklmn',numpy.identity(norb),twopdm)\
                 + numpy.einsum('lm,kijn->ijklmn',numpy.identity(norb),twopdm)\
                 + numpy.einsum('jm,kinl->ijklmn',numpy.identity(norb),twopdm)\
                 + numpy.einsum('mkijln->ijklmn',threepdm)

        twopdm = numpy.einsum('iklj->ijkl',twopdm) + numpy.einsum('il,jk->ijkl',onepdm,numpy.identity(norb))\

        return onepdm, twopdm, threepdm

    def make_rdm3(self, state, norb, nelec, wfnsym, molsym, dt=numpy.dtype('Float64'), filetype = "binary", link_index=None, **kwargs):
        import os

        if self.has_threepdm == False:
            self.twopdm = False
            self.extraline.append('threepdm\n')
            if isinstance(nelec, (int, numpy.integer)):
                neleca = nelec//2 + nelec%2
                nelecb = nelec - neleca
            else :
                neleca, nelecb = nelec
            writeDMRGConfFile(neleca, nelecb, wfnsym, molsym, False, self)

            executeBLOCK(self)
            self.has_threepdm = True
            self.extraline.pop()

        nelectrons = 0
        if isinstance(nelec, (int, numpy.integer)):
            nelectrons = nelec
        else:
            nelectrons = nelec[0]+nelec[1]

        if (filetype == "binary") :
            fname = os.path.join('%s/%s/'%(self.scratchDirectory,"node0"), "spatial_threepdm.%d.%d.bin" %(state, state))
            fnameout = os.path.join('%s/%s/'%(self.scratchDirectory,"node0"), "spatial_threepdm.%d.%d.bin.unpack" %(state, state))
            libE3unpack.unpackE3(ctypes.c_char_p(fname), ctypes.c_char_p(fnameout), ctypes.c_int(norb))

            E3 = numpy.fromfile(fnameout, dtype=numpy.dtype('Float64'))
            E3 = numpy.reshape(E3, (norb, norb, norb, norb, norb, norb), order='F')
            
        else :
            fname = os.path.join('%s/%s/'%(self.scratchDirectory,"node0"), "spatial_threepdm.%d.%d.txt" %(state, state))
            f = open(fname, 'r')
            lines = f.readlines()
            E3 = numpy.zeros(shape=(norb, norb, norb, norb, norb, norb), dtype=dt)
            for line in lines[1:]:
                linesp = line.split()
                if (len(linesp) != 7) :
                    continue
                a, b, c, d, e, f, integral = int(linesp[0]), int(linesp[1]), int(linesp[2]), int(linesp[3]), int(linesp[4]), int(linesp[5]), float(linesp[6])
                E3[a,b,c, f,e,d] = integral
                E3[a,c,b, f,d,e] = integral
                E3[b,a,c, e,f,d] = integral
                E3[b,c,a, e,d,f] = integral
                E3[c,a,b, d,f,e] = integral
                E3[c,b,a, d,e,f] = integral
                
        return E3

    def clearSchedule(self):
        self.scheduleSweeps = []
        self.scheduleMaxMs = []
        self.scheduleTols = []
        self.scheduleNoises = []



def make_schedule(sweeps, Ms, tols, noises, twodot_to_onedot):
    if len(sweeps) == len(Ms) == len(tols) == len(noises):
        schedule = ['schedule']
        for i, s in enumerate(sweeps):
            schedule.append('%d %6d  %8.4e  %8.4e' % (s, Ms[i], tols[i], noises[i]))
        schedule.append('end')
        if (twodot_to_onedot != 0):
            schedule.append('twodot_to_onedot %i'%twodot_to_onedot)
        return '\n'.join(schedule)
    else:
        
        return 'schedule default\nmaxM %s'%Ms[-1]

def writeDMRGConfFile(neleca, nelecb, wfnirrep, molsym, Restart, DMRGCI, approx= False):
    confFile = DMRGCI.configFile
    #confFile = os.path.join(DMRGCI.scratchDirectory,DMRGCI.configFile)

    f = open(confFile, 'w')
    if (DMRGCI.memory is not None):
        f.write('memory, %i, g\n'%(DMRGCI.memory))
    f.write('nelec %i\n'%(neleca+nelecb))
    f.write('spin %i\n' %(neleca-nelecb))
    f.write('irrep %i\n' % wfnirrep)

    if (not Restart):
        #f.write('schedule\n')
        #f.write('0 %6i  %8.4e  %8.4e \n' %(DMRGCI.maxM, 1e-5, 10.0))
        #f.write('1 %6i  %8.4e  %8.4e \n' %(DMRGCI.maxM, 1e-5, 1e-4))
        #f.write('10 %6i  %8.4e  %8.4e \n' %(DMRGCI.maxM, 1e-6, 1e-5))
        #f.write('16 %6i  %8.4e  %8.4e \n' %(DMRGCI.maxM, DMRGCI.tol/10.0, 0e-6))
        #f.write('end\n')
        schedule = make_schedule(DMRGCI.scheduleSweeps,
                                 DMRGCI.scheduleMaxMs,
                                 DMRGCI.scheduleTols,
                                 DMRGCI.scheduleNoises,
                                 DMRGCI.twodot_to_onedot)
        f.write('%s\n' % schedule)
    else :
        f.write('schedule\n')
        #if approx == True :
        #    f.write('0 %6i  %8.4e  %8.4e \n' %(DMRGCI.maxM, DMRGCI.tol*10.0, 0e-6))
        #else :
        #    f.write('0 %6i  %8.4e  %8.4e \n' %(DMRGCI.maxM, DMRGCI.tol, 0e-6))
        f.write('0 %6i  %8.4e  %8.4e \n' %(DMRGCI.maxM, DMRGCI.tol/10, 0e-6))
        f.write('end\n')
        f.write('fullrestart\n')
        f.write('onedot \n')

    f.write('sym %s\n' % molsym)
    f.write('orbitals %s\n' % DMRGCI.integralFile)
    #f.write('orbitals %s\n' % os.path.join(DMRGCI.scratchDirectory,
    #                                       DMRGCI.integralFile))
    if approx == True :
        f.write('maxiter %i\n'%DMRGCI.approx_maxIter)
    else :
        f.write('maxiter %i\n'%DMRGCI.maxIter)
    f.write('sweep_tol %8.4e\n'%DMRGCI.tol)

    f.write('outputlevel %s\n'%DMRGCI.outputlevel)
    f.write('hf_occ integral\n')
    if(DMRGCI.twopdm):
        f.write('twopdm\n')
    if(DMRGCI.nonspinAdapted):
        f.write('nonspinAdapted\n')
    if(DMRGCI.scratchDirectory):
        f.write('prefix  %s\n'%DMRGCI.scratchDirectory)
    if (DMRGCI.nroots !=1):
        f.write('nroots %d\n'%DMRGCI.nroots)
        if (DMRGCI.weights==[]):
            DMRGCI.weights= [1.0/DMRGCI.nroots]* DMRGCI.nroots
        f.write('weights ')
        for weight in DMRGCI.weights:
            f.write('%f '%weight)
        f.write('\n')

    f.write('num_thrds %d\n'%DMRGCI.num_thrds)
    for line in DMRGCI.extraline:
        f.write('%s\n'%line)
    f.close()

    import os
    os.system("cat dmrg.conf")
        #no reorder
    #f.write('noreorder\n')

def writeIntegralFile(h1eff, int2, ncas, neleca, nelecb, orbsym, DMRGCI):
    #integralFile = os.path.join(DMRGCI.scratchDirectory,DMRGCI.integralFile)
    integralFile = DMRGCI.integralFile

    if not os.path.exists(DMRGCI.scratchDirectory):
        os.makedirs(DMRGCI.scratchDirectory)
    f = open(integralFile, 'w+')

    f.write(' &FCI NORB= %i,NELEC= %i,MS2= %i,\n' %(ncas, neleca+nelecb, neleca-nelecb))
    f.write(' ORBSYM=')
    for i in range(ncas):
        f.write('%d,'%(orbsym[i]))

    f.write('\nISYM=1\n')
    f.write('&END\n')

    for i in range(ncas):
        for j in range(i+1):
            for k in range(ncas):
                for l in range(k+1):
                    if (abs(int2[i,k,j,l]) > 1.e-8):
                        f.write('%18.10e %3i  %3i  %3i  %3i\n' %(int2[i,k,j,l], i+1, j+1, k+1, l+1))
    for i in range(ncas):
        for j in range(i+1):
            if (abs(h1eff[i,j]) > 1.e-8):
                f.write('%18.10e %3i  %3i  %3i  %3i\n' %(h1eff[i,j], i+1, j+1, 0, 0))

    f.close()


def executeBLOCK(DMRGCI):

    inFile = DMRGCI.configFile
    outFile = DMRGCI.outputFile
    #inFile = os.path.join(DMRGCI.scratchDirectory,DMRGCI.configFile)
    #outFile = os.path.join(DMRGCI.scratchDirectory,DMRGCI.outputFile)
    from subprocess import check_call
    try:
        output = check_call("%s  %s  %s > %s"%(DMRGCI.mpiprefix, DMRGCI.executable, inFile, outFile), shell=True)
    except ValueError:
        print(output)
        exit()

def readEnergy(DMRGCI):
    import struct, os
    file1 = open(os.path.join('%s/%s/'%(DMRGCI.scratchDirectory,"node0"), "dmrg.e"),"rb")
    format = ['d']*DMRGCI.nroots
    format = ''.join(format)
    calc_e = struct.unpack(format, file1.read())
    file1.close()
    if DMRGCI.nroots ==1:
        return calc_e[0]
    else:
        return list(calc_e)



