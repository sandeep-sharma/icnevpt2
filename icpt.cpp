/*
Developed by Sandeep Sharma and Gerald Knizia, 2016
Copyright (c) 2016, Sandeep Sharma
*/

#include "CxNumpyArray.h"
#include "CxAlgebra.h"
#include "CxMemoryStack.h"
#include "CxPodArray.h"
#include "BlockContract.h"
#include "icpt.h"
#include "CxDiis.h"
#include <fstream>
#include "boost/format.hpp"

#include "inl/E_NEV_aavv.inl"
#include "inl/E_NEV_ccav.inl"
#include "inl/E_NEV_ccvv.inl"
#include "inl/E_NEV_acvv.inl"
#include "inl/E_NEV_ccaa.inl"
#include "inl/E_NEV_caav.inl"
#include "inl/E_NEV_aaav.inl"
#include "inl/E_NEV_aaac.inl"

#include "inl/E_LCC_aavv.inl"
#include "inl/E_LCC_acvv.inl"
#include "inl/E_LCC_ccvv.inl"
#include "inl/E_LCC_ccaa.inl"
#include "inl/E_LCC_ccav.inl"
#include "inl/E_LCC_caav.inl"
#include "inl/E_LCC_aaav.inl"
#include "inl/E_LCC_aaac.inl"
#include "inl/E_LCC_3rd.inl"

#include "inl/E_NEV_aavv_DF.inl"
#include "inl/E_NEV_ccav_DF.inl"
#include "inl/E_NEV_ccvv_DF.inl"
#include "inl/E_NEV_acvv_DF.inl"
#include "inl/E_NEV_ccaa_DF.inl"
#include "inl/E_NEV_caav_DF.inl"
#include "inl/E_NEV_aaav_DF.inl"
#include "inl/E_NEV_aaac_DF.inl"

#include "inl/E_LCC_aavv_DF.inl"
#include "inl/E_LCC_acvv_DF.inl"
#include "inl/E_LCC_ccvv_DF.inl"
#include "inl/E_LCC_ccaa_DF.inl"
#include "inl/E_LCC_ccav_DF.inl"
#include "inl/E_LCC_caav_DF.inl"
#include "inl/E_LCC_aaav_DF.inl"
#include "inl/E_LCC_aaac_DF.inl"
#include "inl/E_LCC_3rd_DF.inl"

#include "inl/E_NEV_aavv_CUMU.inl"
#include "inl/E_NEV_ccav_CUMU.inl"
#include "inl/E_NEV_ccvv_CUMU.inl"
#include "inl/E_NEV_acvv_CUMU.inl"
#include "inl/E_NEV_ccaa_CUMU.inl"
#include "inl/E_NEV_caav_CUMU.inl"
#include "inl/E_NEV_aaav_CUMU.inl"
#include "inl/E_NEV_aaac_CUMU.inl"

#include "inl/E_LCC_aavv_CUMU.inl"
#include "inl/E_LCC_acvv_CUMU.inl"
#include "inl/E_LCC_ccvv_CUMU.inl"
#include "inl/E_LCC_ccaa_CUMU.inl"
#include "inl/E_LCC_ccav_CUMU.inl"
#include "inl/E_LCC_caav_CUMU.inl"
#include "inl/E_LCC_aaav_CUMU.inl"
#include "inl/E_LCC_aaac_CUMU.inl"
#include "inl/E_LCC_3rd_CUMU.inl"

#include "inl/E_NEV_aavv_DFCUMU.inl"
#include "inl/E_NEV_ccav_DFCUMU.inl"
#include "inl/E_NEV_ccvv_DFCUMU.inl"
#include "inl/E_NEV_acvv_DFCUMU.inl"
#include "inl/E_NEV_ccaa_DFCUMU.inl"
#include "inl/E_NEV_caav_DFCUMU.inl"
#include "inl/E_NEV_aaav_DFCUMU.inl"
#include "inl/E_NEV_aaac_DFCUMU.inl"

#include "inl/E_LCC_aavv_DFCUMU.inl"
#include "inl/E_LCC_acvv_DFCUMU.inl"
#include "inl/E_LCC_ccvv_DFCUMU.inl"
#include "inl/E_LCC_ccaa_DFCUMU.inl"
#include "inl/E_LCC_ccav_DFCUMU.inl"
#include "inl/E_LCC_caav_DFCUMU.inl"
#include "inl/E_LCC_aaav_DFCUMU.inl"
#include "inl/E_LCC_aaac_DFCUMU.inl"
#include "inl/E_LCC_3rd_DFCUMU.inl"

using ct::TArray;
using ct::FMemoryStack2;
using boost::format;
using namespace std;

/* density fitting */
extern bool df;
/* track domains throughout the contractions */
extern bool track_contract;
extern int  track_contract_nbr;
extern int  track_contract_nbr_total;
extern int  track_indent;
extern char *track_domains;
/* enforce natural order of contraction */
extern bool enforce;

//=============================================================================
void IrPrintMatrixGen(std::ostream &xout,
                      FScalar *pData,
                      unsigned nRows,
                      unsigned iRowSt,
                      unsigned nCols,
                      unsigned iColSt,
                      std::string const &Caption);
void CopyDomainSubset(FSrciTensor &Out, FSrciTensor const &In, char const *pDomain, FDomainInfoMap &Domains);
FNdArrayView ViewFromNpy(ct::FArrayNpy &A);
FJobData::FJobData(): WfDecl(FWfDecl()),
                      RefEnergy(0.),
                      ThrTrun(1e-4),
                      ThrDen(1e-6),
                      ThrVar(1e-14),
                      resultOut(""),
                      guessInput(""),
                      LevelShift(0.),
                      Shift(0.),
                      nOrb(0),
                      MaxIt(100),
                      WorkSpaceMb(120000), //131072),  //8182),
                      nMaxDiis(6),
                      MethodClass(METHOD_MinE),
                      nAux(0){}


//=============================================================================
namespace srci {
//-----------------------------------------------------------------------------
#define NO_RT_LIB
  double GetTime() {
#ifdef NO_RT_LIB
    timeval tv;
    gettimeofday(&tv, 0);
    return tv.tv_sec + 1e-6 * tv.tv_usec;
#else
    timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    // ^- interesting trivia: CLOCK_MONOTONIC is not guaranteed
    //    to actually be monotonic.
    return ts.tv_sec + 1e-9 * ts.tv_nsec;
#endif // NO_RT_LIB
  } // end GetTime
} // end srci



//=============================================================================
void FJobData::ReadInputFile(char const *pArgFile){
//-----------------------------------------------------------------------------
  /*!
    Read the input file.

   (Maybe I should use boost program options for this? it can deal with files
    and stuff. But it might be helpful to keep the ci program completely
    dependency free for inclusion in molpro and stuff.)
  */
//-----------------------------------------------------------------------------
  bool cumulant=false;
  std::ifstream inpf(pArgFile);
  while (inpf.good()) {
    // behold the most marvellous elegance of C++ I/O handling.
    std::string Line, ArgName, ArgValue;
    std::getline(inpf, Line);
    std::stringstream inp(Line);
    inp >> ArgName >> ArgValue;
    // ^- this won't work if spaces are in the file name...

    if (inpf.eof()) break;
    if (!ArgName.empty() and ArgName[0] == '#')
      continue;

    // Reading keywords
    if (ArgName == "method")
       MethodName = ArgValue;
    else if (ArgName == "nelec")
       WfDecl.nElec = atoi(ArgValue.c_str());
    else if (ArgName == "nact")
       WfDecl.nActElec = atoi(ArgValue.c_str());
    else if (ArgName == "nactorb")
       WfDecl.nActOrb = atoi(ArgValue.c_str());
    else if (ArgName == "naux")
       nAux = atoi(ArgValue.c_str());
    else if (ArgName == "ms2")
       WfDecl.Ms2 = atoi(ArgValue.c_str());
    else if (ArgName == "ref-energy")
       RefEnergy = atof(ArgValue.c_str());
    else if (ArgName == "thr-den")
       ThrDen = atof(ArgValue.c_str());
    else if (ArgName == "orbitalFile")
       orbitalFile = ArgValue;
    else if (ArgName == "thr-var")
       ThrVar = atof(ArgValue.c_str());
    else if (ArgName == "thr-trunc")
       ThrTrun = atof(ArgValue.c_str());
    else if (ArgName == "load")
       guessInput = ArgValue;
    else if (ArgName == "save")
       resultOut = ArgValue;
    else if (ArgName == "shift")
       Shift = atof(ArgValue.c_str());
    else if (ArgName == "level-shift")
       LevelShift = atof(ArgValue.c_str());
    else if (ArgName == "max-diis")
       nMaxDiis = atof(ArgValue.c_str());
    else if (ArgName == "max-iter")
       MaxIt = atoi(ArgValue.c_str());
    else if (ArgName == "cumulant")
       cumulant = true;
    else if (ArgName == "int1e/fock")
       ct::ReadNpy(Int1e_Fock, ArgValue);
    else if (ArgName == "int1e/coreh")
       ct::ReadNpy(Int1e_CoreH, ArgValue);
    else if (ArgName == "work-space-mb")
       WorkSpaceMb = atoi(ArgValue.c_str());
    else if ((ArgName == "handcodedWeeee")
           ||(ArgName == "handcodedE3")
           ||(ArgName == "cumulantE4")){
       std::cout << "('handcodedWeee', 'handcodedE3', 'cumulantE4' are deprecated keywords\n";
       std::cout << " The program just disregards them and carries on.)\n\n";
    }
    else if (ArgName == "orb-type") {
      if (ArgValue == "spatial/MO")
        WfDecl.OrbType = ORBTYPE_Spatial;
      else if (ArgValue == "spinorb/MO")
        WfDecl.OrbType = ORBTYPE_SpinOrb;
      else
        throw std::runtime_error("orbital type '" + ArgValue + "' not recognized.");
    }
    else
      throw std::runtime_error("argument '" + ArgName + "' not recognized.");
  } // end while

  if (nAux!=0){
    df=true;
    MethodName=MethodName+"_DF";
  }
  if (cumulant){
    int countA=0;for (int i=0;i<MethodName.length();i++) if (MethodName[i]=='_') countA++;
    int countB=0;for (int i=0;i<MethodName.length();i++) if (MethodName[i]=='3') countB++;
    if(countA==1&&countB==0) MethodName=MethodName+"_CUMU";
    if(countA==2&&countB==0) MethodName=MethodName+"CUMU";
    if(countA==0&&countB==1) MethodName=MethodName+"_CUMU";
    if(countA==1&&countB==1) MethodName=MethodName+"CUMU";
  }

  // Checks and Balances: nOrb and MakeFockAfterIntegralTrafo
  if (Int1e_Fock.Rank() != 0) {
    if (Int1e_Fock.Rank() != 2 || Int1e_Fock.Shape[0] != Int1e_Fock.Shape[1])
      throw std::runtime_error("int1e/fock must specify a rank 2 square matrix.");
    nOrb = Int1e_Fock.Shape[0];
    MakeFockAfterIntegralTrafo = false;
  } else {
    if (Int1e_CoreH.Rank() != 2 || Int1e_CoreH.Shape[0] != Int1e_CoreH.Shape[1])
      throw std::runtime_error("int1e/coreh must specify a rank 2 square matrix.");
    nOrb = Int1e_CoreH.Shape[0];
    MakeFockAfterIntegralTrafo = false;
  }

  /*
  if (Int2e_4ix.Rank() != 0) {
    if (Int2e_3ix.Rank() != 0)
      throw std::runtime_error("int2e/3ix and int2e/4x interaction matrix elements cannot both be given.");
    if (Int2e_4ix.Rank() != 4 || Int2e_4ix.Shape[0] != Int2e_4ix.Shape[1] ||
      Int2e_4ix.Shape[0] != Int2e_4ix.Shape[2] || Int2e_4ix.Shape[0] != Int2e_4ix.Shape[3])
      throw std::runtime_error("int2e-4ix must be a rank 4 tensor with equal dimensions.");
    if (Int2e_4ix.Shape[0] != nOrb)
      throw std::runtime_error("nOrb derived from int2e_4ix not compatible with nOrb derived from Fock matrix.");
    // ^- I guess technically we might want to allow not specifying
    //    int2e_4ix at all.
  } else {
    if (Int2e_3ix.Rank() == 0)
      throw std::runtime_error("either int2e/3ix or int2e/4x interaction matrix elements must be given.");
    if (Int2e_4ix.Rank() != 3 || Int2e_4ix.Shape[1] != Int2e_4ix.Shape[2])
      throw std::runtime_error("int2e-3ix must be a rank 3 tensor (F|rs) with r,s having equal dimensions.");
    if (Int2e_3ix.Shape[1] != nOrb)
      throw std::runtime_error("nOrb derived from int2e_3ix not compatible with nOrb derived from Fock matrix.");
  }
  */

  // Checks and Balances: Shapes
  if (Orbs.Rank() != 0) {
    if (Orbs.Rank() != 2 || Orbs.Shape[0] < Orbs.Shape[1])
      throw std::runtime_error("Input orbital matrix must have rank 2 and have at least as many orbitals as there are basis functions.");
    if (Int1e_Fock.Shape[0] != Orbs.Shape[0])
      throw std::runtime_error("nOrb derived from Fock not compatible with number of rows in orbital matrix.");
    nOrb = Orbs.Shape[1];
  }

  // Checks and Balances: nElec and nOrbs and Ms2
  if (2*nOrb < WfDecl.nElec)
    throw std::runtime_error("not enough orbitals for the given number of electrons.");
  if ((uint)std::abs(WfDecl.Ms2) > WfDecl.nElec)
    throw std::runtime_error("spin projection quantum number Ms2 lies outside of -nElec..+nElec");
  if (((uint)std::abs(WfDecl.Ms2) % 2) != (WfDecl.nElec % 2))
    throw std::runtime_error("Ms2 and nElec must have same parity.");

} // end FJobData::ReadInputFile


//=============================================================================
void FJobContext::Header(char const *pArgFile){
//-----------------------------------------------------------------------------
  /*!
    Print stuff.
  */
//-----------------------------------------------------------------------------

  std::cout << "\n";
  std::cout << "  _            _\n";
  std::cout << " (_) ___ _ __ | |_\n";
  std::cout << " | |/ __| '_ \\| __|\n";
  std::cout << " | | (__| |_) | |_\n";
  std::cout << " |_|\\___| .__/ \\__|\n";
  std::cout << "        |_|\n";
  std::cout << "\n";
  std::cout << "\n";

  // Printout header
  if (nAux==0){
    std::cout << format("*File '%s'  nOrb = %i%s  nElec = %i  Ms2 = %i  iWfSym = %i")
       % pArgFile % nOrb % "(R)" % WfDecl.nElec % WfDecl.Ms2 % 0 << std::endl;
  } else {
    std::cout << format("*File '%s'  nOrb = %i%s  nAux = %i  nElec = %i  Ms2 = %i  iWfSym = %i")
       % pArgFile % nOrb % "(R)" % nAux % WfDecl.nElec % WfDecl.Ms2 % 0 << std::endl;
  }

  // Message
  std::cout << format(" Performing a %s %s calculation with %s orbitals.\n")
     % Method.pSpinClass
     % MethodName
     % ((WfDecl.OrbType == ORBTYPE_Spatial)? "spatial" : "spin");

  // Print out message about E3
  if (Method.E3handcoded_if_zero==0){
    std::cout << "\n E3handcoded_if_zero=0";
    std::cout << "\n   The 'E3' occurences will";
    std::cout << "\n   be handled by hand-coded equations in 'PerturberDependentCode'.";
    std::cout << "\n   This supposes that the corresponding lines";
    std::cout << "\n   containing 'E3' have been COMMENTED OUT in 'EqsRes' in the INL files.\n";
  } else if (Method.E3handcoded_if_zero==2){
    std::cout << "\n E3handcoded_if_zero=2";
    std::cout << "\n   The 'E3' has no hand-coded equations: N/A. (right??)";
    std::cout << "\n   (AAVV) (CAAV) (CCAA)\n";
  } else {
    std::cout << "\n E3handcoded_if_zero!=0";
    std::cout << "\n   The 'E3' occurences will NOT";
    std::cout << "\n   be handled by hand-coded equations in 'PerturberDependentCode'.";
    std::cout << "\n   This supposes that the corresponding lines";
    std::cout << "\n   containing 'E3' are PRESENT in 'EqsRes' in the INL files.\n";
  }

  //// Print out message about E4
  Method.E4cumulant_if_zero=1;
  //if (Method.E4cumulant_if_zero==1){
  //  std::cout << "\n E4cumulant_if_zero=1";
  //  std::cout << "\n   The cumulant approximation for E4 is NOT used.";
  //  std::cout << "\n   The full E4 is needed in 'int/'.\n";
  //} else {
  //  std::cout << "\n E4cumulant_if_zero=0";
  //  std::cout << "\n   The cumulant approximation for E4 is used.";
  //  std::cout << "\n   E4 will be reconstructed.\n";
  ////std::cout << "\n   This supposes that there is no lines containing 'E4' in the INL files.\n";
  //}

  // Print out message about DF
  if (Method.Whandcoded_if_zero==1 && nAux==0)
    throw std::runtime_error("Whandcoded_if_zero=1 and nAux=0 !");
  if (Method.Whandcoded_if_zero==0){
    std::cout << "\n Whandcoded_if_zero=0";
    std::cout << "\n   The possible 'W:eeee' occurences will be handled";
    std::cout << "\n   by hand-coded equations in 'PerturberDependentCode'.";
    std::cout << "\n   This supposes that the corresponding lines containing 'W:eeee'";
    std::cout << "\n   have been COMMENTED OUT in 'EqsRes' in the INL files.\n";
  } else if (Method.Whandcoded_if_zero==2){
    std::cout << "\n Whandcoded_if_zero=2";
    std::cout << "\n   The 'W' has no hand-coded equations: N/A. (right??)";
    std::cout << "\n   (AAVV) (CCVV) (ACVV)\n";
  } else {
    std::cout << "\n Whandcoded_if_zero!=0";
    std::cout << "\n   The 'W:eeee' hand-coded equations in 'PerturberDependentCode' will NOT be triggered.\n";
  }

}


//=============================================================================
void FJobContext::ExecHandCoded(FMemoryStack2 *Mem){
//-----------------------------------------------------------------------------
    /*!
      Some equations are hand coded here rather then automatically produced by SQA
      It can be for W:eeee or for E3.
    */
//-----------------------------------------------------------------------------
  if(m_Domains['L'].nSize==0){
    return FJobContext::ExecHandCoded_woDF(Mem);
  }else{
    return FJobContext::ExecHandCoded_wDF(Mem);
  }
}


//=============================================================================
void FJobContext::ConjGrad(FMemoryStack2 *Mem){
//-----------------------------------------------------------------------------
  /*!
    Run the Conjugate Gradient algorithm to find "d" coefficients and E2 energy.
  */
//-----------------------------------------------------------------------------
  // Initialize "Amplitudes"
  InitAmplitudes(Mem[0]);
  //TestingDF(Mem);

  // Test nrm of "b"
  bool skip_conjgrad = false;
  FScalar nrm2 = ct::Dot(TN("b")->pData,
                         TN("b")->pData,
                         TN("b")->nValues());                // <b|b>
  std::cout << "\n The nrm2 of bvec is: "<<nrm2 << endl;
  if (nrm2<1e-5){
    {
    FNdArrayView *TEST = TN("b");
    for (int m=0; m<TEST->Sizes[0]; m++)
      for (int n=0; n<TEST->Sizes[1]; n++)
        for (int o=0; o<TEST->Sizes[2]; o++)
          for (int p=0; p<TEST->Sizes[3]; p++){
            printf("[b] % 5i % 5i % 5i % 5i % 13.6f\n",m,n,o,p,TEST->operator()(m,n,o,p));
    }}
    //throw std::runtime_error("The overlap b is null!");
    std::cout << "\n The overlap S.w=b is null: will bypass the ConjGrad and set E=0, d=0\n" << endl;
    skip_conjgrad = true;
  }

  // Make the Overlap and Ortho basis
  MakeOverlapAndOrthogonalBasis(Mem);

  bool
    Converged = false,
    NoSing = false; // if set, explicitly set all singles amplitudes to zero.
  FScalar
    Energy = 0, LastEnergy = 0, Var2 = 0, Nrm2 = 0,
    tResid = 0, tRest = 0, tRold = 0, scale = 1.0;
  double tStart = srci::GetTime(), tMain = -srci::GetTime();

  if (skip_conjgrad){
    Converged=true;
  }else{
  // Iter0
  // Prepare B and T, P and p
  MakeOrthogonal(std::string("b"), std::string("B"));     //b -> B
  if (guessInput.compare("") == 0)
    MakeOrthogonal(std::string("t"), std::string("T"));   //t -> T
  Copy(*TN("P"), *TN("T"));                               //P <- T
  BackToNonOrthogonal("p", "P");                          //p <- P
  TN("Ap")->ClearData();                                  //Ap clear
  ExecEquationSet(Method.EqsRes, m_Tensors, Mem[0]);      //Ap = A*p
  ExecHandCoded(Mem);                                     //Ap = A*p (HandCoded)
  /* track domains: PRINT and TURN OFF*/
  if (track_contract){
    printf("-----total_contract %i\n",track_contract_nbr_total);
    std::cout<<std::endl;
    track_contract=false;
    //return;
  }
  MakeOrthogonal("Ap", "AP");                             //Ap -> AP
  ct::Add(TN("AP")->pData,
          TN("P")->pData,
          Shift,
          TN("AP")->nValues());                           //AP = AP + shift * P
  Copy(*TN("R"), *TN("AP"));                              //R <- AP
  ct::Scale(TN("R")->pData, -scale, TN("R")->nValues());  //R = -1.0*R
  ct::Add(TN("R")->pData,
          TN("B")->pData,
          scale,
          TN("R")->nValues());                            //R = R+B
  tRold = ct::Dot(TN("R")->pData,
                  TN("R")->pData, TN("R")->nValues());    //<R|R>

  Copy(*TN("P"), *TN("R"));                               // R = AP


  // Loop to obtain "d" and "E2"
  std::cout << format("\n Convergence thresholds:   THRDEN = %6.2e  THRVAR = %6.2e\n") % ThrDen % ThrVar;
  std::cout << "\n ITER.      SQ.NORM        ENERGY      ENERGY CHANGE     VAR       TIME     DIIS" << std::endl;
  double last_ten[10]; int last_ten_pos=0;
  for (uint iIt = 0; iIt < MaxIt; ++ iIt){
    BackToNonOrthogonal("p", "P");                       //p <- P

    tResid -= srci::GetTime();
    TN("Ap")->ClearData();                               //Ap clear
    ExecEquationSet(Method.EqsRes, m_Tensors, Mem[0]);   //Ap = A*p
    ExecHandCoded(Mem);                                  //Ap = A*p (HandCoded)
    // level-shift 
    if ((not Converged)&&(iIt!=MaxIt-1)){
      for(int p=0;p<TN("Ap")->Sizes[0];p++)
      for(int q=0;q<TN("Ap")->Sizes[1];q++)
      for(int r=0;r<TN("Ap")->Sizes[2];r++)
      for(int s=0;s<TN("Ap")->Sizes[3];s++)
        TN("Ap")->operator()(p,q,r,s)+=TN("p")->operator()(p,q,r,s)*LevelShift;
    }else{
      //std::cout<<"BM last iter without LevelShift"<<std::endl;
    }
    tResid += srci::GetTime();

    MakeOrthogonal("Ap", "AP");                          //Ap -> AP
    ct::Add(TN("AP")->pData,
            TN("P")->pData,
            Shift,
            TN("AP")->nValues());                        //AP = AP + shift * P
    FScalar alpha = tRold / ct::Dot(TN("P")->pData,
                                    TN("AP")->pData,
                                    TN("P")->nValues()); //<P|AP>
    ct::Add(TN("T")->pData,
            TN("P")->pData,
            alpha,
            TN("R")->nValues());                         //T = T+alph*P
    ct::Add(TN("R")->pData,
            TN("AP")->pData,
            -alpha,
            TN("R")->nValues());                         //R = R-alph*AP
    tResid = ct::Dot(TN("R")->pData,
                     TN("R")->pData,
                     TN("R")->nValues());                // <R|R>
    Nrm2   = ct::Dot(TN("T")->pData,
                     TN("T")->pData,
                     TN("T")->nValues());                // <T|T>
    Energy = -ct::Dot(TN("T")->pData,
                      TN("B")->pData,
                      TN("T")->nValues())                // E=-<T|B>
             -ct::Dot(TN("T")->pData,
                      TN("R")->pData,
                      TN("T")->nValues());               //   -<T|R>

    std::cout << format("%4i   %14.8f %14.8f %14.8f    %8.2e%10.2f\n")
       % (1+iIt) % Nrm2 % (Energy+RefEnergy) % (Energy-LastEnergy) % tResid
       % (srci::GetTime() - tStart) ;
    std::cout << std::flush;
    tStart = srci::GetTime();

    // Converged
    last_ten[last_ten_pos%10]=abs(Energy-LastEnergy);
    last_ten_pos+=1;
    if (Converged) break;
    if ((tResid < ThrVar)
      ||((*std::max_element(last_ten,last_ten+10) < 0.001)
         &&(tResid < 0.0002))) {
      Converged = true;
    //break;
    }

    ct::Scale(TN("P")->pData,
              tResid/tRold,
              TN("P")->nValues());                       //P = (rnew/rold)*P
    ct::Add(TN("P")->pData,
            TN("R")->pData,
            scale,
            TN("R")->nValues());                         //P = P+R
    tRold = tResid;
    LastEnergy = Energy;
  } // end of loop
  } // end of if skip_conjgrad

  // Messages
  if ( !Converged )
    std::cout << format("\n*WARNING: No convergence for root %i."
                        " Stopped at NIT: %i  DEN: %.2e  VAR: %.2e")
                        % 0 % MaxIt % (Energy - LastEnergy) % Var2 << std::endl;
  std::cout << "\n";
  tMain += srci::GetTime();
  tRest = tMain - tResid;
  PrintTiming("main loop", tMain);
  PrintTiming("residual", tResid);
  PrintTiming("rest", tRest);
  FScalar c0 = 1./std::sqrt(Nrm2);
  std::cout << "\n";
  PrintResult("Coefficient of reference function", c0, -1);
  PrintResult("Reference energy", RefEnergy, -1);
  if (RefEnergy != 0.)
    PrintResult("Correlation energy", Energy, -1);

  // Export the final "d" coefficients found
  if (resultOut.compare("") != 0) {
    BackToNonOrthogonal("t", "T");

    // The "resultOut" file is given in the input in the general case.
    ct::FShapeNpy outshape;
    if((0 == strcmp(Method.perturberClass, "CAAV"))){
      // For 3rd-order calculation and for the CAAV class, we need to store D1 and D2 (for the two basis).
      // Hence the following code overwrites the filename given in input.
      // WARNING: This is good only for the 3rd-order calculation!
      std::cout<<"\n WARNING! This is good only in the prospect of 3rd-order calculation!\n";

      int nact = m_Domains['a'].nSize;
      int nvir = m_Domains['e'].nSize;
      int ncor = m_Domains['c'].nSize;

      FNdArrayView *t  = TND("t:cAae");

      FNdArrayView d1; CreateTensorFromShape(d1, "eaca", "", m_Domains);
      double* d1data = Mem->AllocN(d1.nValues(), 1.0);
      d1.pData = d1data;
      for (int m=0; m<ncor; m++)
        for (int n=0; n<nact; n++)
          for (int o=0; o<nact; o++)
            for (int p=0; p<nvir; p++){
              d1.operator()(p,n,m,o)=t->pData[m+ncor*n+ncor*nact*o+ncor*nact*nact*p];
            };
      printf("\n Coefficient of Psi1 wrote to int/D:eaca.npy");
      outshape = ct::MakeShape(d1.Sizes[0],d1.Sizes[1],d1.Sizes[2],d1.Sizes[3]);
      ct::WriteNpy("int/D:eaca.npy", d1.pData, outshape);

      FNdArrayView d2; CreateTensorFromShape(d2, "aeca", "", m_Domains);
      double* d2data = Mem->AllocN(d2.nValues(), 1.0);
      d2.pData = d2data;
      for (int m=0; m<ncor; m++)
        for (int n=0; n<nact; n++)
          for (int o=0; o<nact; o++)
            for (int p=0; p<nvir; p++){
              d2.operator()(n,p,m,o)=t->pData[m+ncor*n+ncor*nact*o+ncor*nact*nact*p+ncor*nact*nact*nvir];
            };
      printf("\n Coefficient of Psi1 wrote to int/D:aeca.npy\n");
      outshape = ct::MakeShape(d2.Sizes[0],d2.Sizes[1],d2.Sizes[2],d2.Sizes[3]);
      ct::WriteNpy("int/D:aeca.npy", d2.pData, outshape);

    } else {
      printf("\n Coefficient of Psi1 wrote to %s\n",resultOut.c_str());
      outshape = ct::MakeShape(TN("t")->Sizes[0], TN("t")->Sizes[1],TN("t")->Sizes[2],TN("t")->Sizes[3]);
      ct::WriteNpy(resultOut, TN("t")->pData, outshape);
    } // end if CAAV
  } // end if resultOut

  std::cout << "\n";
  PrintResult("ENERGY", RefEnergy + Energy, 0);
  //if ( !Converged ) throw std::runtime_error("No convergence!");
} // end FJobContext::ConjGrad


//=============================================================================
void FJobContext::Init(ct::FMemoryStack2 &Mem){
//-----------------------------------------------------------------------------
  /*!
    Initialize stuff
  */
//-----------------------------------------------------------------------------
  // Initialize stuff
  InitDomains();
  CreateTensors(Mem);
  InitAmpResPairs();
} // end FJobContext::Init


//=============================================================================
void FJobContext::ReadMethodFile(){
//-----------------------------------------------------------------------------
  /*!
    Read Info from Method
  */
//-----------------------------------------------------------------------------

  // NEVPT
  if (MethodName == "NEVPT_AAVV")
    NEVPT_AAVV::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_ACVV")
    NEVPT_ACVV::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCAV")
    NEVPT_CCAV::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCVV")
    NEVPT_CCVV::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCAA")
    NEVPT_CCAA::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CAAV")
    NEVPT_CAAV::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_AAAV")
    NEVPT_AAAV::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_AAAC")
    NEVPT_AAAC::GetMethodInfo(Method);
  //else if (MethodName == "NEVPT3_H"){ // "perturberClass" needs to be
  //  NEVPT3_H::GetMethodInfo(Method);  // initialized to prevent errors
  //  Method.perturberClass="empty";    // in "if" statements in PerturberDependentCode
  //}
  //else if (MethodName == "NEVPT3_H0"){ // "perturberClass" needs to be
  //  NEVPT3_H0::GetMethodInfo(Method);  // initialized to prevent errors
  //  Method.perturberClass="empty";     // in "if" statements in PerturberDependentCode
  //}
  // MRLCC
  else if (MethodName == "MRLCC_AAVV")
    MRLCC_AAVV::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_ACVV")
    MRLCC_ACVV::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCVV")
    MRLCC_CCVV::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCAA")
    MRLCC_CCAA::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCAV")
    MRLCC_CCAV::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CAAV")
    MRLCC_CAAV::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_AAAV")
    MRLCC_AAAV::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_AAAC")
    MRLCC_AAAC::GetMethodInfo(Method);
  else if (MethodName == "MRLCC3"){ // "perturberClass" needs to be
    MRLCC3::GetMethodInfo(Method);  // initialized to prevent errors
    Method.perturberClass="empty";  // in "if" statements in PerturberDependentCode
  }

  // NEVPT
  else if (MethodName == "NEVPT_AAVV_DF")
    NEVPT_AAVV_DF::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_ACVV_DF")
    NEVPT_ACVV_DF::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCAV_DF")
    NEVPT_CCAV_DF::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCVV_DF")
    NEVPT_CCVV_DF::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCAA_DF")
    NEVPT_CCAA_DF::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CAAV_DF")
    NEVPT_CAAV_DF::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_AAAV_DF")
    NEVPT_AAAV_DF::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_AAAC_DF")
    NEVPT_AAAC_DF::GetMethodInfo(Method);
  // MRLCC
  else if (MethodName == "MRLCC_AAVV_DF")
    MRLCC_AAVV_DF::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_ACVV_DF")
    MRLCC_ACVV_DF::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCVV_DF")
    MRLCC_CCVV_DF::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCAA_DF")
    MRLCC_CCAA_DF::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCAV_DF")
    MRLCC_CCAV_DF::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CAAV_DF")
    MRLCC_CAAV_DF::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_AAAV_DF")
    MRLCC_AAAV_DF::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_AAAC_DF")
    MRLCC_AAAC_DF::GetMethodInfo(Method);
  else if (MethodName == "MRLCC3_DF"){ // "perturberClass" needs to be
    MRLCC3_DF::GetMethodInfo(Method);  // initialized to prevent errors
    Method.perturberClass="empty";     // in "if" statements in PerturberDependentCode
  }

  // NEVPT
  else if (MethodName == "NEVPT_AAVV_CUMU")
    NEVPT_AAVV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_ACVV_CUMU")
    NEVPT_ACVV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCAV_CUMU")
    NEVPT_CCAV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCVV_CUMU")
    NEVPT_CCVV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCAA_CUMU")
    NEVPT_CCAA_CUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CAAV_CUMU")
    NEVPT_CAAV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_AAAV_CUMU")
    NEVPT_AAAV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_AAAC_CUMU")
    NEVPT_AAAC_CUMU::GetMethodInfo(Method);
  // MRLCC
  else if (MethodName == "MRLCC_AAVV_CUMU")
    MRLCC_AAVV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_ACVV_CUMU")
    MRLCC_ACVV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCVV_CUMU")
    MRLCC_CCVV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCAA_CUMU")
    MRLCC_CCAA_CUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCAV_CUMU")
    MRLCC_CCAV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CAAV_CUMU")
    MRLCC_CAAV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_AAAV_CUMU")
    MRLCC_AAAV_CUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_AAAC_CUMU")
    MRLCC_AAAC_CUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC3_CUMU"){ // "perturberClass" needs to be
    MRLCC3_CUMU::GetMethodInfo(Method);  // initialized to prevent errors
    Method.perturberClass="empty";     // in "if" statements in PerturberDependentCode
  }

  // NEVPT
  else if (MethodName == "NEVPT_AAVV_DFCUMU")
    NEVPT_AAVV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_ACVV_DFCUMU")
    NEVPT_ACVV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCAV_DFCUMU")
    NEVPT_CCAV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCVV_DFCUMU")
    NEVPT_CCVV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CCAA_DFCUMU")
    NEVPT_CCAA_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_CAAV_DFCUMU")
    NEVPT_CAAV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_AAAV_DFCUMU")
    NEVPT_AAAV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "NEVPT_AAAC_DFCUMU")
    NEVPT_AAAC_DFCUMU::GetMethodInfo(Method);
  // MRLCC
  else if (MethodName == "MRLCC_AAVV_DFCUMU")
    MRLCC_AAVV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_ACVV_DFCUMU")
    MRLCC_ACVV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCVV_DFCUMU")
    MRLCC_CCVV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCAA_DFCUMU")
    MRLCC_CCAA_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CCAV_DFCUMU")
    MRLCC_CCAV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_CAAV_DFCUMU")
    MRLCC_CAAV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_AAAV_DFCUMU")
    MRLCC_AAAV_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC_AAAC_DFCUMU")
    MRLCC_AAAC_DFCUMU::GetMethodInfo(Method);
  else if (MethodName == "MRLCC3_DFCUMU"){ // "perturberClass" needs to be
    MRLCC3_DFCUMU::GetMethodInfo(Method);  // initialized to prevent errors
    Method.perturberClass="empty";     // in "if" statements in PerturberDependentCode
  }

  // Error
  else
    throw std::runtime_error("Method '" + MethodName + "' not recognized.");
} // end FJobContext::ReadMethodFile


//=============================================================================
void FJobContext::InitDomains(){
//-----------------------------------------------------------------------------
  /*!
    Initialize info about Domains (core, active, external, auxiliary)
  */
//-----------------------------------------------------------------------------
  if (WfDecl.OrbType == ORBTYPE_Spatial) {
    FDomainInfo
       &Ext = m_Domains['e'],
       &Act = m_Domains['a'],
       &Clo = m_Domains['c'],
       &Aux = m_Domains['L'];
    Clo.iBase = 0;
    Clo.nSize = (WfDecl.nElec-WfDecl.nActElec)/2.;
    //CopyEps(Clo, -1., Int1e_Fock);

    Act.iBase = Clo.nSize;
    Act.nSize = WfDecl.nActOrb;
    //CopyEps(Act, -1., Int1e_Fock);

    Ext.iBase = Clo.nSize+Act.nSize;
    Ext.nSize = nOrb - Ext.iBase;
    //CopyEps(Ext, +1., Int1e_Fock);

    Aux.iBase = Clo.nSize+Act.nSize+Ext.nSize;
    Aux.nSize = nAux;
    //CopyEps(Ext, +1., Int1e_Fock);

    for (int d=0; d<Method.nDomainDecls; d++) {
      FDomainInfo &d1 = m_Domains[Method.pDomainDecls[d].pName[0]];
      FDomainInfo &Related = m_Domains[ Method.pDomainDecls[d].pRelatedTo[0]];
      d1.iBase = Related.iBase;
      d1.nSize = Method.pDomainDecls[d].f(Related.nSize);
    }
  } //end if
} // end FJobContext::InitDomains


//=============================================================================
void FJobContext::CreateTensors(ct::FMemoryStack2 &Mem){
//-----------------------------------------------------------------------------
  /*!
    Create all the Tensors found in "Method.nTensorDecls"
  */
//-----------------------------------------------------------------------------
  m_Tensors.resize(Method.nTensorDecls);
  size_t TotalSize = 0;

  // Meta-data
  for (uint i = 0; i != Method.nTensorDecls; ++i) {
    FTensorDecl const
    &Decl = Method.pTensorDecls[i];
    CreateTensorFromShape(m_Tensors[i], Decl.pDomain, Decl.pSymmetry, m_Domains);
    // "Placeholders" don't have their own data
    if (Decl.Usage != USAGE_PlaceHolder && Decl.Storage != STORAGE_Disk)
      TotalSize += m_Tensors[i].nValues();

    // Keep a link in case we need to look up the contents of individual tensors.
    std::string NameAndDomain = boost::str(format("%s:%s") % Decl.pName % Decl.pDomain);
    m_TensorsByNameAndDomain[NameAndDomain] = &m_Tensors[i];
    m_TensorsByName[Decl.pName] = &m_Tensors[i];
  }

  // Actual data array and assign the tensors to their target position.
  for (uint i = 0; i != Method.nTensorDecls; ++i) {
    if (Method.pTensorDecls[i].Storage != STORAGE_Disk)
      FillData(i, Mem);
  }

  // Message
  size_t nSizeVec;
  std::cout << format("\n Size of working set:           %12i mb\n"
    " Size of Hamiltonian:           %12i mb\n"
    " Size of CI amplitude vector:   %12i mb [%i entries/vec]")
    % (TotalSize*sizeof(FScalar)/1048576)
    % ((Int1e_Fock.Size() + Int2e_4ix.Size())*sizeof(FScalar)/1048576)
    % (nSizeVec*sizeof(FScalar)/1048576)
    % (nSizeVec)
    << std::endl;
} // end FJobContext::CreateTensors


//=============================================================================
void FJobContext::CreateTensorFromShape(FSrciTensor &Out,
                                        char const *pDomain, char const *pSymmetry,
                                        FDomainInfoMap const &DomainInfo){
//-----------------------------------------------------------------------------
  /*!
    Create Empty Tensor of Shape corresponding to pDomain
  */
//-----------------------------------------------------------------------------
  Out.pData = 0;
  Out.Sizes.clear();
  Out.Strides.clear();
  FArrayOffset Stride = 1;
  for (char const *p = pDomain; *p != 0; ++ p) {
    Out.Sizes.push_back(value(DomainInfo, *p).nSize);
    Out.Strides.push_back(Stride);
    Stride *= Out.Sizes.back();
  }
} // end FJobContext::CreateTensorFromShape


//=============================================================================
void FJobContext::FillData(int i, ct::FMemoryStack2 &Mem) {
//-----------------------------------------------------------------------------
  /*!
    Fill ith Tensor with it's due data
  */
//-----------------------------------------------------------------------------
  FTensorDecl const
    &Decl = Method.pTensorDecls[i];
  double A = 1.0;

  // Only "Placeholder" don't have their own data
  if (Decl.Usage != USAGE_PlaceHolder) {
    double* tensorData = Mem.AllocN(m_Tensors[i].nValues(), A);
    m_Tensors[i].pData = tensorData;
  }

  // Density Tensors
  if (Decl.Usage == USAGE_Density) {
    uint k;
    const char *delta = "delta";
    // delta Tensors
    if (0 == strcmp(Decl.pName, delta)) {
      m_Tensors[i].ClearData();
      for (int i1=0; i1<m_Domains[Decl.pDomain[0]].nSize; i1++)
      m_Tensors[i](i1,i1) = 1.0;
    // Overlap Tensors
    } else if (Decl.pName[0] == 'S') {
      ;
    // E4 cumulant Tensors
    } else if ((Decl.pName=="E4")&&(Method.E4cumulant_if_zero==0)){
      E4cumulant(Mem);
    // Anything else in "int"
    } else {
      std::string filename = "int/"+string(Decl.pName)+".npy";
      m_Tensors[i].Sizes.clear();
      m_Tensors[i].Strides.clear();
      ct::ReadNpyData(m_Tensors[i], filename);
    }
  } // end if Density

  // Hamiltonian Tensors
  if (Decl.Usage == USAGE_Hamiltonian) {
    // "int/W:domain.npy" Tensors
    if (0 == strcmp(Decl.pName, "W") ){// && 0==strcmp( string(Decl.pDomain).c_str(), "caca") ) {
      std::string filename = "int/W:"+string(Decl.pDomain)+".npy";
      m_Tensors[i].Sizes.clear();
      m_Tensors[i].Strides.clear();
      ct::ReadNpyData(m_Tensors[i], filename);
    // "int/D:domain.npy" Tensors
    } else if (0 == strcmp(Decl.pName, "D") ){
      std::string filename = "int/D:"+string(Decl.pDomain)+".npy";
      m_Tensors[i].Sizes.clear();
      m_Tensors[i].Strides.clear();
      ct::ReadNpyData(m_Tensors[i], filename);
   // Fock-like Tensors
    } else {
      // "f" or "k" depending on PT type (Fock of CoreH)
      char const
        *pIntNames[] = {"f", "k"};
      ct::FArrayNpy
        *pIntArrays[] = {&Int1e_Fock, &Int1e_CoreH};
      uint
        nIntTerms = sizeof(pIntNames)/sizeof(pIntNames[0]);
      uint k;
      for (k = 0; k < nIntTerms; ++k){
        if (0 == strcmp(Decl.pName, pIntNames[k])) {
         CopyDomainSubset(m_Tensors[i], ViewFromNpy(*pIntArrays[k]), Decl.pDomain, m_Domains);
         break;
        }
      }
      // If did not find "Decl.pName" in pIntNames(f or k), then something's wrong
      if (k == nIntTerms)
       throw std::runtime_error("Hamiltonian term '" + std::string(Decl.pName) + "' not recognized.");
    }
  } // end if Hamiltonian
} // end FJobContext::FillData


//=============================================================================
void FJobContext::InitAmpResPairs(){
//-----------------------------------------------------------------------------
  /*!
    Assign Amplitude/Residual pairs (??)
  */
//-----------------------------------------------------------------------------
  for (uint i = 0; i != Method.nTensorDecls; ++ i) {
    FTensorDecl const
       &iDecl = Method.pTensorDecls[i];
    if (iDecl.Usage != USAGE_Residual)
       break; // through with the residuals. all should have been assigned by now.
    for (uint j = i+1; j != Method.nTensorDecls; ++ j) {
      FTensorDecl const
         &jDecl = Method.pTensorDecls[j];
      if (jDecl.Usage != USAGE_Amplitude)
         continue;
      if (0 != strcmp(iDecl.pDomain, jDecl.pDomain))
         continue;
    }
  }
} // end FJobContext::InitAmpResPairs


//=============================================================================
void FJobContext::ExecEquationSet( FEqSet const &Set, std::vector<FSrciTensor>& m_Tensors, FMemoryStack2 &Mem){
//-----------------------------------------------------------------------------
  /*!
    Execute the Equations (the Tensor contractions) provided by the INL files
  */
//-----------------------------------------------------------------------------
  //std::cout << "! EXEC: " << Set.pDesc << std::endl<<std::flush;
  double time=srci::GetTime();
  int nTerms;

  // For each line of equation
  track_contract_nbr_total=0;
  for (FEqInfo const *pEq = Set.pEqs; pEq != Set.pEqs + Set.nEqs; ++ pEq) {
    //std::cout << "****  "<<pEq->pCoDecl<<std::endl<<std::flush;

    /* enforce order: INIT */
    if (pEq->nTerms<0){
      enforce=true;
      nTerms=-pEq->nTerms;
    } else {
      enforce=false;
      nTerms= pEq->nTerms;
    }

    /* track domains: CREATE */
    track_indent=0;
    track_contract_nbr=0;
    track_domains=new char[100];
    for (int i=0; i<nTerms;++i){
     if (i==0) strcpy(track_domains,Method.pTensorDecls[pEq->iTerms[i]].pDomain);
     else      strcat(track_domains,Method.pTensorDecls[pEq->iTerms[i]].pDomain);
     if (i==0) strcat(track_domains,"=");
     else if (i!=nTerms-1) strcat(track_domains,".");
    };
    if (track_contract) {
      std::cout<<std::flush;
      printf("**** %-20s %-20s %s\n",pEq->pCoDecl,track_domains,std::string(29,'*').c_str());
      std::cout<<std::flush;
    }

    // If more than 3 terms: use ContractN
    if (nTerms != 3) {
      //throw std::runtime_error("expected input equations to be in binary contraction form. One isn't.");

      FNdArrayView **pTs;
      Mem.Alloc(pTs, nTerms);
      for (uint i = 0; i < nTerms; ++i) {
        pTs[i] = &m_Tensors[pEq->iTerms[i]];//TensorById(pEq->iTerms[i]);
        if (Method.pTensorDecls[pEq->iTerms[i]].Storage == STORAGE_Disk) {
          FillData(pEq->iTerms[i], Mem);
        }
        if (i != 0 && pTs[i] == pTs[0]) {
          throw std::runtime_error(boost::str(
                      format("contraction %i has overlapping dest and source tensors."
                             " Tensors may not contribute to contractions involving themselves.") % (pEq - Set.pEqs)));
        }
      }
      ContractN(pTs, pEq->pCoDecl, pEq->Factor, true, Mem);
      Mem.Free(pTs);

     // For 3 terms: use ContractBinary
     } else {
       void *pBaseOfMemory = Mem.Alloc(0);
       FNdArrayView
         *pD = &m_Tensors[pEq->iTerms[0]],
         *pS = &m_Tensors[pEq->iTerms[1]],
         *pT = &m_Tensors[pEq->iTerms[2]];//TensorById(pEq->iTerms[i]);
       for (int j=0; j<3; j++) {
         if (Method.pTensorDecls[pEq->iTerms[j]].Storage == STORAGE_Disk) {
           FillData(pEq->iTerms[j], Mem);
         }
       }
       //*pD = TensorById(pEq->iTerms[0]),
       //*pS = TensorById(pEq->iTerms[1]),
       //*pT = TensorById(pEq->iTerms[2]);
       if (pD == pS || pD == pT)
        throw std::runtime_error(boost::str(format("contraction %i has overlapping dest and source tensors."
           " Tensors may not contribute to contractions involving themselves.") % (pEq - Set.pEqs)));
       ContractBinary(*pD, pEq->pCoDecl, *pS, *pT, pEq->Factor, true, Mem);
       Mem.Free(pBaseOfMemory);
     }

  } //end of "For each line of equation"

  time_contract=time_contract+srci::GetTime()-time;
} // end FJobContext::ExecEquationSet


//=============================================================================
void CopyDomainSubset(FSrciTensor &Out, FSrciTensor const &In, char const *pDomain, FDomainInfoMap &Domains){
//-----------------------------------------------------------------------------
  /*!
    Get a sub-copy of In in Out
  */
//-----------------------------------------------------------------------------
  assert(Out.Rank() == In.Rank());
  // View into the subset of the input array we are to copy.
  FNdArrayView
    InSubset = In;
  for (uint i = 0; i < In.Rank(); ++ i){
    assert(pDomain[i] != 0);
    FDomainInfo const &d = value(Domains, pDomain[i]);
    InSubset.pData += InSubset.Strides[i] * d.iBase;
    assert(InSubset.Sizes[i] >= d.iBase + d.nSize);
    InSubset.Sizes[i] = d.nSize;
    assert(InSubset.Sizes[i] == Out.Sizes[i]);
  }
  assert(pDomain[In.Rank()] == 0);

  Copy(Out, InSubset);
} // end CopyDomainSubset


//=============================================================================
FNdArrayView ViewFromNpy(ct::FArrayNpy &A){
//-----------------------------------------------------------------------------
  /*!
    Make a NdArrayView looking into an ArrayNpy object.
  */
//-----------------------------------------------------------------------------
  FNdArrayView
    Out;
  Out.pData = &A.Data[0];
  for (uint i = 0; i < A.Rank(); ++i){
    Out.Strides.push_back(A.Strides[i]);
    Out.Sizes.push_back(A.Shape[i]);
  }
  return Out;
} // end ViewFromNpy


//=============================================================================
void SplitStackmem(ct::FMemoryStack2 *Mem){
//-----------------------------------------------------------------------------
  /*!
    Distribute remaining memory equally among different threads
  */
//-----------------------------------------------------------------------------
  long originalSize = Mem[0].m_Size;
  long remainingMem = Mem[0].m_Size - Mem[0].m_Pos;
  long memPerThrd = remainingMem/numthrds;
  Mem[0].m_Size = Mem[0].m_Pos+memPerThrd;
  for (int i=1; i<numthrds; i++) {
    Mem[i].m_pData = Mem[i-1].m_pData+Mem[i-1].m_Size;
    Mem[i].m_Pos = 0;
    Mem[i].m_Size = memPerThrd;
  }
  Mem[numthrds-1].m_Size += remainingMem%numthrds;
} // end SplitStackmem


//=============================================================================
void MergeStackmem(ct::FMemoryStack2 *Mem){
//-----------------------------------------------------------------------------
  /*!
    Put all the memory again in the zeroth thrd
  */
//-----------------------------------------------------------------------------
  for (int i=1; i<numthrds; i++) {
    Mem[0].m_Size += Mem[i].m_Size;
    Mem[i].m_pData = 0;
    Mem[i].m_Pos = 0;
    Mem[i].m_Size = 0;
  }
} // end MergeStackmem


//=============================================================================
void FJobContext::Allocate(std::string op, FMemoryStack2 &Mem) {
//-----------------------------------------------------------------------------
  /*!
    Allocate memory
  */
//-----------------------------------------------------------------------------
  size_t len = TND(op)->nValues();
  double scale = 1.0;
  double *data = Mem.AllocN(len, scale);
  TND(op)->pData = data;
} // end FJobContext::Allocate


//=============================================================================
void FJobContext::DeAllocate(std::string op, FMemoryStack2 &Mem) {
//-----------------------------------------------------------------------------
  /*!
    Deallocate memory
  */
//-----------------------------------------------------------------------------
  Mem.Free(TND(op)->pData);
  TND(op)->pData = NULL;
} // end FJobContext::DeAllocate


//=============================================================================
void FJobContext::SaveToDisk(std::string file, std::string op) {
//-----------------------------------------------------------------------------
  /*!
    Write stuff to disk
  */
//-----------------------------------------------------------------------------
  FILE *File = fopen(file.c_str(), "wb");
  fwrite(TND(op)->pData, sizeof(TND(op)->pData[0]), TND(op)->nValues(), File);
  fclose(File);
} // end FJobContext::SaveToDisk


//=============================================================================
static void CopyEps(FDomainInfo &Dom, FScalar EpsFactor, ct::FArrayNpy const &Fock){
//-----------------------------------------------------------------------------
  /*!
    [comment here]
  */
//-----------------------------------------------------------------------------
  for (uint i = Dom.iBase; i != Dom.iBase + Dom.nSize; ++ i)
    Dom.Eps.push_back(EpsFactor * Fock(i,i));
} // end CopyEps


//=============================================================================
FSrciTensor *FJobContext::TND(std::string const &NameAndDomain){
//-----------------------------------------------------------------------------
  /*!
    [comment here]
  */
//-----------------------------------------------------------------------------
  return value(m_TensorsByNameAndDomain, NameAndDomain);
} // end FJobContext::TND


//=============================================================================
FSrciTensor *FJobContext::TN(std::string const &Name){
//-----------------------------------------------------------------------------
  /*!
    [comment here]
  */
//-----------------------------------------------------------------------------
  return value(m_TensorsByName, Name);
} // end FJobContext::TN


//=============================================================================
FSrciTensor *FJobContext::TensorById(int iTensorRef) {
//-----------------------------------------------------------------------------
  /*!
    [comment here]
  */
//-----------------------------------------------------------------------------
  if (iTensorRef < 0 || (unsigned)iTensorRef >= m_Tensors.size())
     throw std::runtime_error(boost::str(format("referenced tensor id %i does not exist.") % iTensorRef));
  return &m_Tensors[(unsigned)iTensorRef];
} // end FJobContext::TensorById


//=============================================================================
void FJobContext::DeleteData(ct::FMemoryStack2 &Mem) {
//-----------------------------------------------------------------------------
  /*!
    [comment here]
  */
//-----------------------------------------------------------------------------
  Mem.Free(m_TensorData);
  m_Tensors.resize(0);
} // end FJobContext::DeleteData


//=============================================================================
void FJobContext::ClearTensors(size_t UsageFlags){
//-----------------------------------------------------------------------------
  /*!
    [comment here]
  */
//-----------------------------------------------------------------------------
  for (uint i = 0; i != Method.nTensorDecls; ++ i) {
    FTensorDecl const
       &iDecl = Method.pTensorDecls[i];
    // Except Flags
    if ((iDecl.Usage & UsageFlags) == 0) continue;
    //std::cout << "clear:" << UsageFlags << "  wiping: iDecl i == " << i << ": " << iDecl.pName << std::endl;
    m_Tensors[i].ClearData();
  }
} // end FJobContext::ClearTensors


//=============================================================================
static void DenomScaleUpdateR(FScalar *pAmp, FScalar *pRes, FArrayOffset *pStride,
                              FArrayOffset *pSize, FArrayOffset RankLeft,
                              FScalar const **ppEps, FScalar CurEps){
//-----------------------------------------------------------------------------
  /*!
    [comment here]

    Note: this assumes that the strides for pAmp and pRes are equal.
    If we create the tensors ourselves (which we do), they are. But
    in general they might not be.
  */
//-----------------------------------------------------------------------------

//  if (RankLeft > 0)
//    for (FArrayOffset i = 0; i < pSize[0]; ++ i)
//      DenomScaleUpdateR(pAmp + i*pStride[0], pRes + i*pStride[0], pStride-1, pSize-1, RankLeft-1, ppEps-1, CurEps + ppEps[0][i]);
//  else if (RankLeft == 2)
//    for (FArrayOffset j = 0; j < pSize[ 0]; ++ j)
//      for (FArrayOffset i = 0; i < pSize[-1]; ++ i) {
//        FArrayOffset ij = j*pStride[0] + i*pStride[-1];
//        pAmp[ij] -= pRes[ij] / (CurEps + ppEps[0][j] + ppEps[-1][i]);
//    }
//  // ^- note: this case is technically redundant.
//  else
//     pAmp[0] -= pRes[0] / CurEps;

  if (RankLeft > 0)
    for (FArrayOffset i = 0; i < pSize[0]; ++ i)
      DenomScaleUpdateR(pAmp + i*pStride[0], pRes + i*pStride[0], pStride-1, pSize-1, RankLeft-1, ppEps-1, CurEps + ppEps[0][i]);
  else if (RankLeft == 2)
    for (FArrayOffset j = 0; j < pSize[ 0]; ++ j)
      for (FArrayOffset i = 0; i < pSize[-1]; ++ i) {
         pRes[j*pStride[0] + i*pStride[-1]] /= (CurEps + ppEps[0][j] + ppEps[-1][i]);
    }
  // ^- note: this case is technically redundant.
  else
     pRes[0] /= CurEps;
} // end DenomScaleUpdateR


//=============================================================================
void FJobContext::PrintTiming(char const *p, FScalar t){
//-----------------------------------------------------------------------------
  /*!
    [comment here]
  */
//-----------------------------------------------------------------------------
  std::cout << format(" Time for %-35s%10.2f sec") % (std::string(p) + ":") % t << std::endl;
} // end FJobContext::PrintTiming


//=============================================================================
void FJobContext::PrintTimingT0(char const *p, FScalar t0){
//-----------------------------------------------------------------------------
  /*!
    [comment here]
  */
//-----------------------------------------------------------------------------
  PrintTiming(p, srci::GetTime() - t0);
} // end FJobContext::PrintTimingT0


//=============================================================================
void FJobContext::PrintResult(std::string const &s, FScalar v, int iState, char const *pAnnotation){
//-----------------------------------------------------------------------------
  /*!
    [comment here]
  */
//-----------------------------------------------------------------------------
  std::stringstream
     Caption;
  if ( iState >= 0 ) {
     Caption << format("!%s STATE %i %s") % MethodName % (iState + 1) % s;
  } else {
     Caption << " " + s;
  }
//std::cout << format("%-23s%20.14f") % Caption.str() % v << std::endl;
  std::cout << format("%-35s%20.14f") % Caption.str() % v;
  if (pAnnotation)
     std::cout << "  " << pAnnotation;
  std::cout << std::endl;
} // end FJobContext::PrintResult


//=============================================================================
void FJobContext::E4cumulant(FMemoryStack2 &Mem){
//-----------------------------------------------------------------------------
  /*!
    Establish E4 from E1,E2 and E3 using the cumulant approximation
  */
//-----------------------------------------------------------------------------
  std::cout<<"BM cumulant E4"<<std::endl;
  FNdArrayView *E1 = TND("E1:aa");
  FNdArrayView *E2 = TND("E2:aaaa");
  FNdArrayView *E3 = TND("E3:aaaaaa");
  FNdArrayView *E4 = TND("E4:aaaaaaaa");

  int nact = m_Domains['a'].nSize;
  double elt;

  for (int q4=0; q4<nact; q4++)
  for (int q3=0; q3<nact; q3++)
  for (int q2=0; q2<nact; q2++)
  for (int q1=0; q1<nact; q1++)
   for (int p4=0; p4<nact; p4++)
   for (int p3=0; p3<nact; p3++)
   for (int p2=0; p2<nact; p2++)
   for (int p1=0; p1<nact; p1++){
    E4->operator()(p1,p2,p3,p4,q1,q2,q3,q4) = 0.0;
    elt = 0.0;

    // 16 E3.E1
    elt = elt
       + E3->operator()(p1,p2,p3,q1,q2,q3) * E1->operator()(p4,q4)
       - E3->operator()(p1,p2,p3,q4,q2,q3) * E1->operator()(p4,q1) * 0.50
       - E3->operator()(p1,p2,p3,q1,q4,q3) * E1->operator()(p4,q2) * 0.50
       - E3->operator()(p1,p2,p3,q1,q2,q4) * E1->operator()(p4,q3) * 0.50
       + E3->operator()(p1,p2,p4,q1,q2,q4) * E1->operator()(p3,q3)
       - E3->operator()(p1,p2,p4,q3,q2,q4) * E1->operator()(p3,q1) * 0.50
       - E3->operator()(p1,p2,p4,q1,q3,q4) * E1->operator()(p3,q2) * 0.50
       - E3->operator()(p1,p2,p4,q1,q2,q3) * E1->operator()(p3,q4) * 0.50
       + E3->operator()(p1,p3,p4,q1,q3,q4) * E1->operator()(p2,q2)
       - E3->operator()(p1,p3,p4,q2,q3,q4) * E1->operator()(p2,q1) * 0.50
       - E3->operator()(p1,p3,p4,q1,q2,q4) * E1->operator()(p2,q3) * 0.50
       - E3->operator()(p1,p3,p4,q1,q3,q2) * E1->operator()(p2,q4) * 0.50
       + E3->operator()(p2,p3,p4,q2,q3,q4) * E1->operator()(p1,q1)
       - E3->operator()(p2,p3,p4,q1,q3,q4) * E1->operator()(p1,q2) * 0.50
       - E3->operator()(p2,p3,p4,q2,q1,q4) * E1->operator()(p1,q3) * 0.50
       - E3->operator()(p2,p3,p4,q2,q3,q1) * E1->operator()(p1,q4) * 0.50;

    // 27 E2.E2
    elt = elt
       + E2->operator()(p1,p2,q1,q2) * E2->operator()(p3,p4,q3,q4)
       - E2->operator()(p1,p2,q1,q3) * E2->operator()(p3,p4,q2,q4) * 0.50
       - E2->operator()(p1,p2,q1,q4) * E2->operator()(p3,p4,q3,q2) * 0.50
       - E2->operator()(p1,p2,q3,q2) * E2->operator()(p3,p4,q1,q4) * 0.50
       - E2->operator()(p1,p2,q4,q2) * E2->operator()(p3,p4,q3,q1) * 0.50
       + E2->operator()(p1,p2,q3,q4) * E2->operator()(p3,p4,q1,q2) / 3.0
       + E2->operator()(p1,p2,q3,q4) * E2->operator()(p3,p4,q2,q1) / 6.0
       + E2->operator()(p1,p2,q4,q3) * E2->operator()(p3,p4,q1,q2) / 6.0
       + E2->operator()(p1,p2,q4,q3) * E2->operator()(p3,p4,q2,q1) / 3.0
       + E2->operator()(p1,p3,q1,q3) * E2->operator()(p2,p4,q2,q4)
       - E2->operator()(p1,p3,q1,q2) * E2->operator()(p2,p4,q3,q4) * 0.50
       - E2->operator()(p1,p3,q1,q4) * E2->operator()(p2,p4,q2,q3) * 0.50
       - E2->operator()(p1,p3,q2,q3) * E2->operator()(p2,p4,q1,q4) * 0.50
       - E2->operator()(p1,p3,q4,q3) * E2->operator()(p2,p4,q2,q1) * 0.50
       + E2->operator()(p1,p3,q2,q4) * E2->operator()(p2,p4,q1,q3) / 3.0
       + E2->operator()(p1,p3,q4,q2) * E2->operator()(p2,p4,q1,q3) / 6.0
       + E2->operator()(p1,p3,q2,q4) * E2->operator()(p2,p4,q3,q1) / 6.0
       + E2->operator()(p1,p3,q4,q2) * E2->operator()(p2,p4,q3,q1) / 3.0
       + E2->operator()(p1,p4,q1,q4) * E2->operator()(p3,p2,q3,q2)
       - E2->operator()(p1,p4,q1,q3) * E2->operator()(p2,p3,q2,q4) * 0.50
       - E2->operator()(p1,p4,q1,q2) * E2->operator()(p2,p3,q4,q3) * 0.50
       - E2->operator()(p1,p4,q3,q4) * E2->operator()(p2,p3,q2,q1) * 0.50
       - E2->operator()(p1,p4,q2,q4) * E2->operator()(p2,p3,q1,q3) * 0.50
       + E2->operator()(p1,p4,q3,q2) * E2->operator()(p2,p3,q4,q1) / 3.0
       + E2->operator()(p1,p4,q2,q3) * E2->operator()(p2,p3,q4,q1) / 6.0
       + E2->operator()(p1,p4,q3,q2) * E2->operator()(p2,p3,q1,q4) / 6.0
       + E2->operator()(p1,p4,q2,q3) * E2->operator()(p2,p3,q1,q4) / 3.0;

    // 36 E2.E1.E1
    elt = elt - 2.0 * (
       + E2->operator()(p1,p2,q1,q2) * (E1->operator()(p3,q3)*E1->operator()(p4,q4)
                                       -E1->operator()(p3,q4)*E1->operator()(p4,q3)*0.5)
       - E2->operator()(p1,p2,q1,q3) * (E1->operator()(p3,q2)*E1->operator()(p4,q4)
                                       -E1->operator()(p3,q4)*E1->operator()(p4,q2)*0.5) * 0.50
       - E2->operator()(p1,p2,q1,q4) * (E1->operator()(p3,q3)*E1->operator()(p4,q2)
                                       -E1->operator()(p3,q2)*E1->operator()(p4,q3)*0.5) * 0.50
       - E2->operator()(p1,p2,q3,q2) * (E1->operator()(p3,q1)*E1->operator()(p4,q4)
                                       -E1->operator()(p3,q4)*E1->operator()(p4,q1)*0.5) * 0.50
       - E2->operator()(p1,p2,q4,q2) * (E1->operator()(p3,q3)*E1->operator()(p4,q1)
                                       -E1->operator()(p3,q1)*E1->operator()(p4,q3)*0.5) * 0.50
       + E2->operator()(p1,p2,q3,q4) *  E1->operator()(p3,q1)*E1->operator()(p4,q2)      * 0.25
       + E2->operator()(p1,p2,q4,q3) *  E1->operator()(p3,q2)*E1->operator()(p4,q1)      * 0.25
       + E2->operator()(p1,p3,q1,q3) * (E1->operator()(p2,q2)*E1->operator()(p4,q4)
                                       -E1->operator()(p2,q4)*E1->operator()(p4,q2)*0.5)
       - E2->operator()(p1,p3,q1,q2) * (E1->operator()(p2,q3)*E1->operator()(p4,q4)
                                       -E1->operator()(p2,q4)*E1->operator()(p4,q3)*0.5) * 0.50
       - E2->operator()(p1,p3,q1,q4) * (E1->operator()(p2,q2)*E1->operator()(p4,q3)
                                       -E1->operator()(p2,q3)*E1->operator()(p4,q2)*0.5) * 0.50
       - E2->operator()(p1,p3,q2,q3) * (E1->operator()(p2,q1)*E1->operator()(p4,q4)
                                       -E1->operator()(p2,q4)*E1->operator()(p4,q1)*0.5) * 0.50
       - E2->operator()(p1,p3,q4,q3) * (E1->operator()(p2,q2)*E1->operator()(p4,q1)
                                       -E1->operator()(p2,q1)*E1->operator()(p4,q2)*0.5) * 0.50
       + E2->operator()(p1,p3,q2,q4) *  E1->operator()(p2,q1)*E1->operator()(p4,q3)      * 0.25
       + E2->operator()(p1,p3,q4,q2) *  E1->operator()(p2,q3)*E1->operator()(p4,q1)      * 0.25
       + E2->operator()(p1,p4,q1,q4) * (E1->operator()(p3,q3)*E1->operator()(p2,q2)
                                       -E1->operator()(p3,q2)*E1->operator()(p2,q3)*0.5)
       - E2->operator()(p1,p4,q1,q3) * (E1->operator()(p3,q4)*E1->operator()(p2,q2)
                                       -E1->operator()(p3,q2)*E1->operator()(p2,q4)*0.5) * 0.50
       - E2->operator()(p1,p4,q1,q2) * (E1->operator()(p3,q3)*E1->operator()(p2,q4)
                                       -E1->operator()(p3,q4)*E1->operator()(p2,q3)*0.5) * 0.50
       - E2->operator()(p1,p4,q3,q4) * (E1->operator()(p3,q1)*E1->operator()(p2,q2)
                                       -E1->operator()(p3,q2)*E1->operator()(p2,q1)*0.5) * 0.50
       - E2->operator()(p1,p4,q2,q4) * (E1->operator()(p3,q3)*E1->operator()(p2,q1)
                                       -E1->operator()(p3,q1)*E1->operator()(p2,q3)*0.5) * 0.50
       + E2->operator()(p1,p4,q3,q2) *  E1->operator()(p3,q1)*E1->operator()(p2,q4)      * 0.25
       + E2->operator()(p1,p4,q2,q3) *  E1->operator()(p3,q4)*E1->operator()(p2,q1)      * 0.25
       );

    // 36 E1.E1.E2
    elt = elt - 2.0 * (
       + (E1->operator()(p1,q1)*E1->operator()(p2,q2)
         -E1->operator()(p1,q2)*E1->operator()(p2,q1)*0.5) * E2->operator()(p3,p4,q3,q4)
       - (E1->operator()(p1,q1)*E1->operator()(p2,q3)
         -E1->operator()(p1,q3)*E1->operator()(p2,q1)*0.5) * E2->operator()(p3,p4,q2,q4) * 0.50
       - (E1->operator()(p1,q1)*E1->operator()(p2,q4)
         -E1->operator()(p1,q4)*E1->operator()(p2,q1)*0.5) * E2->operator()(p3,p4,q3,q2) * 0.50
       - (E1->operator()(p1,q3)*E1->operator()(p2,q2)
         -E1->operator()(p1,q2)*E1->operator()(p2,q3)*0.5) * E2->operator()(p3,p4,q1,q4) * 0.50
       - (E1->operator()(p1,q4)*E1->operator()(p2,q2)
         -E1->operator()(p1,q2)*E1->operator()(p2,q4)*0.5) * E2->operator()(p3,p4,q3,q1) * 0.50
       +  E1->operator()(p1,q3)*E1->operator()(p2,q4)      * E2->operator()(p3,p4,q1,q2) * 0.25
       +  E1->operator()(p1,q4)*E1->operator()(p2,q3)      * E2->operator()(p3,p4,q2,q1) * 0.25
       + (E1->operator()(p1,q1)*E1->operator()(p3,q3)
         -E1->operator()(p1,q3)*E1->operator()(p3,q1)*0.5) * E2->operator()(p2,p4,q2,q4)
       - (E1->operator()(p1,q1)*E1->operator()(p3,q2)
         -E1->operator()(p1,q2)*E1->operator()(p3,q1)*0.5) * E2->operator()(p2,p4,q3,q4) * 0.50
       - (E1->operator()(p1,q1)*E1->operator()(p3,q4)
         -E1->operator()(p1,q4)*E1->operator()(p3,q1)*0.5) * E2->operator()(p2,p4,q2,q3) * 0.50
       - (E1->operator()(p1,q2)*E1->operator()(p3,q3)
         -E1->operator()(p1,q3)*E1->operator()(p3,q2)*0.5) * E2->operator()(p2,p4,q1,q4) * 0.50
       - (E1->operator()(p1,q4)*E1->operator()(p3,q3)
         -E1->operator()(p1,q3)*E1->operator()(p3,q4)*0.5) * E2->operator()(p2,p4,q2,q1) * 0.50
       +  E1->operator()(p1,q2)*E1->operator()(p3,q4)      * E2->operator()(p2,p4,q1,q3) * 0.25
       +  E1->operator()(p1,q4)*E1->operator()(p3,q2)      * E2->operator()(p2,p4,q3,q1) * 0.25
       + (E1->operator()(p1,q1)*E1->operator()(p4,q4)
         -E1->operator()(p1,q4)*E1->operator()(p4,q1)*0.5) * E2->operator()(p3,p2,q3,q2)
       - (E1->operator()(p1,q1)*E1->operator()(p4,q3)
         -E1->operator()(p1,q3)*E1->operator()(p4,q1)*0.5) * E2->operator()(p3,p2,q4,q2) * 0.50
       - (E1->operator()(p1,q1)*E1->operator()(p4,q2)
         -E1->operator()(p1,q2)*E1->operator()(p4,q1)*0.5) * E2->operator()(p3,p2,q3,q4) * 0.50
       - (E1->operator()(p1,q3)*E1->operator()(p4,q4)
         -E1->operator()(p1,q4)*E1->operator()(p4,q3)*0.5) * E2->operator()(p3,p2,q1,q2) * 0.50
       - (E1->operator()(p1,q2)*E1->operator()(p4,q4)
         -E1->operator()(p1,q4)*E1->operator()(p4,q2)*0.5) * E2->operator()(p3,p2,q3,q1) * 0.50
       +  E1->operator()(p1,q3)*E1->operator()(p4,q2)      * E2->operator()(p3,p2,q1,q4) * 0.25
       +  E1->operator()(p1,q2)*E1->operator()(p4,q3)      * E2->operator()(p3,p2,q4,q1) * 0.25
       );

    // 24 E1.E1.E1.E1
    elt = elt + 6.0 * (
       + E1->operator()(p1,q1) * E1->operator()(p2,q2) * E1->operator()(p3,q3) * E1->operator()(p4,q4)
       - E1->operator()(p1,q1) * E1->operator()(p2,q2) * E1->operator()(p3,q4) * E1->operator()(p4,q3) * 0.500
       + E1->operator()(p1,q1) * E1->operator()(p2,q3) * E1->operator()(p3,q4) * E1->operator()(p4,q2) * 0.250
       - E1->operator()(p1,q1) * E1->operator()(p2,q3) * E1->operator()(p3,q2) * E1->operator()(p4,q4) * 0.500
       + E1->operator()(p1,q1) * E1->operator()(p2,q4) * E1->operator()(p3,q2) * E1->operator()(p4,q3) * 0.250
       - E1->operator()(p1,q1) * E1->operator()(p2,q4) * E1->operator()(p3,q3) * E1->operator()(p4,q2) * 0.500
       + E1->operator()(p1,q2) * E1->operator()(p2,q3) * E1->operator()(p3,q1) * E1->operator()(p4,q4) * 0.250
       - E1->operator()(p1,q2) * E1->operator()(p2,q3) * E1->operator()(p3,q4) * E1->operator()(p4,q1) * 0.125
       + E1->operator()(p1,q2) * E1->operator()(p2,q1) * E1->operator()(p3,q4) * E1->operator()(p4,q3) * 0.250
       - E1->operator()(p1,q2) * E1->operator()(p2,q1) * E1->operator()(p3,q3) * E1->operator()(p4,q4) * 0.500
       + E1->operator()(p1,q2) * E1->operator()(p2,q4) * E1->operator()(p3,q3) * E1->operator()(p4,q1) * 0.250
       - E1->operator()(p1,q2) * E1->operator()(p2,q4) * E1->operator()(p3,q1) * E1->operator()(p4,q3) * 0.125
       + E1->operator()(p1,q3) * E1->operator()(p2,q1) * E1->operator()(p3,q2) * E1->operator()(p4,q4) * 0.250
       - E1->operator()(p1,q3) * E1->operator()(p2,q1) * E1->operator()(p3,q4) * E1->operator()(p4,q2) * 0.125
       + E1->operator()(p1,q3) * E1->operator()(p2,q2) * E1->operator()(p3,q4) * E1->operator()(p4,q1) * 0.250
       - E1->operator()(p1,q3) * E1->operator()(p2,q2) * E1->operator()(p3,q1) * E1->operator()(p4,q4) * 0.500
       + E1->operator()(p1,q3) * E1->operator()(p2,q4) * E1->operator()(p3,q1) * E1->operator()(p4,q2) * 0.250
       - E1->operator()(p1,q3) * E1->operator()(p2,q4) * E1->operator()(p3,q2) * E1->operator()(p4,q1) * 0.125
       + E1->operator()(p1,q4) * E1->operator()(p2,q1) * E1->operator()(p3,q3) * E1->operator()(p4,q2) * 0.250
       - E1->operator()(p1,q4) * E1->operator()(p2,q1) * E1->operator()(p3,q2) * E1->operator()(p4,q3) * 0.125
       + E1->operator()(p1,q4) * E1->operator()(p2,q3) * E1->operator()(p3,q2) * E1->operator()(p4,q1) * 0.250
       - E1->operator()(p1,q4) * E1->operator()(p2,q3) * E1->operator()(p3,q1) * E1->operator()(p4,q2) * 0.125
       + E1->operator()(p1,q4) * E1->operator()(p2,q2) * E1->operator()(p3,q1) * E1->operator()(p4,q3) * 0.250
       - E1->operator()(p1,q4) * E1->operator()(p2,q2) * E1->operator()(p3,q3) * E1->operator()(p4,q1) * 0.500
       );

    E4->operator()(p1,p2,p3,p4,q1,q2,q3,q4) = elt;//E4->operator()(p1,p2,p3,p4,q1,q2,q3,q4) + elt;
  }
} // end FJobContext::E4cumulant


//double lambda(const int p1, const int p2, const int q1, const int q2){
//  FNdArrayView *E1 = TND("E1:aa");
//  FNdArrayView *E2 = TND("E2:aaaa");
//   const double value = E2->operator()(p1,p2,q1,q2)
//                      - E1->operator()(p1,q1) * E1->operator()(p2,q2)
//                      + E1->operator()(p1,q2) * E1->operator()(p2,q1) * 0.5;
//   return value;
//}

////=============================================================================
//void FJobContext::E4cumulant_2(FMemoryStack2 &Mem){
////-----------------------------------------------------------------------------
//  /*!
//    Establish E4 from E1,E2 and E3 using the cumulant approximation
//  */
////-----------------------------------------------------------------------------
//  std::cout<<"BM cumulant E4"<<std::endl;
//  FNdArrayView *E1 = TND("E1:aa");
//  FNdArrayView *E2 = TND("E2:aaaa");
//  FNdArrayView *E3 = TND("E3:aaaaaa");
//  FNdArrayView *E4 = TND("E4:aaaaaaaa");
//
//  int nact = m_Domains['a'].nSize;
//  double elt;
//
//  for (int q4=0; q4<nact; q4++)
//  for (int q3=0; q3<nact; q3++)
//  for (int q2=0; q2<nact; q2++)
//  for (int q1=0; q1<nact; q1++)
//   for (int p4=0; p4<nact; p4++)
//   for (int p3=0; p3<nact; p3++)
//   for (int p2=0; p2<nact; p2++)
//   for (int p1=0; p1<nact; p1++){
//    const double part1 = (       E3->operator()(p1,p2,p3,q1,q2,q3) * E1->operator()(p4,q4)
//                         - 0.5 * E3->operator()(p1,p2,p3,q4,q2,q3) * E1->operator()(p4,q1)
//                         - 0.5 * E3->operator()(p1,p2,p3,q1,q4,q3) * E1->operator()(p4,q2)
//                         - 0.5 * E3->operator()(p1,p2,p3,q1,q2,q4) * E1->operator()(p4,q3)
//                         +       E3->operator()(p1,p2,p4,q1,q2,q4) * E1->operator()(p3,q3)
//                         - 0.5 * E3->operator()(p1,p2,p4,q3,q2,q4) * E1->operator()(p3,q1)
//                         - 0.5 * E3->operator()(p1,p2,p4,q1,q3,q4) * E1->operator()(p3,q2)
//                         - 0.5 * E3->operator()(p1,p2,p4,q1,q2,q3) * E1->operator()(p3,q4)
//                         +       E3->operator()(p1,p3,p4,q1,q3,q4) * E1->operator()(p2,q2)
//                         - 0.5 * E3->operator()(p1,p3,p4,q2,q3,q4) * E1->operator()(p2,q1)
//                         - 0.5 * E3->operator()(p1,p3,p4,q1,q2,q4) * E1->operator()(p2,q3)
//                         - 0.5 * E3->operator()(p1,p3,p4,q1,q3,q2) * E1->operator()(p2,q4)
//                         +       E3->operator()(p2,p3,p4,q2,q3,q4) * E1->operator()(p1,q1)
//                         - 0.5 * E3->operator()(p2,p3,p4,q1,q3,q4) * E1->operator()(p1,q2)
//                         - 0.5 * E3->operator()(p2,p3,p4,q2,q1,q4) * E1->operator()(p1,q3)
//                         - 0.5 * E3->operator()(p2,p3,p4,q2,q3,q1) * E1->operator()(p1,q4) );
//
//    const double part2 = ( E2->operator()(p1,p2,q1,q2) * E2->operator()(p3,p4,q3,q4)
//                         - E2->operator()(p1,p2,q1,q3) * E2->operator()(p3,p4,q2,q4) * 0.5
//                         - E2->operator()(p1,p2,q1,q4) * E2->operator()(p3,p4,q3,q2) * 0.5
//                         - E2->operator()(p1,p2,q3,q2) * E2->operator()(p3,p4,q1,q4) * 0.5
//                         - E2->operator()(p1,p2,q4,q2) * E2->operator()(p3,p4,q3,q1) * 0.5
//                         + E2->operator()(p1,p2,q3,q4) * E2->operator()(p3,p4,q1,q2) / 3.0
//                         + E2->operator()(p1,p2,q3,q4) * E2->operator()(p3,p4,q2,q1) / 6.0
//                         + E2->operator()(p1,p2,q4,q3) * E2->operator()(p3,p4,q1,q2) / 6.0
//                         + E2->operator()(p1,p2,q4,q3) * E2->operator()(p3,p4,q2,q1) / 3.0
//                         + E2->operator()(p1,p3,q1,q3) * E2->operator()(p2,p4,q2,q4)
//                         - E2->operator()(p1,p3,q1,q2) * E2->operator()(p2,p4,q3,q4) * 0.5
//                         - E2->operator()(p1,p3,q1,q4) * E2->operator()(p2,p4,q2,q3) * 0.5
//                         - E2->operator()(p1,p3,q2,q3) * E2->operator()(p2,p4,q1,q4) * 0.5
//                         - E2->operator()(p1,p3,q4,q3) * E2->operator()(p2,p4,q2,q1) * 0.5
//                         + E2->operator()(p1,p3,q2,q4) * E2->operator()(p2,p4,q1,q3) / 3.0
//                         + E2->operator()(p1,p3,q4,q2) * E2->operator()(p2,p4,q1,q3) / 6.0
//                         + E2->operator()(p1,p3,q2,q4) * E2->operator()(p2,p4,q3,q1) / 6.0
//                         + E2->operator()(p1,p3,q4,q2) * E2->operator()(p2,p4,q3,q1) / 3.0
//                         + E2->operator()(p1,p4,q1,q4) * E2->operator()(p3,p2,q3,q2)
//                         - E2->operator()(p1,p4,q1,q3) * E2->operator()(p3,p2,q4,q2) * 0.5
//                         - E2->operator()(p1,p4,q1,q2) * E2->operator()(p3,p2,q3,q4) * 0.5
//                         - E2->operator()(p1,p4,q3,q4) * E2->operator()(p3,p2,q1,q2) * 0.5
//                         - E2->operator()(p1,p4,q2,q4) * E2->operator()(p3,p2,q3,q1) * 0.5
//                         + E2->operator()(p1,p4,q3,q2) * E2->operator()(p3,p2,q1,q4) / 3.0
//                         + E2->operator()(p1,p4,q2,q3) * E2->operator()(p3,p2,q1,q4) / 6.0
//                         + E2->operator()(p1,p4,q3,q2) * E2->operator()(p3,p2,q4,q1) / 6.0
//                         + E2->operator()(p1,p4,q2,q3) * E2->operator()(p3,p2,q4,q1) / 3.0 );
//
//    const double part3 = ( lambda(p1,p2,q1,q2) * lambda(p3,p4,q3,q4)
//                         - lambda(p1,p2,q1,q3) * lambda(p3,p4,q2,q4) * 0.5
//                         - lambda(p1,p2,q1,q4) * lambda(p3,p4,q3,q2) * 0.5
//                         - lambda(p1,p2,q3,q2) * lambda(p3,p4,q1,q4) * 0.5
//                         - lambda(p1,p2,q4,q2) * lambda(p3,p4,q3,q1) * 0.5
//                         + lambda(p1,p2,q3,q4) * lambda(p3,p4,q1,q2) / 3.0
//                         + lambda(p1,p2,q3,q4) * lambda(p3,p4,q2,q1) / 6.0
//                         + lambda(p1,p2,q4,q3) * lambda(p3,p4,q1,q2) / 6.0
//                         + lambda(p1,p2,q4,q3) * lambda(p3,p4,q2,q1) / 3.0
//                         + lambda(p1,p3,q1,q3) * lambda(p2,p4,q2,q4)
//                         - lambda(p1,p3,q1,q2) * lambda(p2,p4,q3,q4) * 0.5
//                         - lambda(p1,p3,q1,q4) * lambda(p2,p4,q2,q3) * 0.5
//                         - lambda(p1,p3,q2,q3) * lambda(p2,p4,q1,q4) * 0.5
//                         - lambda(p1,p3,q4,q3) * lambda(p2,p4,q2,q1) * 0.5
//                         + lambda(p1,p3,q2,q4) * lambda(p2,p4,q1,q3) / 3.0
//                         + lambda(p1,p3,q4,q2) * lambda(p2,p4,q1,q3) / 6.0
//                         + lambda(p1,p3,q2,q4) * lambda(p2,p4,q3,q1) / 6.0
//                         + lambda(p1,p3,q4,q2) * lambda(p2,p4,q3,q1) / 3.0
//                         + lambda(p1,p4,q1,q4) * lambda(p3,p2,q3,q2)
//                         - lambda(p1,p4,q1,q3) * lambda(p3,p2,q4,q2) * 0.5
//                         - lambda(p1,p4,q1,q2) * lambda(p3,p2,q3,q4) * 0.5
//                         - lambda(p1,p4,q3,q4) * lambda(p3,p2,q1,q2) * 0.5
//                         - lambda(p1,p4,q2,q4) * lambda(p3,p2,q3,q1) * 0.5
//                         + lambda(p1,p4,q3,q2) * lambda(p3,p2,q1,q4) / 3.0
//                         + lambda(p1,p4,q2,q3) * lambda(p3,p2,q1,q4) / 6.0
//                         + lambda(p1,p4,q3,q2) * lambda(p3,p2,q4,q1) / 6.0
//                         + lambda(p1,p4,q2,q3) * lambda(p3,p2,q4,q1) / 3.0 );
//
//    E4->operator()(p1,p2,p3,p4,q1,q2,q3,q4) = ( part1 - part2 + 2 * part3 );
//  }
//} // end FJobContext::E4cumulant_2


