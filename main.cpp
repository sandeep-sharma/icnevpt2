/*
Developed by Sandeep Sharma and Gerald Knizia, 2016
Copyright (c) 2016, Sandeep Sharma
*/
#include "CxNumpyArray.h"
#include "CxMemoryStack.h"
#include "BlockContract.h"
#include "CxDefs.h"
#include "icpt.h"
#include "mkl.h"
#include "mkl_cblas.h"
using ct::TArray;
using ct::FMemoryStack;
using boost::format;

/* density fitting */
bool df=false;
/* track domains throughout the contractions */
bool track_contract=true;
bool in_handcoded=false;
int  track_contract_nbr=0;
int  track_contract_nbr_total=0;
int  track_indent=0;
char *track_domains=(char*) malloc(100);
/* enforce natural order of contraction */
bool enforce=false;

//=============================================================================
int main(int argc, char const *argv[]){
//=============================================================================
   /*!
     Read arguments and input file

     What we need:
       - CI order, nElec, Ms2  (-> nOccA, nOccB)
       - Tensor declarations
       - Equations (read from .inl files)
       - E0/Fock/Int2e (take from numpy arrays).
         Assume input is already in MO basis.
       - Something to split up tensors (H) and combine them (RDM)
         regarding universal indices
   */
//-----------------------------------------------------------------------------
  mkl_set_num_threads(numthrds);

  FJobContext Job;
  Job.time_total=srci::GetTime();
  //Job.time_total=-srci::GetTime();
  Job.time_contract=0;
  Job.time_ortho=0;
  char const *pInp = "CoMinAo.args";
  std::string MethodOverride;

  ct::FMemoryStack2 Mem[numthrds];
  size_t mb = 1048576;
  Mem[0].Create(Job.WorkSpaceMb*mb);

  // Arguments from Command line
  for (int iArg = 1; iArg < argc; ++ iArg) {
    if (0 == strcmp(argv[iArg], "-m")) {
      ++ iArg;
      if (iArg < argc) MethodOverride = std::string(argv[iArg]);
      continue;
    }
    pInp = argv[iArg];
  }

  // Input File
  Job.ReadInputFile(pInp);
  if (!MethodOverride.empty()) Job.MethodName = MethodOverride;

  // Assign stuff depending on MethodName
  if (Job.MethodName == "MRLCC") {
    std::string methods[] = {"MRLCC_CCVV", "MRLCC_ACVV", "MRLCC_AAVV", "MRLCC_CCAA",
                             "MRLCC_CCAV", "MRLCC_CAAV", "MRLCC_AAAV", "MRLCC_AAAC"};

    for (int i=0; i<6; i++) {
      Job.MethodName = methods[i];
      Job.ReadMethodFile();
      Job.Header(pInp);
      Job.Init(Mem[0]);
      Job.ConjGrad(Mem);
      Job.DeleteData(Mem[0]);
    }
  } else if (Job.MethodName == "NEVPT") {
    std::string methods[] = {"NEVPT_CCVV", "NEVPT_ACVV", "NEVPT_AAVV", "NEVPT_CCAA",
                             "NEVPT_CCAV", "NEVPT_CAAV", "NEVPT_AAAV", "NEVPT_AAAC"};

    for (int i=0; i<6; i++) {
      Job.MethodName = methods[i];
      Job.ReadMethodFile();
      Job.Header(pInp);
      Job.Init(Mem[0]);
      Job.ConjGrad(Mem);
      Job.DeleteData(Mem[0]);
    }
  //} else if((Job.MethodName == "NEVPT3_H")){
  //  Job.ReadMethodFile();
  //  Job.Header(pInp);
  //  Job.Init(Mem[0]);
  //  Job.Calc3RD(Mem[0]);
  //} else if((Job.MethodName == "NEVPT3_H0")){
  //  Job.ReadMethodFile();
  //  Job.Header(pInp);
  //  Job.Init(Mem[0]);
  //  Job.Calc3RD(Mem[0]);
  } else if((Job.MethodName == "MRLCC3")
         or (Job.MethodName == "MRLCC3_DF")
         or (Job.MethodName == "MRLCC3_CUMU")
         or (Job.MethodName == "MRLCC3_DFCUMU")){
    Job.ReadMethodFile();
    Job.Header(pInp);
    Job.Init(Mem[0]);
    Job.Calc3RD(Mem[0]);
  } else {
    Job.ReadMethodFile();
    Job.Header(pInp);
    Job.Init(Mem[0]);
    Job.ConjGrad(Mem);
  }
  Job.time_total=srci::GetTime()-Job.time_total;
  //Job.time_total+=srci::GetTime();
  printf(" Time (ortho):    % 9.4f\n",Job.time_ortho)   ;std::cout<<std::flush;
  printf(" Time (contract): % 9.4f\n",Job.time_contract);std::cout<<std::flush;
  printf(" Time (total):    % 9.4f\n",Job.time_total)   ;std::cout<<std::flush;
} // end main

