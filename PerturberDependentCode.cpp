/*
Developed by Sandeep Sharma and Gerald Knizia, 2016
Copyright (c) 2016, Sandeep Sharma
*/
#include "CxNumpyArray.h"
#include "CxAlgebra.h"
#include "CxMemoryStack.h"
#include "CxPodArray.h"
#include "BlockContract.h"
#include "icpt.h"
#ifdef _OPENMP
#include <omp.h>
#endif
#include <string>

using ct::TArray;
using ct::FMemoryStack2;
using boost::format;
using namespace std;

FNdArrayView ViewFromNpy(ct::FArrayNpy &A);

/* track domains throughout the contractions */
extern bool track_contract;
extern int  track_contract_nbr_total;

//=============================================================================
void FJobContext::InitAmplitudes(FMemoryStack2 &Mem){
//-----------------------------------------------------------------------------
  /*!
     Initialize the Amplitudes

     The amplitude "b" is set to b=S.W.
     This is done mainly by executing the "bVec" part of the ".inl" file.
     [This will soon be achieved by a in-code b=S.W]
  */
//-----------------------------------------------------------------------------
  ClearTensors(USAGE_Amplitude);

  // [debug/testing]
  AddHeff(Mem);

  // The CAAV class has two basis, so the ".inl" file shows
  // two variants of all amplitude tensors (p1,p2, etc...).
  // The usual amplitude tensors p/Ap/b/B are made to be pointers to p1/Ap1/b1/B1.
  if((0 == strcmp(Method.perturberClass, "CAAV"))){
    FNdArrayView *p  = TN("p");
    FNdArrayView *Ap = TN("Ap");
    FNdArrayView *b  = TN("b");
    FNdArrayView *B  = TN("B");
    p ->pData = TN("p1") ->pData;
    Ap->pData = TN("Ap1")->pData;
    b ->pData = TN("b1") ->pData;
    B ->pData = TN("B1") ->pData;
  }

  //[This will soon be achieved by a in-code b=S.W]
  std::cout<<"\n Calculate b=S.W\n"<<std::flush;
  ExecEquationSet(Method.bVec, m_Tensors, Mem);
  /* track domains: PRINT */
  if (track_contract)printf("-----total_contract %i\n",track_contract_nbr_total);

  // [debug: Print out stuff]
  //FNdArrayView *W1 = TND("W:eaca");
  //FNdArrayView *W2 = TND("W:aeca");
  //for (int m=0; m<ncor; m++)
  //  for (int n=0; n<nact; n++)
  //    for (int o=0; o<nact; o++)
  //      for (int p=0; p<nvir; p++){
  //        printf("W1Exec % 5i % 5i % 5i % 5i % 13.6f\n",m,n,o,p,W1->operator()(p,n,m,o));
  //        printf("W2Exec % 5i % 5i % 5i % 5i % 13.6f\n",m,n,o,p,W2->operator()(n,p,m,o));
  //};
  //FNdArrayView *b1 = TND("b1:caae");
  //FNdArrayView *b2 = TND("b2:caae");
  //for (int m=0; m<ncor; m++)
  //  for (int n=0; n<nact; n++)
  //    for (int o=0; o<nact; o++)
  //      for (int p=0; p<nvir; p++){
  //        printf("b1Exec % 5i % 5i % 5i % 5i % 13.6f\n",m,n,o,p,b1->operator()(m,n,o,p));
  //        printf("b2Exec % 5i % 5i % 5i % 5i % 13.6f\n",m,n,o,p,b2->operator()(m,n,o,p));
  //};
  //FNdArrayView *TEST = TN("b");
  //for (int m=0; m<TEST->Sizes[0]; m++)
  //  for (int n=0; n<TEST->Sizes[1]; n++)
  //    for (int o=0; o<TEST->Sizes[2]; o++)
  //      for (int p=0; p<TEST->Sizes[3]; p++){
  //        printf("[b_post_Exec] % 5i % 5i % 5i % 5i % 13.6f\n",m,n,o,p,TEST->operator()(m,n,o,p));
  //}

  // [debug/testing]
  AddHeffIntob(Mem);

  // About the starting guess
  ct::FArrayNpy guess;
  if (guessInput.compare("") != 0){
    FScalar zero = 0.0;
    PrintResult("Reading guess from "+guessInput, zero, 0);
    ct::ReadNpy( guess ,guessInput);
    Copy(*TN("T"), ViewFromNpy(guess));  // T<-Guess
  } else {
    Copy(*TN("t"), *TN("b"));            // t<-b
  }

} // end FJobContext::InitAmplitudes


//=============================================================================
void FJobContext::MakeXhalf(std::string stringS, int size){
//-----------------------------------------------------------------------------
  /*!
    Make the unitary transformation from and to Ortho basis

    Returns "V=U.D^1/2" into the array given as "stringS".
       U: eigenvectors of the overlap given in input as "stringS"
       D: eigenvalues  of the overlap given in input as "stringS"
  */
//-----------------------------------------------------------------------------
  std::cout << " Calculate V=U.D^1/2\n";

  // [debug: Print out stuff]
  //printf("[size] %i\n",size);
  //for (int i=0; i<size; i++)
  //  for (int j=0; j<size; j++){
  //    printf("[overlap(S)] % 5i % 5i % 13.6f\n",i,j,TN(stringS)->pData[i+j*size]);
  //  }

  // Eigensystem (U,D) of the overlap -------------------------
  FScalar *eigen = (FScalar*)::malloc(sizeof(FScalar)*size);
  ct::Diagonalize(eigen, TN(stringS)->pData, size, size);

  // [debug: Print out eigenvec and eigenval]
  //for (int i=0; i<size; i++)
  //  printf("[eigval] % 5i % 13.8e\n",i,eigen[i]);
  //for (int i=0; i<size; i++)
  //  for (int j=0; j<size; j++){
  //    printf("[eigvec] % 5i % 5i % 13.6f\n",i,j,TN(stringS)->pData[i+j*size]);
  //}

  // [debug: Test U.U^t = Id]
  //FScalar *test = (FScalar*)::malloc(sizeof(FScalar)*size*size);
  //for (int i=0; i<size; i++)
  //  for (int j=0; j<size; j++){
  //    test[i+j*size]=0.0;
  //    for (int k=0; k<size; k++){
  //      test[i+j*size]+=TN(stringS)->pData[i+k*size]*TN(stringS)->pData[j+k*size];
  //    }
  //    if (fabs(test[i+j*size])>0.0000001)
  //    printf("[should_be_Id] [U.Ut] [%s] % 5i % 5i % 13.6f\n",stringS.c_str(),i,j,test[i+j*size]);
  //}

  // Construct V=U.D^1/2 -------------------------------------
  bool inside=false;
  for (int i=0; i<size; i++){
    if (eigen[i] < -ThrTrun){
      printf("   [negative!!!] % 5i % 13.6e\n",i,eigen[i]);
      //throw std::runtime_error("Trying to sqrt a negative eigenval in PerturberDependentCode.cpp:MakeXhalf");
      eigen[i]=0.0;
      for (int j=0; j<size; j++)
        TN(stringS)->pData[j+i*size] = 0.0;
    } else if (eigen[i] < ThrTrun){
      inside=true;
      //printf("   [insideThres] % 5i % 13.6e\n",i,eigen[i]);
      eigen[i]=0.0;
      for (int j=0; j<size; j++)
        TN(stringS)->pData[j+i*size] = 0.0;
    } else {
      for (int j=0; j<size; j++)
        TN(stringS)->pData[j+i*size] = TN(stringS)->pData[j+i*size]/(pow(eigen[i],0.5));
    }
  }
  if (inside)  printf("   [there are values inside ThrTrun: set to zero!]\n");
  std::cout<<std::flush;

  // [debug: Print S^1/2]
  //for (int i=0; i<size; i++)
  //  for (int j=0; j<size; j++){
  //    printf("[V] % 5i % 5i % 13.6f\n",i,j,TN(stringS)->pData[i+j*size]);
  //}

  // [debug: Test V.D.V^t = Id ]
  //FScalar *test = (FScalar*)::malloc(sizeof(FScalar)*size*size);
  //for (int i=0; i<size; i++)
  //  for (int j=0; j<size; j++){
  //    test[i+j*size]=0.0;
  //    for (int k=0; k<size; k++){
  //      test[i+j*size]+=TN(stringS)->pData[i+k*size]*TN(stringS)->pData[j+k*size]*eigen[k];
  //    }
  //    if (fabs(test[i+j*size])>0.0000001)
  //    printf("[should_be_Id] [V.D.Vt] [%s] % 5i % 5i % 13.6e\n",stringS.c_str(),i,j,test[i+j*size]);
  //}

  ::free(eigen);
} // end FJobContext::MakeXhalf

//=============================================================================
void FJobContext::MakeOverlapAndOrthogonalBasis(ct::FMemoryStack2 *Mem){
//-----------------------------------------------------------------------------
  /*!
    Construct:
     1\ the overlap (see Eqs in part IV of AngCimMal-JCP-2002)
     2\ the amplitude b=S.W
     3\ the orthogonal basis V=U.D^1/2
   
    The overlaps are in S1 and possibly in S2 is there is the need.
   
    The overlaps always have only active indexes.
   
    The overlaps may have a block structure to handle
    different cases such as a<b, a>b or i<j, i>j, etc....
    [I think in most cases all can be done with S1=S-S^T an S2=S+S^T]
   
    Summary:
      AAVV: block structure is avoided by constructing S1=S-S^T an S2=S+S^T
      CCAA: block structure is avoided by constructing S1=S-S^T an S2=S+S^T
      ACVV: block structure for a<b, a>b and S2 for a=b [ongoing ever to get rid of that!]
      CCAV: block structure for i<j, i>j and S2 for i=j [ongoing ever to get rid of that!]
      AAAV: no block structure, no S2
      AAAC: no block structure, no S2
      CAAV: block structure to handle the two different basis, no S2
  */
//-----------------------------------------------------------------------------
  double time=srci::GetTime();
  std::cout << "\n\n Calculate Orthogonal Basis\n";

  int nact = m_Domains['a'].nSize;
  int nvir = m_Domains['e'].nSize;
  int ncor = m_Domains['c'].nSize;

  if (0 == strcmp(Method.perturberClass, "AAVV")){
    // 1\ Construct S1=E2-E2t and S2=E2+E2t
    FNdArrayView *S1 = TN("S1"); S1->ClearData();
    FNdArrayView *S2 = TN("S2"); S2->ClearData();
    FNdArrayView *E2 = TND("E2:aaaa");
    for (int s=0; s<nact; s++)
      for (int r=0; r<nact; r++)
        for (int q=0; q<nact; q++)
          for (int p=0; p<nact; p++){
              double S =E2->operator()(p,q,r,s);
              double St=E2->operator()(p,q,s,r);
              S1->operator()(p,q,r,s) = 0.5*(S-St);
              S2->operator()(p,q,r,s) = 0.5*(S+St);
          }

    //[debug/testing]
    //[to_come]// 2\ Construct the amplitude "b", using "BackToNonOrthogonal".
    //[to_come]//    (In this context S1 and S2 are not the orthogonal basis, but plain S1 and S2.)
    //[to_come]ct::Scale(S1->pData, -1.0,S1->nValues());
    //[to_come]BackToNonOrthogonal(std::string("b:eeaa"),std::string("W:eeaa"));
    //[to_come]ct::Scale(S1->pData, -1.0,S1->nValues());

    // 3\ Construct V=U.D^-1/2
    MakeXhalf("S1", nact*nact);
    MakeXhalf("S2", nact*nact);
  }
  
  if (0 == strcmp(Method.perturberClass, "CCAA")){
    // 1\ Construct S1=\tilde{E2}
    FNdArrayView *S1 = TN("S1"); S1->ClearData();
    FNdArrayView *S2 = TN("S2"); S2->ClearData();
    FNdArrayView *E2 = TND("E2:aaaa");
    FNdArrayView *E1 = TND("E1:aa");
    for (int s=0; s<nact; s++)
      for (int r=0; r<nact; r++)
        for (int q=0; q<nact; q++)
          for (int p=0; p<nact; p++){
                                 S1->operator()(p,q,r,s)+= 1.0*E2->operator()(p,q,r,s);
            if  (q==s)           S1->operator()(p,q,r,s)+=-2.0*E1->operator()(p,r);
            if  (q==r)           S1->operator()(p,q,r,s)+= 1.0*E1->operator()(p,s);
            if  (p==s)           S1->operator()(p,q,r,s)+= 1.0*E1->operator()(q,r);
            if  (p==r)           S1->operator()(p,q,r,s)+=-2.0*E1->operator()(q,s);
            if ((p==s)and(q==r)) S1->operator()(p,q,r,s)+=-2.0;
            if ((p==r)and(q==s)) S1->operator()(p,q,r,s)+= 4.0;
                                 S2->operator()(p,q,r,s)+= 1.0*E2->operator()(p,q,s,r);
                                 S2->operator()(p,q,r,s)+= 1.0*E2->operator()(p,q,r,s);
            if  (q==s)           S2->operator()(p,q,r,s)+=-1.0*E1->operator()(p,r);
            if  (q==r)           S2->operator()(p,q,r,s)+=-1.0*E1->operator()(p,s);
            if  (p==s)           S2->operator()(p,q,r,s)+=-1.0*E1->operator()(q,r);
            if  (p==r)           S2->operator()(p,q,r,s)+=-1.0*E1->operator()(q,s);
            if ((p==s)and(q==r)) S2->operator()(p,q,r,s)+= 2.0;
            if ((p==r)and(q==s)) S2->operator()(p,q,r,s)+= 2.0;
          }

    //[debug/testing]
    //[to_come]// 2\ Construct the amplitude "b".
    //[to_come]//    In this case, the use of "MakeOrthogonal" or "BackToNonOrthogonal" with S(+/-)S^t
    //[to_come]//    does not give a suitable "b" vector : instead, an explicit b=S.W is done.
    //[to_come]FNdArrayView *W = TND("W:ccaa");
    //[to_come]FNdArrayView *b = TN("b"); b->ClearData();
    //[to_come]for (int r=0; r<nact; r++)
    //[to_come]  for (int s=0; s<nact; s++)
    //[to_come]    for (int j=0; j<b->Sizes[0]; j++)
    //[to_come]      for (int i=0; i<b->Sizes[0]; i++)
    //[to_come]        for (int q=0; q<nact; q++)
    //[to_come]          for (int p=0; p<nact; p++){
    //[to_come]            b->operator()(i,j,r,s)+=S1->operator()(p,q,r,s)*W->operator()(i,j,p,q);
    //[to_come]          }

    //[debug/testing]
    //// Now, construction of S1=\tilde{E2}-\tilde{E2}^t and S2=\tilde{E2}+\tilde{E2}^t
    //for (int s=0; s<nact; s++)
    //  for (int r=0; r<nact; r++)
    //    for (int q=0; q<nact; q++)
    //      for (int p=0; p<nact; p++){
    //        double S =S1->operator()(p,q,r,s);
    //        double St=S1->operator()(p,q,s,r);
    //        S1->operator()(p,q,r,s)=0.5*(S-St);
    //        S2->operator()(p,q,r,s)=0.5*(S+St);
    //      }

    // 3\ Construct V=U.D^-1/2
    MakeXhalf("S1", nact*nact);
    MakeXhalf("S2", nact*nact);
  } 
  
  if (0 == strcmp(Method.perturberClass, "ACVV")){
    // 1\ [debug/testing]
    //    ongoing tries to avoid the block structure]
    FNdArrayView *S1 = TND("S1:AA"); S1->ClearData();
    FNdArrayView *S2 = TND("S2:aa"); S2->ClearData();
    FNdArrayView *E1 = TND("E1:aa");
    for (int q=0; q<nact; q++)
      for (int p=0; p<nact; p++){
        S1->operator()(p     ,q)      =  2.0*E1->operator()(p,q);
        S1->operator()(p+nact,q)      = -1.0*E1->operator()(p,q);
        S1->operator()(p     ,q+nact) = -1.0*E1->operator()(p,q);
        S1->operator()(p+nact,q+nact) =  2.0*E1->operator()(p,q);
        S2->operator()(p     ,q)      =  1.0*E1->operator()(p,q);
      }

    //[debug/testing]
    //[to_come]// 2\ Construct the amplitude "b", using "MakeOrthogonal".
    //[to_come]//    (In this context S1 and S2 are not the orthogonal basis, but plain S1 and S2.)
    //[to_come]MakeOrthogonal(std::string("W:eeca"),std::string("b:eeca"));

    // 3\ Construct V=U.D^-1/2
    MakeXhalf("S1", 2*nact);
    MakeXhalf("S2", nact);
  } 
  
  if (0 == strcmp(Method.perturberClass, "CCAV")){
    // 1\ [debug/testing]
    //    ongoing tries to avoid the block structure]
    FNdArrayView *S1 = TND("S1:AA"); S1->ClearData();
    FNdArrayView *S2 = TND("S2:aa"); S2->ClearData();
    FNdArrayView *E1 = TND("E1:aa");
    for (int p=0; p<nact; p++)
      for (int q=0; q<nact; q++){
          S1->operator()(q     ,p)      += -2.0*E1->operator()(p,q);
          S1->operator()(q+nact,p)      +=  1.0*E1->operator()(p,q);
          S1->operator()(q     ,p+nact) +=  1.0*E1->operator()(p,q);
          S1->operator()(q+nact,p+nact) += -2.0*E1->operator()(p,q);
          S2->operator()(q     ,p)      += -1.0*E1->operator()(p,q);
        if (p==q){
          S1->operator()(q     ,p)      +=  4.0;
          S1->operator()(q+nact,p)      += -2.0;
          S1->operator()(q     ,p+nact) += -2.0;
          S1->operator()(q+nact,p+nact) +=  4.0;
          S2->operator()(q     ,p)      +=  2.0;
        }
      }

    //[debug/testing]
    //[to_come]// 2\ Construct the amplitude "b", using "MakeOrthogonal".
    //[to_come]//    (In this context S1 and S2 are not the orthogonal basis, but plain S1 and S2.)
    //[to_come]MakeOrthogonal(std::string("W:ccae"),std::string("b:ccae"));

    // 3\ Construct V=U.D^-1/2
    MakeXhalf("S1", 2*nact);
    MakeXhalf("S2", nact);
  } 
  
  if (0 == strcmp(Method.perturberClass, "AAAV")){
    // 1\ Construct S1 (no block structure, no tricks)
    FNdArrayView *S1=TN("S1"); S1->ClearData();
    FNdArrayView *E1=TN("E1");
    FNdArrayView *E2=TN("E2");
    FNdArrayView *E3=TN("E3");
    for (int r=0; r<nact; r++)
      for (int q=0; q<nact; q++)
        for (int p=0; p<nact; p++)
    for (int w=0; w<nact; w++)
      for (int v=0; v<nact; v++)
        for (int u=0; u<nact; u++){
            // < E_u^w E_b^v E_q^a E_r^p >
                                 S1->operator()(u,v,w,  p,q,r)+= 1.0*E3->operator()(p,v,w,r,q,u);
            if  (v==u)           S1->operator()(u,v,w,  p,q,r)+= 1.0*E2->operator()(p,w,r,q);
            if  (p==u)           S1->operator()(u,v,w,  p,q,r)+= 1.0*E2->operator()(v,w,q,r);
            if  (q==p)           S1->operator()(u,v,w,  p,q,r)+= 1.0*E2->operator()(v,w,r,u);
            if ((q==p)and(v==u)) S1->operator()(u,v,w,  p,q,r)+= 1.0*E1->operator()(w,r);
        };

    //[debug/testing]
    //[to_come]// 2\ Construct the amplitude "b", using "MakeOrthogonal".
    //[to_come]//    (In this context S1 is not the orthogonal basis, but plain S1.)
    //[to_come]MakeOrthogonal(std::string("W:eaaa"),std::string("b:eaaa"));

    // 3\ Construct V=U.D^-1/2
    MakeXhalf("S1", nact*nact*nact);
  } 
  
  if (0 == strcmp(Method.perturberClass, "AAAC")){
    // 1\ Construct S1 (no block structure, no tricks)
    FNdArrayView *S1=TN("S1"); S1->ClearData();
    FNdArrayView *E1=TN("E1");
    FNdArrayView *E2=TN("E2");
    FNdArrayView *E3=TN("E3");
    for (int r=0; r<nact; r++)
      for (int q=0; q<nact; q++)
        for (int p=0; p<nact; p++)
    for (int w=0; w<nact; w++)
      for (int v=0; v<nact; v++)
        for (int u=0; u<nact; u++){
            // < E_u^w E_v^j E_i^q E_r^p >
                                 S1->operator()(u,v,w,  p,q,r)+=-1.0*E3->operator()(p,q,w,r,v,u);
            if  (q==u)           S1->operator()(u,v,w,  p,q,r)+=-1.0*E2->operator()(p,w,r,v);
            if  (p==u)           S1->operator()(u,v,w,  p,q,r)+=-1.0*E2->operator()(q,w,v,r);
            if  (v==p)           S1->operator()(u,v,w,  p,q,r)+=-1.0*E2->operator()(q,w,r,u);
            if ((v==p)and(q==u)) S1->operator()(u,v,w,  p,q,r)+=-1.0*E1->operator()(w,r);
            if  (q==v)           S1->operator()(u,v,w,  p,q,r)+= 2.0*E2->operator()(p,w,r,u);
            if ((u==p)and(q==v)) S1->operator()(u,v,w,  p,q,r)+= 2.0*E1->operator()(w,r);
        };

    //[debug/testing]
    //[to_come]// 2\ Construct the amplitude "b", using "MakeOrthogonal".
    //[to_come]//    (In this context S1 is not the orthogonal basis, but plain S1.)
    //[to_come]MakeOrthogonal(std::string("W:caaa"),std::string("b:caaa"));

    // 3\ Construct V=U.D^-1/2
    MakeXhalf("S1", nact*nact*nact);
  } 
  
  if (0 == strcmp(Method.perturberClass, "CAAV")){
    FNdArrayView *S1=TN("S1"); S1->ClearData();
    FNdArrayView *E1=TN("E1");
    FNdArrayView *E2=TN("E2");
    //original//
    S1->Sizes.resize(0);S1->Strides.resize(0);
    S1->Sizes.push_back(2*nact*nact); S1->Sizes.push_back(2*nact*nact);
    S1->Strides.push_back(1); S1->Strides.push_back(2*nact*nact);
    int fullrange = nact*nact;
    for (int r=0; r<nact; r++)
    for (int s=0; s<nact; s++)
    for (int q=0; q<nact; q++)
    for (int p=0; p<nact; p++){
      if (p==r)   S1->operator()(p+q*nact          , r+s*nact          ) +=  2.0*E1->operator()(s,q);
                  S1->operator()(p+q*nact          , r+s*nact          ) +=  2.0*E2->operator()(p,s,q,r);
      if (p==r)   S1->operator()(p+q*nact+fullrange, r+s*nact          ) += -1.0*E1->operator()(s,q);
                  S1->operator()(p+q*nact+fullrange, r+s*nact          ) += -1.0*E2->operator()(p,s,q,r);
      if (p==r)   S1->operator()(r+s*nact          , p+q*nact+fullrange) += -1.0*E1->operator()(s,q);
                  S1->operator()(r+s*nact          , p+q*nact+fullrange) += -1.0*E2->operator()(p,s,q,r);
      if (p==r)   S1->operator()(p+q*nact+fullrange, r+s*nact+fullrange) +=  2.0*E1->operator()(s,q);
                  S1->operator()(p+q*nact+fullrange, r+s*nact+fullrange) += -1.0*E2->operator()(p,s,r,q);
    }

    //[debug/testing]
    //S1 is not ReShaped//
    //for (int q=0; q<nact; q++)
    //  for (int p=0; p<nact; p++)
    //    for (int s=0; s<nact; s++)
    //      for (int r=0; r<nact; r++){
    //          S1->operator()(r     ,s,p     ,q) +=  2.0*E2->operator()(p,s,q,r);
    //          S1->operator()(r+nact,s,p     ,q) += -1.0*E2->operator()(p,s,q,r);
    //          S1->operator()(r     ,s,p+nact,q) += -1.0*E2->operator()(p,s,q,r);
    //          S1->operator()(r+nact,s,p+nact,q) += -1.0*E2->operator()(p,s,r,q);
    //        if (p==r){
    //          S1->operator()(r     ,s,p     ,q) +=  2.0*E1->operator()(s,q);
    //          S1->operator()(r+nact,s,p     ,q) += -1.0*E1->operator()(s,q);
    //          S1->operator()(r     ,s,p+nact,q) += -1.0*E1->operator()(s,q);
    //          S1->operator()(r+nact,s,p+nact,q) +=  2.0*E1->operator()(s,q);
    //        };
    //      }
    //// [TEST]
    //FNdArrayView *W1 = TND("W:eaca");
    //FNdArrayView *W2 = TND("W:aeca");
    ////FNdArrayView *R  = TND("R:cAae"); R->ClearData();
    //for (int i=0; i<ncor; i++)
    //  for (int r=0; r<nact; r++)
    //    for (int s=0; s<nact; s++)
    //      for (int a=0; a<nvir; a++){
    //        //R->operator()(i,r     ,s,a)=W1->operator()(a,r,i,s);
    //        //R->operator()(i,r+nact,s,a)=W2->operator()(r,a,i,s);
    //        //printf("W1 % 5i % 5i % 5i % 5i % 13.6f\n",i,r,s,a,R->operator()(i,r     ,s,a));
    //        //printf("W2 % 5i % 5i % 5i % 5i % 13.6f\n",i,r,s,a,R->operator()(i,r+nact,s,a));
    //        printf("W1Make % 5i % 5i % 5i % 5i % 13.6f\n",i,r,s,a,W1->operator()(a,r,i,s));
    //        printf("W2Make % 5i % 5i % 5i % 5i % 13.6f\n",i,r,s,a,W2->operator()(r,a,i,s));
    //      }
    ////MakeOrthogonal(std::string("R:cAae"),std::string("B:cAae"));
    ////FNdArrayView *B  = TND("B:cAae");
    //FNdArrayView *B1 = TND("B1:caae"); B1->ClearData();
    //FNdArrayView *B2 = TND("B2:caae"); B2->ClearData();
    //for (int i=0; i<ncor; i++)
    //  for (int p=0; p<nact; p++)
    //    for (int q=0; q<nact; q++)
    //      for (int a=0; a<nvir; a++){
    //        for (int r=0; r<nvir; r++)
    //        for (int s=0; s<nvir; s++){
    //          B1->operator()(i,p,q,a)+= 2.0*E2->operator()(r,q,s,p)*W1->operator()(a,r,i,s);
    //          B1->operator()(i,p,q,a)+=-1.0*E2->operator()(r,q,s,p)*W2->operator()(r,a,i,s);
    //          B2->operator()(i,p,q,a)+=-1.0*E2->operator()(r,q,s,p)*W1->operator()(a,r,i,s);
    //          B2->operator()(i,p,q,a)+=-1.0*E2->operator()(r,q,p,s)*W2->operator()(r,a,i,s);
    //          if (p==r){
    //          B1->operator()(i,p,q,a)+= 2.0*E1->operator()(s,q)    *W1->operator()(a,r,i,s);
    //          B1->operator()(i,p,q,a)+=-1.0*E1->operator()(s,q)    *W2->operator()(r,a,i,s);
    //          B2->operator()(i,p,q,a)+=-1.0*E1->operator()(s,q)    *W1->operator()(a,r,i,s);
    //          B2->operator()(i,p,q,a)+= 2.0*E1->operator()(s,q)    *W2->operator()(r,a,i,s);
    //          }
    //        }
    //        //B1->operator()(i,p,q,a)=B->operator()(i,p     ,q,a);
    //        //B2->operator()(i,p,q,a)=B->operator()(i,p+nact,q,a);
    //        //printf("[b_post_MAKE] % 5i % 5i % 5i % 5i % 13.6f\n",i,n,q,a,B->operator()(i,p,q,a));
    //        printf("b1Make % 5i % 5i % 5i % 5i % 13.6f\n",i,p,q,a,B1->operator()(i,p,q,a));
    //        printf("b2Make % 5i % 5i % 5i % 5i % 13.6f\n",i,p,q,a,B2->operator()(i,p,q,a));
    //  }
    //B1->ClearData();
    //B2->ClearData();
    ////R->ClearData();
    ////B->ClearData();

    // 3\ Construct V=U.D^-1/2
    MakeXhalf("S1", 2*nact*nact);
  }

  //[debug/testing]
  //FNdArrayView *TEST = TN("b");
  //for (int m=0; m<TEST->Sizes[0]; m++)
  //  for (int n=0; n<TEST->Sizes[1]; n++)
  //    for (int o=0; o<TEST->Sizes[2]; o++)
  //      for (int p=0; p<TEST->Sizes[3]; p++){
  //        printf("[b_post_MAKE] % 5i % 5i % 5i % 5i % 13.6f\n",m,n,o,p,TEST->operator()(m,n,o,p));
  //  }

  std::cout<<std::flush;
  time_ortho=time_ortho+srci::GetTime()-time;
} // end FJobContext::MakeOverlapAndOrthogonalBasis

//=============================================================================
void FJobContext::MakeOrthogonal(std::string StringA, std::string StringB){
//-----------------------------------------------------------------------------
  /*!
    Make "StringA" orthogonal, into "StringB".
    This is "StringB"= (U.D^1/2)^t."StringA"
    where (U.D^1/2) is in S1 (and possibly S2).
  */
//-----------------------------------------------------------------------------
  double time=srci::GetTime();
  int nact = m_Domains['a'].nSize;
  int nvir = m_Domains['e'].nSize;
  int ncor = m_Domains['c'].nSize;

  FNdArrayView *t  = NULL;
  FNdArrayView *Ta = NULL;
  if (StringA.find(":")!=std::string::npos) t  = TND(StringA);
  else t  = TN(StringA);
  if (StringB.find(":")!=std::string::npos) Ta = TND(StringB);
  else Ta = TN(StringB);
  Ta->ClearData();

  if (0 == strcmp(Method.perturberClass, "AAVV")){
    FNdArrayView *S1 = TN("S1");
    FNdArrayView *S2 = TN("S2");
#pragma omp parallel for schedule(dynamic)
    for (int r=0; r<nact; r++)
      for (int s=0; s<nact; s++)
        for (int c=0; c<nvir; c++)
          for (int b=c; b<nvir; b++)
            for (int q=0; q<nact; q++)
              for (int p=0; p<nact; p++){
                if (b == c)
                  Ta->operator()(b,c,r,s) += S2->operator()(p,q,r,s)*t->operator()(b,c,p,q);
                else {
                  Ta->operator()(b,c,r,s) +=-S1->operator()(p,q,r,s)*t->operator()(b,c,p,q)
                                          +  S1->operator()(p,q,r,s)*t->operator()(c,b,p,q);
                  Ta->operator()(c,b,r,s) += S2->operator()(p,q,r,s)*t->operator()(b,c,p,q)
                                          +  S2->operator()(p,q,r,s)*t->operator()(c,b,p,q);
                }
              }
  } 
  
  if (0 == strcmp(Method.perturberClass, "CCAA")){
    FNdArrayView *S1 = TN("S1");
    FNdArrayView *S2 = TN("S2");
    for (int r=0; r<nact; r++)
      for (int s=0; s<nact; s++)
        for (int j=0; j<ncor; j++)
          for (int i=j; i<ncor; i++)
            for (int q=0; q<nact; q++)
              for (int p=0; p<nact; p++){
                //[correct_bug]
   	         if (i == j)
   	           Ta->operator()(i,j,r,s) += S2->operator()(p,q,r,s)*t->operator()(i,j,p,q);
   	         else {
   	           Ta->operator()(i,j,r,s) += S1->operator()(p,q,r,s)*t->operator()(i,j,p,q);
   	           Ta->operator()(j,i,r,s)  = 0.0;
   	         }
                //[debug/testing]
                //if (i == j)
                //  Ta->operator()(i,j,r,s) += S2->operator()(p,q,r,s)*t->operator()(i,j,p,q);
                // else {
                //  Ta->operator()(i,j,r,s) +=-S1->operator()(p,q,r,s)*t->operator()(i,j,p,q)
                //                          +  S1->operator()(p,q,r,s)*t->operator()(j,i,p,q);
                //  Ta->operator()(j,i,r,s) += S2->operator()(p,q,r,s)*t->operator()(i,j,p,q)
                //                          +  S2->operator()(p,q,r,s)*t->operator()(j,i,p,q);
                //}
              }
  } 
  
  if (0 == strcmp(Method.perturberClass, "ACVV")){
    FNdArrayView *S1 = TN("S1");
    FNdArrayView *S2 = TN("S2");
    for (int q=0; q<nact; q++)
      for (int i=0; i<ncor; i++)
        for (int c=0; c<nvir; c++)
          for (int b=c; b<nvir; b++)
            for (int p=0; p<nact; p++){
              if (b == c)
                Ta->operator()(b,c,i,q) += S2->operator()(p     ,q)     *t->operator()(b,c,i,p);
              else {
                // [debug/testing]
                // ongoing tries to avoid the block structure]
                //Ta->operator()(b,c,i,q) += -S2->operator()(p     ,q)     *t->operator()(b,c,i,p)/sqrt(3)
                //                        +   S2->operator()(p     ,q)     *t->operator()(c,b,i,p)/sqrt(3);
                //Ta->operator()(c,b,i,q) +=  S2->operator()(p     ,q     )*t->operator()(b,c,i,p)
                //                        +   S2->operator()(p     ,q     )*t->operator()(c,b,i,p)            ;
                  Ta->operator()(b,c,i,q) += S1->operator()(p     ,q)     *t->operator()(b,c,i,p)
                                          +  S1->operator()(p+nact,q)     *t->operator()(c,b,i,p);
                  Ta->operator()(c,b,i,q) += S1->operator()(p     ,q+nact)*t->operator()(b,c,i,p)
                                          +  S1->operator()(p+nact,q+nact)*t->operator()(c,b,i,p);
              }
            }
  } 
  
  if (0 == strcmp(Method.perturberClass, "CCAV")){
    FNdArrayView *S1 = TN("S1");
    FNdArrayView *S2 = TN("S2");
    for (int a=0; a<nvir; a++)
      for (int p=0; p<nact; p++)
        for (int i=0; i<ncor; i++)
          for (int j=0; j<i+1; j++)
            for (int q=0; q<nact; q++){
              if (i == j)
                Ta->operator()(i,j,p,a) += S2->operator()(q     ,p)     *t->operator()(i,j,q,a);
              else {
                Ta->operator()(i,j,p,a) += S1->operator()(q     ,p)     *t->operator()(i,j,q,a)
                                        +  S1->operator()(q+nact,p)     *t->operator()(j,i,q,a);
                Ta->operator()(j,i,p,a) += S1->operator()(q     ,p+nact)*t->operator()(i,j,q,a)
                                        +  S1->operator()(q+nact,p+nact)*t->operator()(j,i,q,a);
              }
            }
  } 
  
  if (0 == strcmp(Method.perturberClass, "AAAV")){
    FNdArrayView *S1 = TN("S1");
    for (int a=0; a<nvir; a++)
      for (int r2=0; r2<nact; r2++)
        for (int q2=0; q2<nact; q2++)
          for (int p2=0; p2<nact; p2++)
      for (int r1=0; r1<nact; r1++){
        for (int q1=0; q1<nact; q1++)
          for (int p1=0; p1<nact; p1++)
             Ta->operator()(a,p2,q2,r2)+= S1->operator()(p1,q1,r1, p2,q2,r2)
                                          *t->operator()(a,p1,q1,r1);
          }
  } 
  
  if (0 == strcmp(Method.perturberClass, "AAAC")){
    FNdArrayView *S1 = TN("S1");
    for (int i=0; i<ncor; i++)
      for (int r2=0; r2<nact; r2++){
        for (int q2=0; q2<nact; q2++)
          for (int p2=0; p2<nact; p2++)
      for (int r1=0; r1<nact; r1++)
        for (int q1=0; q1<nact; q1++)
          for (int p1=0; p1<nact; p1++)
             Ta->operator()(i,p2,q2,r2)+= S1->operator()(p1,q1,r1, p2,q2,r2)
                                          *t->operator()(i,p1,q1,r1);
          }
  } 
  
  if (0 == strcmp(Method.perturberClass, "CAAV")){
    FNdArrayView *S1 = TN("S1");
    int fullrange = ncor*nact*nact*nvir;
    for (int a=0; a<nvir; a++)
      for (int q=0; q<nact; q++)
        for (int p=0; p<nact; p++)
          for (int i=0; i<ncor; i++)
            for (int s=0; s<nact; s++)
              for (int r=0; r<nact; r++){
              //original//
                Ta->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact]          += S1->operator()(r+s*nact,p+q*nact)                     *t->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact]
                                                                          +  S1->operator()(r+s*nact+nact*nact,p+q*nact)           *t->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact+fullrange];
                Ta->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact+fullrange]+= S1->operator()(r+s*nact,p+q*nact+nact*nact)           *t->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact]
                                                                          +  S1->operator()(r+s*nact+nact*nact,p+q*nact+nact*nact) *t->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact+fullrange];
              //[debug/testing]
              //S1 is not ReShaped//
              //Ta->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact]          += S1->operator()(r     ,s,p     ,q)                     *t->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact]
              //                                                          +  S1->operator()(r+nact,s,p     ,q)                     *t->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact+fullrange];
              //Ta->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact+fullrange]+= S1->operator()(r     ,s,p+nact,q)                     *t->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact]
              //                                                          +  S1->operator()(r+nact,s,p+nact,q)                     *t->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact+fullrange];
              //t is called with operator//
              //Ta->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact]          += S1->operator()(r     ,s,p     ,q)                     *t->operator()(i,r     ,s,a)
              //                                                          +  S1->operator()(r+nact,s,p     ,q)                     *t->operator()(i,r+nact,s,a);
              //Ta->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact+fullrange]+= S1->operator()(r     ,s,p+nact,q)                     *t->operator()(i,r     ,s,a)
              //                                                          +  S1->operator()(r+nact,s,p+nact,q)                     *t->operator()(i,r+nact,s,a);
              }

    //[debug/testing]
    //for (int i=0; i<2*fullrange; i++) printf("[FLAG0] %i % 13.8e\n",i,Ta->pData[i]);
    //for (int p=0; p<nact; p++)
    //  for (int q=0; q<nact; q++)
    //     for (int a=0; a<nvir; a++)
    //       for (int i=0; i<ncor; i++){
    //         //printf("[FLAGA] %i %i %i %i | % 5.1e % 13.8e % 13.8e | % 5.1e % 13.8e % 13.8e \n",
    //         printf("[FLAGA] %i %i %i %i % 13.8e % 13.8e\n",
    //             i,p,q,a,
    //           //t->operator()(i          ,r,s,a)-
    //           //t->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact],
    //           //t->operator()(i+fullrange,r,s,a)-
    //           //t->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact+fullrange],
    //             //Ta->operator()(i,p,q,a)-Ta->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact],
    //             //Ta->operator()(i,p,q,a),
    //             Ta->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact],
    //             //Ta->operator()(i+fullrange,p,q,a)-Ta->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact+fullrange],
    //             //Ta->operator()(i+fullrange,p,q,a),
    //             Ta->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact+fullrange]
    //             );
    //       };

  } 
  
  if (0 == strcmp(Method.perturberClass, "CCVV")){
    for (int i=0; i<ncor; i++)
      for (int j=i; j<ncor; j++)
        for (int c=0; c<nvir; c++)
          for (int b=c; b<nvir; b++){
            if (b == c){
              Ta->operator()(c,b,j,i) += ( t->operator()(c,b,j,i)
                                          +t->operator()(c,b,i,j) )/2.0/pow(2.0,0.5);
            } else {
              Ta->operator()(c,b,j,i) += (-t->operator()(c,b,j,i)
                                          +t->operator()(c,b,i,j)
                                          +t->operator()(b,c,j,i)
                                          -t->operator()(b,c,i,j) )/4.0/pow(3.0,0.5);
              Ta->operator()(c,b,i,j) += ( t->operator()(c,b,j,i)
                                          +t->operator()(c,b,i,j)
                                          +t->operator()(b,c,j,i)
                                          +t->operator()(b,c,i,j) )/4.0;
            }
          }
  }

  std::cout<<std::flush;
  time_ortho=time_ortho+srci::GetTime()-time;
} // end FJobContext::MakeOrthogonal 


//=============================================================================
void FJobContext::BackToNonOrthogonal(std::string StringA, std::string StringB){
//-----------------------------------------------------------------------------
  /*!
   Make "StringB" back into non-orthogonal, into "StringA".
   This is "StringA"= (U.D^1/2)."StringB"
   where (U.D^1/2) is in S1 (and possibly S2)
  */
//-----------------------------------------------------------------------------
  double time=srci::GetTime();
  int nact = m_Domains['a'].nSize;
  int nvir = m_Domains['e'].nSize;
  int ncor = m_Domains['c'].nSize;

  FNdArrayView *t  = NULL;
  FNdArrayView *Ta = NULL;
  if (StringA.find(":")!=std::string::npos) t  = TND(StringA);
  else t  = TN(StringA);
  if (StringB.find(":")!=std::string::npos) Ta = TND(StringB);
  else Ta = TN(StringB);
  t->ClearData();

  if (0 == strcmp(Method.perturberClass, "AAVV")){
    FNdArrayView *S1 = TN("S1");
    FNdArrayView *S2 = TN("S2");
#pragma omp parallel for schedule(dynamic)
    for (int pq=0; pq<nact*nact; pq++){
      int p=pq/nact, q=pq%nact;
      for (int c=0; c<nvir; c++)
        for (int b=c; b<nvir; b++)
          for (int s=0; s<nact; s++)
            for (int r=0; r<nact; r++){
              if (b == c)
                t->operator()(b,c,p,q) += S2->operator()(p,q,r,s)*Ta->operator()(b,c,r,s);
              else {
                t->operator()(b,c,p,q) +=-S1->operator()(p,q,r,s)*Ta->operator()(b,c,r,s)
                                       +  S2->operator()(p,q,r,s)*Ta->operator()(c,b,r,s);
                t->operator()(c,b,p,q) += S1->operator()(p,q,r,s)*Ta->operator()(b,c,r,s)
                                       +  S2->operator()(p,q,r,s)*Ta->operator()(c,b,r,s);
              }
            }
    }

//    //in the equations we assume that t[a,b,p,q,] = t[b,a,q,p]
//    //this equality might be lost in single precision
//    //so for numerical reasons it is important to explicitly symmetrize
//#pragma omp parallel for schedule(dynamic)
//    for (int p=0; p<nact; p++)
//      for (int q=p; q<nact; q++)
//        for (int c=0; c<nvir; c++)
//          for (int b=c+1; b<nvir; b++){
//            float x = t->operator()(b,c,p,q), y = t->operator()(c,b,q,p);
//            t->operator()(b,c,p,q) = 0.5*(x+y);
//            t->operator()(c,b,q,p) = 0.5*(x+y);
//
//            x = t->operator()(b,c,q,p); y = t->operator()(c,b,p,q);
//            t->operator()(b,c,q,p) = 0.5*(x+y);
//            t->operator()(c,b,p,q) = 0.5*(x+y);
//          }
  } 
  
  if (0 == strcmp(Method.perturberClass, "CCAA")){
    FNdArrayView *S1 = TND("S1:aaaa");
    FNdArrayView *S2 = TND("S2:aaaa");
    for (int pq=0; pq<nact*nact; pq++){
      int p=pq/nact, q=pq%nact;
        for (int j=0; j<ncor; j++)
          for (int i=j; i<ncor; i++)
            for (int s=0; s<nact; s++)
              for (int r=0; r<nact; r++){
                //[correct_bug]
		        if (i == j)
		          t->operator()(i,j,p,q) += S2->operator()(p,q,r,s)*Ta->operator()(i,j,r,s);
		        else {
		          t->operator()(i,j,p,q) += S1->operator()(p,q,r,s)*Ta->operator()(i,j,r,s);
		          t->operator()(j,i,p,q)  = 0.0;
		        }
                //[debug/testing]
                //if (i == j)
                //  t->operator()(i,j,p,q) += S2->operator()(p,q,r,s)*Ta->operator()(i,j,r,s);
                // else {
                //  t->operator()(i,j,p,q) +=-S1->operator()(p,q,r,s)*Ta->operator()(i,j,r,s)
                //                         +  S2->operator()(p,q,r,s)*Ta->operator()(j,i,r,s);
                //  t->operator()(j,i,p,q) += S1->operator()(p,q,r,s)*Ta->operator()(i,j,r,s)
                //                         +  S2->operator()(p,q,r,s)*Ta->operator()(j,i,r,s);
                //}
              }
    }

//    //in the equations we assume that t[a,b,p,q,] = t[b,a,q,p]
//    //this equality might be lost in single precision
//    //so for numerical reasons it is important to explicitly symmetrize
//#pragma omp parallel for schedule(dynamic)
//    for (int p=0; p<nact; p++)
//      for (int q=p; q<nact; q++)
//        for (int i=0; i<nvir; i++)
//          for (int j=i+1; j<nvir; j++){
//            float x = t->operator()(j,i,p,q), y = t->operator()(i,j,q,p);
//            t->operator()(j,i,p,q) = 0.5*(x+y);
//            t->operator()(i,j,q,p) = 0.5*(x+y);
//
//            x = t->operator()(j,i,q,p); y = t->operator()(i,j,p,q);
//            t->operator()(j,i,q,p) = 0.5*(x+y);
//            t->operator()(i,j,p,q) = 0.5*(x+y);
//          }
  } 
  
  if (0 == strcmp(Method.perturberClass, "ACVV")){
     FNdArrayView *S1 = TN("S1");
     FNdArrayView *S2 = TN("S2");
     for (int p=0; p<nact; p++)
       for (int i=0; i<ncor; i++)
         for (int c=0; c<nvir; c++)
           for (int b=c; b<nvir; b++)
             for (int q=0; q<nact; q++){
               if (b == c)
                 t->operator()(b,c,i,p) += S2->operator()(p     ,q)     *Ta->operator()(b,c,i,q);
               else {
                 // [debug/testing]
                 // ongoing tries to avoid the block structure]
                 //t->operator()(b,c,i,q) += -sqrt(3)*S2->operator()(p     ,q)     *Ta->operator()(b,c,i,p)
                 //                       +           S2->operator()(p     ,q)     *Ta->operator()(c,b,i,p);
                 //t->operator()(c,b,i,q) +=  sqrt(3)*S2->operator()(p     ,q     )*Ta->operator()(b,c,i,p)
                 //                       +           S2->operator()(p     ,q     )*Ta->operator()(c,b,i,p);
                   t->operator()(b,c,i,p) += S1->operator()(p     ,q)     *Ta->operator()(b,c,i,q)
                                          +  S1->operator()(p     ,q+nact)*Ta->operator()(c,b,i,q);
                   t->operator()(c,b,i,p) += S1->operator()(p+nact,q)     *Ta->operator()(b,c,i,q)
                                          +  S1->operator()(p+nact,q+nact)*Ta->operator()(c,b,i,q);
               }
             }
  } 
  
  if (0 == strcmp(Method.perturberClass, "CCAV")){
    FNdArrayView *S1 = TN("S1");
    FNdArrayView *S2 = TN("S2");
    for (int a=0; a<nvir; a++)
      for (int q=0; q<nact; q++)
        for (int i=0; i<ncor; i++)
          for (int j=0; j<i+1; j++)
            for (int p=0; p<nact; p++){
              if (i == j)
                t->operator()(i,j,q,a) += S2->operator()(q     ,p)     *Ta->operator()(i,j,p,a);
              else {
                t->operator()(i,j,q,a) += S1->operator()(q     ,p)     *Ta->operator()(i,j,p,a)
                                       +  S1->operator()(q     ,p+nact)*Ta->operator()(j,i,p,a);
                t->operator()(j,i,q,a) += S1->operator()(q+nact,p)     *Ta->operator()(i,j,p,a)
                                       +  S1->operator()(q+nact,p+nact)*Ta->operator()(j,i,p,a);
              }
            }
  } 
  
  if (0 == strcmp(Method.perturberClass, "CCVV")){
    for (int i=0; i<ncor; i++)
      for (int j=i; j<ncor; j++)
        for (int c=0; c<nvir; c++)
          for (int b=c; b<nvir; b++){
            if (b == c){
              t->operator()(c,b,j,i) +=   Ta->operator()(c,b,j,i)/2.0/pow(2.0,0.5);
              t->operator()(c,b,i,j) +=   Ta->operator()(c,b,j,i)/2.0/pow(2.0,0.5);
            } else {
              t->operator()(c,b,j,i) += (-Ta->operator()(c,b,j,i)/pow(3.0,0.5)
                                         +Ta->operator()(c,b,i,j)              )/4.0;
              t->operator()(c,b,i,j) += ( Ta->operator()(c,b,j,i)/pow(3.0,0.5)
                                         +Ta->operator()(c,b,i,j)              )/4.0;
              t->operator()(b,c,j,i)  =    t->operator()(c,b,i,j);
              t->operator()(b,c,i,j)  =    t->operator()(c,b,j,i);
            }
          }
  } 
  
  if (0 == strcmp(Method.perturberClass, "CAAV")){
    FNdArrayView *S1 = TN("S1");
    int fullrange = ncor*nact*nact*nvir;
    for (int a=0; a<nvir; a++)
      for (int q=0; q<nact; q++)
        for (int p=0; p<nact; p++)
          for (int i=0; i<ncor; i++)
            for (int r=0; r<nact; r++)
              for (int s=0; s<nact; s++){
              //original//
                t->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact]          += S1->operator()(p+q*nact,r+s*nact)                    *Ta->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact]
                                                                         +  S1->operator()(p+q*nact,r+s*nact+nact*nact)          *Ta->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact+fullrange];
                t->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact+fullrange]+= S1->operator()(p+q*nact+nact*nact,r+s*nact)          *Ta->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact]
                                                                         +  S1->operator()(p+q*nact+nact*nact,r+s*nact+nact*nact)*Ta->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact+fullrange];
              //[debug/testing]
              //S1 is not ReShaped//
              //t->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact]          += S1->operator()(p     ,q,r     ,s)                    *Ta->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact]
              //                                                         +  S1->operator()(p     ,q,r+nact,s)                    *Ta->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact+fullrange];
              //t->pData[i+p*ncor+q*ncor*nact+a*ncor*nact*nact+fullrange]+= S1->operator()(p+nact,q,r     ,s)                    *Ta->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact]
              //                                                         +  S1->operator()(p+nact,q,r+nact,s)                    *Ta->pData[i+r*ncor+s*ncor*nact+a*ncor*nact*nact+fullrange];
              }
  } 
  
  if (0 == strcmp(Method.perturberClass, "AAAV")){
    FNdArrayView *S1 = TN("S1");
    for (int a=0; a<nvir; a++)
    for (int r1=0; r1<nact; r1++)
      for (int q1=0; q1<nact; q1++)
        for (int p1=0; p1<nact; p1++)
    for (int r2=0; r2<nact; r2++)
      for (int q2=0; q2<nact; q2++)
        for (int p2=0; p2<nact; p2++){
           t->operator()(a,p1,q1,r1) += S1->operator()(p1,q1,r1, p2,q2,r2)
                                       *Ta->operator()(a,p2,q2,r2);
    };
  } 
  
  if (0 == strcmp(Method.perturberClass, "AAAC")){
    FNdArrayView *S1 = TN("S1");
    for (int i=0; i<ncor; i++)
    for (int r1=0; r1<nact; r1++)
      for (int q1=0; q1<nact; q1++)
        for (int p1=0; p1<nact; p1++)
    for (int r2=0; r2<nact; r2++)
      for (int q2=0; q2<nact; q2++)
        for (int p2=0; p2<nact; p2++){
           t->operator()(i,p1,q1,r1) += S1->operator()(p1,q1,r1, p2,q2,r2)
                                       *Ta->operator()(i,p2,q2,r2);
    };

  }

  std::cout<<std::flush;
  time_ortho=time_ortho+srci::GetTime()-time;
} // end FJobContext::BackToNonOrthogonal


//=============================================================================
void FJobContext::TestingDF(FMemoryStack2 *Mem){
//-----------------------------------------------------------------------------
    /*!
    [debug/testing] Density Fitting
    */
//-----------------------------------------------------------------------------
  FJobContext Job;
  int nact = m_Domains['a'].nSize;
  int nvir = m_Domains['e'].nSize;
  int ncor = m_Domains['c'].nSize;

  if (nAux!=0){ // DensityFitting


    if(0 == strcmp(Method.perturberClass, "AAAV")){
      FNdArrayView *Wa= TND("W:Lea");
      FNdArrayView *Wb= TND("W:Laa");
      FNdArrayView W1; Job.CreateTensorFromShape(W1, "eaaa", "", m_Domains);
      double* W1data = Mem[omprank].AllocN(nvir*nact*nact*nact, 1.0);
      W1.pData = W1data;
      for (int a=0;a<nvir;a++)
      for (int q=0;q<nact;q++)
      for (int r=0;r<nact;r++)
      for (int s=0;s<nact;s++){
        for (int L=0;L<nAux;L++){
          W1.operator()(a,q,r,s)+=Wa->operator()(L,a,r)*Wb->operator()(L,q,s);
        };
        printf("[BM] W:eaaa % 5i % 5i % 5i % 5i % 11.5f\n",a,q,r,s,W1.operator()(a,q,r,s));
      };
      FNdArrayView W2; Job.CreateTensorFromShape(W2, "aaaa", "", m_Domains);
      double* W2data = Mem[omprank].AllocN(nact*nact*nact*nact, 1.0);
      W2.pData = W2data;
      for (int p=0;p<nact;p++)
      for (int q=0;q<nact;q++)
      for (int r=0;r<nact;r++)
      for (int s=0;s<nact;s++){
        for (int L=0;L<nAux;L++){
          W2.operator()(p,q,r,s)+=Wb->operator()(L,p,r)*Wb->operator()(L,q,s);
        };
        printf("[BM] W:aaaa % 5i % 5i % 5i % 5i % 11.5f\n",p,q,r,s,W2.operator()(p,q,r,s));
      };
    }

    if(0 == strcmp(Method.perturberClass, "CCAA")){
      FNdArrayView *Wa= TND("W:Lca");
      FNdArrayView *Wb= TND("W:Laa");
      FNdArrayView W1; Job.CreateTensorFromShape(W1, "ccaa", "", m_Domains);
      double* W1data = Mem[omprank].AllocN(ncor*ncor*nact*nact, 1.0);
      W1.pData = W1data;
      for (int i=0;i<ncor;i++)
      for (int j=0;j<ncor;j++)
      for (int p=0;p<nact;p++)
      for (int q=0;q<nact;q++){
        W1.operator()(i,j,p,q)=0.0;
        for (int L=0;L<nAux;L++){
          W1.operator()(i,j,p,q)+=Wa->operator()(L,i,p)*Wa->operator()(L,j,q);
        };
        printf("[BM] W:ccaa % 5i % 5i % 5i % 5i % 11.5f\n",i,j,p,q,W1.operator()(i,j,p,q));
      };
      FNdArrayView W2; Job.CreateTensorFromShape(W2, "aaaa", "", m_Domains);
      double* W2data = Mem[omprank].AllocN(nact*nact*nact*nact, 1.0);
      W2.pData = W2data;
      for (int p=0;p<nact;p++)
      for (int q=0;q<nact;q++)
      for (int r=0;r<nact;r++)
      for (int s=0;s<nact;s++){
        W2.operator()(p,q,r,s)=0.0;
        for (int L=0;L<nAux;L++){
          W2.operator()(p,q,r,s)+=Wb->operator()(L,p,r)*Wb->operator()(L,q,s);
        };
        printf("[BM] W:aaaa % 5i % 5i % 5i % 5i % 11.5f\n",p,q,r,s,W2.operator()(p,q,r,s));
      };
    }


  } else {


    if(0 == strcmp(Method.perturberClass, "AAAV")){
      FNdArrayView *W1= TND("W:eaaa");
      for (int a=0;a<nvir;a++)
      for (int q=0;q<nact;q++)
      for (int r=0;r<nact;r++)
      for (int s=0;s<nact;s++){
        printf("[BM] W:eaaa % 5i % 5i % 5i % 5i % 11.5f\n",a,q,r,s,W1->operator()(a,q,r,s));
      };
      FNdArrayView *W2= TND("W:aaaa");
      for (int p=0;p<nact;p++)
      for (int q=0;q<nact;q++)
      for (int r=0;r<nact;r++)
      for (int s=0;s<nact;s++){
        printf("[BM] W:aaaa % 5i % 5i % 5i % 5i % 11.5f\n",p,q,r,s,W2->operator()(p,q,r,s));
      };
    }

    if(0 == strcmp(Method.perturberClass, "CCAA")){
      FNdArrayView *W1= TND("W:ccaa");
      for (int i=0;i<ncor;i++)
      for (int j=0;j<ncor;j++)
      for (int p=0;p<nact;p++)
      for (int q=0;q<nact;q++){
        printf("[BM] W:ccaa % 5i % 5i % 5i % 5i % 11.5f\n",i,j,p,q,W1->operator()(i,j,p,q));
      };
      FNdArrayView *W2= TND("W:aaaa");
      for (int p=0;p<nact;p++)
      for (int q=0;q<nact;q++)
      for (int r=0;r<nact;r++)
      for (int s=0;s<nact;s++){
        printf("[BM] W:aaaa % 5i % 5i % 5i % 5i % 11.5f\n",p,q,r,s,W2->operator()(p,q,r,s));
      };
    }

  } // naux or not
} // end FJobContext::TestingDF


//=============================================================================
void FJobContext::AddHeffIntob(FMemoryStack2 &Mem){
//-----------------------------------------------------------------------------
    /*!
    [debug/testing] Add Heff directly into b
    */
//-----------------------------------------------------------------------------
  if (nAux!=0){ // DensityFitting

  int nact = m_Domains['a'].nSize;
  int nvir = m_Domains['e'].nSize;
  int ncor = m_Domains['c'].nSize;

  if(0 == strcmp(Method.perturberClass, "AAAV")){
    cout<<"\n[AddHeffIntob_AAAV]\n"<<endl;
    FNdArrayView *b = TND("b:eaaa");
    FNdArrayView *f = TND("f:ea");
    FNdArrayView *W = NULL;
    FNdArrayView *Wl= NULL;
    FNdArrayView *Wr= NULL;
    // left over to do some testing comparisons
    if (nAux==0){
    W = TND("W:eaaa");
    } else {
    Wl= TND("W:Lea");
    Wr= TND("W:Laa");
    }
    FNdArrayView *E1= TN("E1");
    FNdArrayView *E2= TN("E2");
    FNdArrayView *E3= TN("E3");
    for (int a=0; a<nvir; a++)
      for (int p=0; p<nact; p++)
        for (int q=0; q<nact; q++)
          for (int r=0; r<nact; r++)
      for (int v=0; v<nact; v++){

        double hav=f->operator()(a,v)/(WfDecl.nActElec);
        // left over to do some testing comparisons
        if (nAux==0){
        for (int s=0; s<nact; s++)
          hav+=-W->operator()(a,s,s,v)/(WfDecl.nActElec);
        } else {
        for (int L=0; L<nAux; L++)
        for (int s=0; s<nact; s++)
          hav+=-Wl->operator()(L,a,s)*Wr->operator()(L,s,v)/(WfDecl.nActElec);
        }

        for (int u=0; u<nact; u++){
          int w=u;
                               b->operator()(a,p,q,r)+= 1.0*E3->operator()(p,v,w,r,q,u)*hav;
          if  (v==u)           b->operator()(a,p,q,r)+= 1.0*E2->operator()(p,w,r,q)    *hav;
          if  (p==u)           b->operator()(a,p,q,r)+= 1.0*E2->operator()(v,w,q,r)    *hav;
          if  (q==p)           b->operator()(a,p,q,r)+= 1.0*E2->operator()(v,w,r,u)    *hav;
          if ((q==p)and(v==u)) b->operator()(a,p,q,r)+= 1.0*E1->operator()(w,r)        *hav;
          };
        };
  }

  if(0 == strcmp(Method.perturberClass, "AAAC")){
    cout<<"\n[AddHeffIntob_AAAC]\n"<<endl;
    FNdArrayView *b = TND("b:caaa");
    FNdArrayView *f = TND("f:ca");
    FNdArrayView *E1= TN("E1");
    FNdArrayView *E2= TN("E2");
    FNdArrayView *E3= TN("E3");
    for (int i=0; i<ncor; i++)
      for (int r=0; r<nact; r++)
        for (int q=0; q<nact; q++)
          for (int p=0; p<nact; p++)
    for (int v=0; v<nact; v++)
      for (int u=0; u<nact; u++){
        int w=u;
                             b->operator()(i,p,q,r)+=-1.0*E3->operator()(p,q,w,r,v,u)*f->operator()(i,v)/(WfDecl.nActElec);
        if  (q==u)           b->operator()(i,p,q,r)+=-1.0*E2->operator()(p,w,r,v)    *f->operator()(i,v)/(WfDecl.nActElec);
        if  (p==u)           b->operator()(i,p,q,r)+=-1.0*E2->operator()(q,w,v,r)    *f->operator()(i,v)/(WfDecl.nActElec);
        if  (v==p)           b->operator()(i,p,q,r)+=-1.0*E2->operator()(q,w,r,u)    *f->operator()(i,v)/(WfDecl.nActElec);
        if ((v==p)and(q==u)) b->operator()(i,p,q,r)+=-1.0*E1->operator()(w,r)        *f->operator()(i,v)/(WfDecl.nActElec);
        if  (q==v)           b->operator()(i,p,q,r)+= 2.0*E2->operator()(p,w,r,u)    *f->operator()(i,v)/(WfDecl.nActElec);
        if ((u==p)and(q==v)) b->operator()(i,p,q,r)+= 2.0*E1->operator()(w,r)        *f->operator()(i,v)/(WfDecl.nActElec);
    };
  }

  if(0 == strcmp(Method.perturberClass, "CAAV")){
    cout<<"\n[AddHeffIntob_CAAV]\n"<<endl;
    FNdArrayView *b1= TND("b1:caae");
    FNdArrayView *b2= TND("b2:caae");
    FNdArrayView *f = TND("f:ec");
    FNdArrayView *E1= TN("E1");
    FNdArrayView *E2= TN("E2");
    for (int i=0; i<ncor; i++)
      for (int p=0; p<nact; p++)
        for (int q=0; q<nact; q++)
          for (int a=0; a<nvir; a++)
    for (int r=0; r<nact; r++){
      int s=r;
      b1->operator()(i,p,q,a) +=  2.0*E2->operator()(r,q,s,p)*f->operator()(a,i)/(WfDecl.nActElec);
      b2->operator()(i,p,q,a) += -1.0*E2->operator()(r,q,s,p)*f->operator()(a,i)/(WfDecl.nActElec);
      if (r==p){
      b1->operator()(i,p,q,a) +=  2.0*E1->operator()(s,q)    *f->operator()(a,i)/(WfDecl.nActElec);
      b2->operator()(i,p,q,a) += -1.0*E1->operator()(s,q)    *f->operator()(a,i)/(WfDecl.nActElec);
      };
    };
  }

  std::cout<<std::flush;
  } // end of "if naux"
} // end FJobContext::AddHeffIntob


//=============================================================================
void FJobContext::AddHeff(FMemoryStack2 &Mem){
//-----------------------------------------------------------------------------
    /*!
    [debug/testing] Add Heff into W
    */
//-----------------------------------------------------------------------------
  if (nAux==0){ // not DensityFitting

  int nact = m_Domains['a'].nSize;
  int nvir = m_Domains['e'].nSize;
  int ncor = m_Domains['c'].nSize;

  if(0 == strcmp(Method.perturberClass, "AAAV")){
    cout<<"\n[AddHeff_AAAV]\n"<<endl;
    // Need to add "h^\prime_eff" to "W:eaaa"
    // (see Eqs in part IV.G of AngCimMal-JCP-2002)
    //   W(a,pqr)=W(a,pqr)+ \delta_pr/N (f(a,q) - \sum_s <as|sq>)
    // (this needs an intermediary array for <as|sq>: it will be "t:eaaa")
    FNdArrayView *W  = TND("W:eaaa");
    FNdArrayView *f  = TND("f:ea");
    FNdArrayView *W2 = TND("t:eaaa");
    Copy(*TND("t:eaaa"), *TND("W:eaaa"));
    for (int q=0; q<nact; q++)
      for (int a=0; a<nvir; a++){
        double haq=f->operator()(a,q)/(WfDecl.nActElec);
        for (int s=0; s<nact;  s++){
          haq+=-W2->operator()(a,s,s,q)/(WfDecl.nActElec);
        };
        for (int p=0; p<nact;  p++) W->operator()(a,p,q,p) += haq;
      };
    W2->ClearData();
  }

  if(0 == strcmp(Method.perturberClass, "AAAC")){
    cout<<"\n[AddHeff_AAAC]\n"<<endl;
    // Need to add "f" to "W:caaa"
    // (see Eqs in part IV.H of AngCimMal-JCP-2002)
    //   W(i,pqr)=W(i,pqr)+ \delta_pr/N f(i,q)
    FNdArrayView *W  = TND("W:caaa");
    FNdArrayView *f  = TND("f:ca");
    for (int q=0; q<nact; q++)
      for (int i=0; i<ncor; i++){
        double fiq=f->operator()(i,q)/(WfDecl.nActElec);
        for (int p=0; p<nact;  p++) W->operator()(i,p,q,p) += fiq;
      };
  }

  if(0 == strcmp(Method.perturberClass, "CAAV")){
    cout<<"\n[AddHeff_CAAV]\n"<<endl;
    // Need to add "f" to "W:eaca"
    // (see Eqs in part IV.F of AngCimMal-JCP-2002)
    //   W(a,p,i,q)=W(a,p,i,q)+ \delta_pq/N f(a,i)
    FNdArrayView *W = TND("W:eaca");
    FNdArrayView *f = TND("f:ec");
    for (int i=0; i<ncor; i++)
      for (int a=0; a<nvir; a++){
        double fai=f->operator()(a,i)/(WfDecl.nActElec);
        for (int p=0; p<nact;  p++) W->operator()(a,p,i,p) += fai;
      };
  }

  std::cout<<std::flush;
  } // end of "if naux"
} // end FJobContext::AddHeff


//=============================================================================
void FJobContext::Calc3RD(FMemoryStack2 &Mem){
//-----------------------------------------------------------------------------
    /*!
      Calculation of E^(3).
      We have:
         < Psi_1 | V | Psi_1 > = \sum d_\mu d_\nu w_\tau < O_\mu O_\tau O_\nu >
                               = \sum w_\tau A_\tau
     
      The tensors A_\tau for each class are build in EqsRes.
      All there is left to do is the trace with w_\tau.
    */
//-----------------------------------------------------------------------------
  double energy=0, contrib=0, prov=0;
  std::cout << "\n Calculate contribution to 3RD order\n";

  // Accumulation of A_\tau, for all classes
  ExecEquationSet(Method.EqsRes, m_Tensors, Mem);
  /* track domains: PRINT */
  if (track_contract){
    printf("-----total_contract %i\n",track_contract_nbr_total);
    std::cout<<std::endl;
    track_contract=false;
    //return;
  }

  std::cout<<""<<std::endl;
  for (uint i=0; i<Method.nTensorDecls; ++i){
    FTensorDecl const &Decl = Method.pTensorDecls[i];
    if (Decl.pName[0]=='A'){
      std::string NameAndDomain1 = boost::str(format("%s:%s") % Decl.pName % Decl.pDomain);
      std::string NameAndDomain2;
      if (strlen(Decl.pDomain)!=2){
        NameAndDomain2 = boost::str(format("%s:%s") % "W" % Decl.pDomain);
        contrib =ct::Dot(TND(NameAndDomain1)->pData, TND(NameAndDomain2)->pData, TND(NameAndDomain1)->nValues());
        cout<<NameAndDomain1<<"."<<NameAndDomain2;printf("% 13.5e\n",contrib);std::cout<<std::flush;
        energy+=contrib;
      } else {
        if (MethodName.find("NEVPT3_H0")!=std::string::npos){
          NameAndDomain2 = boost::str(format("%s:%s") % "f" % Decl.pDomain);
        }else{
          NameAndDomain2 = boost::str(format("%s:%s") % "k" % Decl.pDomain);
        }
        contrib =ct::Dot(TND(NameAndDomain1)->pData, TND(NameAndDomain2)->pData, TND(NameAndDomain1)->nValues());
        cout<<NameAndDomain1<<"  ."<<NameAndDomain2<<"  ";printf("% 13.5e\n",contrib);std::cout<<std::flush;
        energy+=contrib;
      }
    }
  }
  std::cout<<""<<std::endl;

  // Final E^(3) energy
  if (MethodName.find("NEVPT")!=std::string::npos){
    printf("!NEVPT3 STATE 1 ENERGY     % 28.14f\n",energy);std::cout<<std::flush;
  } else {
    printf("!MRLCC3 STATE 1 ENERGY     % 28.14f\n",energy);std::cout<<std::flush;
  }
} // end FJobContext::Calc3RD


