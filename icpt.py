#!/usr/bin/env python
#
# Author: Sandeep Sharma <sanshar@gmail.com>
#         Qiming Sun <osirpt.sun@gmail.com>
#

import ctypes
import _ctypes
import time
import tempfile
from functools import reduce
import numpy
import h5py
import sys
from pyscf import tools
#import dmrgcicopy
from pyscf import lib
import pyscf.lib

float_precision = numpy.dtype('Float64')
mpiprefix=""
executable="/home/sharma/apps/forServer/pyscf/future/icmpspt/icpt"
libint2unpack = pyscf.lib.load_library('libint2unpack')

NUMERICAL_ZERO = 1e-14
def readIntegrals(infile, reorder, dt=numpy.dtype('Float64')):
    startScaling = False

    norb = -1 #need to read norb from infile
    nelec = -1

    lines = []
    f = open(infile, 'r')

    irreps = []
    index = 0
    for line in f:
        linesplit = line.replace("="," ")
        linesplit = linesplit.replace(","," ")
        linesp = linesplit.split()
        if (startScaling == False and len(linesp) == 1 and (linesp[0] == "&END" or linesp[0] == "/")):
            startScaling = True
            index += 1
            break
        elif(startScaling == False):
            if (linesp[0] == "&FCI"):
                if (linesp[1] == "NORB"):
                    norb = int(linesp[2])
                if (linesp[3] == "NELEC"):
                    nelec = int(linesp[4])
            elif (linesp[0] == "ISYM"):
                continue
            elif (linesp[0] == "ORBSYM"):
                for token in linesp[1:] :
                    irreps.append(int(token))
            else :
                for token in linesp :
                    irreps.append(int(token))
            index += 1

    reorderIrrep = len(irreps)*[1]
    for i in range(len(irreps)):
        reorderIrrep[reorder.index(i)] = irreps[i]
    if (norb == -1 or nelec == -1):
        print "could not read the norbs or nelec"
        exit(0)

    int2 = numpy.zeros(shape=(norb*(norb+1)/2, norb*(norb+1)/2), dtype=dt, order='F')
    int1 = numpy.zeros(shape=(norb, norb), dtype=dt, order='F')
    coreE = 0.0

    totalIntegralLines = len(lines) - index
    for line in f:
        linesp = line.split()
        if (len(linesp) != 5) :
            continue
        integral, a, b, c, d = float(linesp[0]), int(linesp[1]), int(linesp[2]), int(linesp[3]), int(linesp[4])

        if(a==b==c==d==0):
            coreE = integral
        elif (b==c==d==0 and a != 0):
            continue
        elif (c==d==0):
            A,B = reorder.index(a-1)+1, reorder.index(b-1)+1
            int1[A-1,B-1] = integral
            int1[B-1,A-1] = integral
        else:
            A,B,C,D = max(reorder.index(a-1), reorder.index(b-1)), min(reorder.index(a-1), reorder.index(b-1)), max(reorder.index(c-1), reorder.index(d-1)), min(reorder.index(c-1), reorder.index(d-1)) #,reorder.index(c-1)+1, reorder.index(d-1)+1

            int2[A*(A+1)/2+B, C*(C+1)/2+D] = integral
            int2[C*(C+1)/2+D, A*(A+1)/2+B] = integral


    return norb, nelec, int2, int1, coreE, reorderIrrep

def makeheff(int1, int2popo, int2ppoo, E1, ncore, nvirt):
        nc = int1.shape[0]-nvirt

        int1_eff = 1.*int1 + 2.0*numpy.einsum('mnii->mn', int2ppoo[:, :, :ncore, :ncore])-numpy.einsum('mini->mn', int2popo[:,:ncore,:, :ncore])

        int1_eff[:ncore, :ncore] += numpy.einsum('lmjk,jk->lm',int2ppoo[:ncore,:ncore,ncore:nc,ncore:nc], E1) - 0.5*numpy.einsum('ljmk,jk->lm',int2popo[:ncore,ncore:nc,:ncore,ncore:nc], E1)
        int1_eff[nc:, nc:] += numpy.einsum('lmjk,jk->lm',int2ppoo[nc:,nc:,ncore:nc,ncore:nc], E1) - 0.5*numpy.einsum('ljmk,jk->lm',int2popo[nc:,ncore:nc,nc:,ncore:nc], E1)
        #int1_eff[nc:, nc:] += numpy.einsum('ljmk,jk->lm',int2[nc:,ncore:nc,nc:,ncore:nc], E1) - 0.5*numpy.einsum('ljkm,jk->lm',int2[nc:,ncore:nc,ncore:nc,nc:], E1)
        return int1_eff


def writeE3Numpy(dm3):
    numpy.save("int/E3",dm3)
    numpy.save("int/E3B.npy", dm3.transpose(0,3,1,4,2,5))
    numpy.save("int/E3C.npy", dm3.transpose(5,0,2,4,1,3))

def writeInt2Numpy(ncore, ncas, nelec, int2ppoo, int2popo):
    nocc = ncore+ncas
    nact = ncas
    norbs = int2ppoo.shape[0]
    nvirt = norbs-ncore-nact
    nelectron = nelec
    nc = ncore+nact

    import os
    os.system("mkdir int")

    '''
    numpy.save("int/W:caca", numpy.asfortranarray(int2[:ncore, ncore:nc, :ncore, ncore:nc]))
    numpy.save("int/W:caac", numpy.asfortranarray(int2[:ncore,ncore:nc, ncore:nc, :ncore]))

    numpy.save("int/W:cece", numpy.asfortranarray(int2[:ncore, nc:, :ncore, nc:]))
    numpy.save("int/W:ceec", numpy.asfortranarray(int2[:ncore, nc:, nc:, :ncore]))

    numpy.save("int/W:aeae", numpy.asfortranarray(int2[ncore:nc, nc:, ncore:nc, nc:]))
    numpy.save("int/W:aeea", numpy.asfortranarray(int2[ncore:nc, nc:, nc:, ncore:nc]))

    numpy.save("int/W:cccc", numpy.asfortranarray(int2[:ncore,:ncore, :ncore, :ncore]))
    numpy.save("int/W:aaaa", numpy.asfortranarray(int2[ncore:nc,ncore:nc, ncore:nc, ncore:nc]))
    '''

    numpy.save("int/W:caca", numpy.asfortranarray(int2ppoo[:ncore, :ncore, ncore:nc, ncore:nc].transpose(0,2,1,3)))
    numpy.save("int/W:caac", numpy.asfortranarray(int2popo[:ncore,ncore:nc, ncore:nc, :ncore].transpose(0,2,1,3)))

    numpy.save("int/W:cece", numpy.asfortranarray(int2ppoo[nc:, nc:, :ncore, :ncore].transpose(2,0,3,1)))
    numpy.save("int/W:ceec", numpy.asfortranarray(int2popo[nc:, :ncore, nc:, :ncore].transpose(1,2,0,3)))

    numpy.save("int/W:aeae", numpy.asfortranarray(int2ppoo[nc:, nc:, ncore:nc,ncore:nc].transpose(2,0,3,1)))
    numpy.save("int/W:aeea", numpy.asfortranarray(int2popo[nc:, ncore:nc,nc:, ncore:nc].transpose(1,2,0,3)))

    numpy.save("int/W:cccc", numpy.asfortranarray(int2ppoo[:ncore,:ncore, :ncore, :ncore].transpose(0,2,1,3)))
    numpy.save("int/W:aaaa", numpy.asfortranarray(int2ppoo[ncore:nc,ncore:nc, ncore:nc, ncore:nc].transpose(0,2,1,3)))



    #store perturbers
    numpy.save("int/W:eecc", numpy.asfortranarray(int2popo[nc:,:ncore,nc:,:ncore].transpose(0,2,1,3)))
    numpy.save("int/W:eeca", numpy.asfortranarray(int2popo[nc:,:ncore, nc:, ncore:nc].transpose(0,2,1,3)))
    numpy.save("int/W:ccaa", numpy.asfortranarray(int2popo[:ncore,ncore:nc, :ncore, ncore:nc].transpose(0,2,1,3)))
    numpy.save("int/W:eeaa", numpy.asfortranarray(int2popo[nc:,ncore:nc, nc:, ncore:nc].transpose(0,2,1,3)))
    numpy.save("int/W:eaca", numpy.asfortranarray(int2popo[nc:,:ncore, ncore:nc, ncore:nc].transpose(0,2,1,3)))
    numpy.save("int/W:aeca", numpy.asfortranarray(int2popo[ncore:nc,:ncore, nc:,ncore:nc].transpose(0,2,1,3)))
    numpy.save("int/W:ccae", numpy.asfortranarray(int2popo[:ncore,ncore:nc, nc:, :ncore].transpose(0,3,1,2)))
    '''
    numpy.save("int/W:eecc", numpy.asfortranarray(int2[nc:,nc:,:ncore,:ncore]))
    numpy.save("int/W:eeca", numpy.asfortranarray(int2[nc:, nc:,:ncore, ncore:nc]))
    numpy.save("int/W:ccaa", numpy.asfortranarray(int2[:ncore, :ncore, ncore:nc, ncore:nc]))
    numpy.save("int/W:eeaa", numpy.asfortranarray(int2[nc:, nc:,ncore:nc, ncore:nc]))
    numpy.save("int/W:eaca", numpy.asfortranarray(int2[nc:,ncore:nc,:ncore, ncore:nc]))
    numpy.save("int/W:aeca", numpy.asfortranarray(int2[ncore:nc, nc:,:ncore,ncore:nc]))
    numpy.save("int/W:ccae", numpy.asfortranarray(int2[:ncore, :ncore,ncore:nc, nc:]))
    '''
def writeNumpyforMRLCC(ncore, ncas, nelec, int2ppoo, int2popo, int1, h1eff, irrep, coreE, E1, E2, E3) :
    nocc = ncore+ncas
    nact = ncas
    norbs = int1.shape[0]
    nvirt = norbs-ncore-nact
    nelectron = nelec
    nc = ncore+nact


    int1_eff = 1.0*h1eff
    int1_eff[:ncore,:ncore] += numpy.einsum('acbd,bd', int2ppoo[ :ncore, :ncore, ncore:nocc, ncore:nocc], E1)
    int1_eff[:ncore,:ncore] -= numpy.einsum('abcd,bd', int2popo[:ncore, ncore:nocc, :ncore, ncore:nocc], E1)*0.5
    int1_eff[nocc:,nocc:] += numpy.einsum('acbd,bd', int2ppoo[ nocc:, nocc:, ncore:nocc, ncore:nocc], E1)
    int1_eff[nocc:,nocc:] -= numpy.einsum('abcd,bd', int2popo[ nocc:, ncore:nocc, nocc:, ncore:nocc], E1)*0.5
    #int1_eff[nocc:,nocc:] += numpy.einsum('acbd,cd', int2[nocc:,ncore:nocc, nocc:, ncore:nocc], E1)
    #int1_eff[nocc:,nocc:] -= numpy.einsum('abcd,bc', int2[nocc:, ncore:nocc, ncore:nocc,nocc:], E1)*0.5


    numpy.save("int/E2",E2)
    numpy.save("int/E1",E1)
    #numpy.save("int/int2",int2)
    numpy.save("int/int1",numpy.asfortranarray(int1))
    numpy.save("int/int1eff",numpy.asfortranarray(int1_eff))



    nc = ncore+ncas
    energyE0 = 1.0*numpy.einsum('ij,ij', h1eff[ncore:nc, ncore:nc], E1)
    energyE0 += 0.5*numpy.einsum('ikjl,ijkl', int2ppoo[ncore:nc, ncore:nc, ncore:nc, ncore:nc], E2)
    energy_core = 2.0*numpy.einsum('iijj', int2ppoo[:ncore,:ncore,:ncore,:ncore])-numpy.einsum('ijij', int2popo[:ncore,:ncore,:ncore,:ncore])
    energyE0 += energy_core
    print "Energy = ", energyE0+coreE+numpy.einsum('ii', int1[:ncore, :ncore])*2.0
    orbsymout=irrep


    fout = open('FCIDUMP_aaav','w')
    tools.fcidump.write_head(fout, int1.shape[0]-ncore, nelectron, orbsym= orbsymout[ncore:])

    for i in range(ncore,int1.shape[0]):
        for j in range(ncore, i+1):
            for k in range(ncas):
                for l in range(k+1):
                    #if (i>=ncore+ncas and j <ncore+ncas and j-ncore != k and j-ncore!=l and k!=l):
                    #continue
                    if abs(int2ppoo[i,j, k+ncore,l+ncore]) > 1.e-8 :
                        fout.write(' %17.9e %4d %4d %4d %4d\n' \
                                   % (int2ppoo[i,j, k+ncore,l+ncore], i+1-ncore, j+1-ncore, k+1, l+1))
                    if (j >= nc and abs(int2popo[i, k+ncore, j, l+ncore]) > 1.e-8):
                        fout.write(' %17.9e %4d %4d %4d %4d\n' \
                                       % (int2popo[i,k+ncore,j, l+ncore], i+1-ncore, k+1, l+1, j+1-ncore))
                    if (j >= nc and abs(int2popo[i, l+ncore, j, k+ncore]) > 1.e-8):
                        fout.write(' %17.9e %4d %4d %4d %4d\n' \
                                       % (int2popo[i,l+ncore, j, k+ncore], i+1-ncore, l+1, k+1, j+1-ncore))


    tools.fcidump.write_hcore(fout, h1eff[ncore:,ncore:], int1.shape[0]-ncore, tol=1e-8)
    fout.write(' %17.9e  0  0  0  0\n' %( energy_core-energyE0))
    fout.close()

    '''
    for i in range(ncore,int1.shape[0]):
        for j in range(ncore, i+1):
            for k in range(ncas):
                for l in range(k+1):
                    if abs(int2[i,k+ncore,j,l+ncore]) > 1.e-8 :
                        fout.write(' %17.9e %4d %4d %4d %4d\n' \
                                   % (int2[i,k+ncore,j,l+ncore], i+1-ncore, j+1-ncore, k+1, l+1))
                    if (j >= nc and abs(int2[i, j, k+ncore, l+ncore]) > 1.e-8):
                        fout.write(' %17.9e %4d %4d %4d %4d\n' \
                                       % (int2[i,j, k+ncore,l+ncore], i+1-ncore, k+1, l+1, j+1-ncore))
                    if (j >= nc and abs(int2[i, j, l+ncore, k+ncore]) > 1.e-8):
                        fout.write(' %17.9e %4d %4d %4d %4d\n' \
                                       % (int2[i,j, l+ncore,k+ncore], i+1-ncore, l+1, k+1, j+1-ncore))


    tools.fcidump.write_hcore(fout, h1eff[ncore:,ncore:], int1.shape[0]-ncore, tol=1e-8)
    fout.write(' %17.9e  0  0  0  0\n' %( energy_core-energyE0))
    fout.close()
    '''

    nc = ncore+ncas
    fout = open('FCIDUMP_aaac','w')
    tools.fcidump.write_head(fout, nc, nelectron, orbsym= orbsymout[:nc])
    for i in range(nc):
        for j in range(i+1):
            for k in range(nc):
                for l in range(k+1):
                    if abs(int2ppoo[i,j,k,l]) > 1.e-8 :
                        fout.write(' %17.9e %4d %4d %4d %4d\n' \
                                   % (int2ppoo[i,j,k,l], i+1, j+1, k+1, l+1))

    tools.fcidump.write_hcore(fout, int1[:nocc,:nocc], nocc, tol=1e-8)
    fout.write(' %17.9e  0  0  0  0\n' %( -energyE0-numpy.einsum('ii', int1[:ncore, :ncore])*2.0))
    fout.close()

    return energyE0, norbs


def writeNevpt2Integrals(ncore, ncas, nelec, int2ppoo, int2popo, int1, h1eff, irrep, coreE, dm1, dm2, dm3):
    nocc = ncore + ncas
    nact = ncas
    norbs = int2ppoo.shape[0]
    nvirt = norbs-ncore-nact
    nelectron = nelec
    eris_sp={}
    eris_sp['h1eff']= 1.*h1eff #numpy.zeros(shape=(norbs, norbs))

    eris_sp['h1eff'][:ncore,:ncore] += numpy.einsum('abcd,cd', int2ppoo[:ncore, :ncore, ncore:nocc, ncore:nocc], dm1)
    eris_sp['h1eff'][:ncore,:ncore] -= numpy.einsum('abcd,bc', int2ppoo[:ncore, ncore:nocc, ncore:nocc,:ncore], dm1)*0.5
    eris_sp['h1eff'][nocc:,nocc:] += numpy.einsum('abcd,cd', int2ppoo[nocc:,nocc:,ncore:nocc, ncore:nocc], dm1)
    eris_sp['h1eff'][nocc:,nocc:] -= numpy.einsum('abcd,bd', int2popo[nocc:, ncore:nocc, nocc:,ncore:nocc], dm1)*0.5

    offdiagonal = 0.0
    #zero out off diagonal core
    for k in range(ncore):
        for l in range(ncore):
            if(k != l):
                offdiagonal = max(abs(offdiagonal), abs(eris_sp['h1eff'][k,l] ))
    #zero out off diagonal virtuals
    for k in range(ncore+ncas, norbs):
        for l in range(ncore+ncas,norbs):
            if(k != l):
                offdiagonal = max(abs(offdiagonal), abs(eris_sp['h1eff'][k,l] ))

    if (abs(offdiagonal) > 1e-6):
        print "WARNING ***** Have to use natorual orbitals from casscf ****"
        print "offdiagonal elements ", offdiagonal

    numpy.save("int/int1eff", numpy.asfortranarray(eris_sp['h1eff']))
    numpy.save("int/E1.npy", numpy.asfortranarray(dm1))
    numpy.save("int/E2.npy", numpy.asfortranarray(dm2))

    nc = ncore+ncas
    energyE0 = 1.0*numpy.einsum('ij,ij', eris_sp['h1eff'][ncore:nc, ncore:nc], dm1)
    energyE0 += 0.5*numpy.einsum('ikjl,ijkl', int2ppoo[ncore:nc, ncore:nc, ncore:nc, ncore:nc], dm2)
    energy_core = 2.0*numpy.einsum('iijj', int2ppoo[:ncore,:ncore,:ncore,:ncore])-numpy.einsum('ijij', int2popo[:ncore,:ncore,:ncore,:ncore])
    energyE0 += energy_core
    print "Energy = ", energyE0+coreE+numpy.einsum('ii', int1[:ncore, :ncore])*2.0
    orbsymout=irrep


    fout = open('FCIDUMP_aaav','w')
    tools.fcidump.write_head(fout, eris_sp['h1eff'].shape[0]-ncore, nelectron-2*ncore, orbsym= orbsymout[ncore:])

    for i in range(ncore,eris_sp['h1eff'].shape[0]):
        for j in range(ncore, ncore+ncas):
            for k in range(ncas):
                for l in range(k+1):
                    if (j >=nc and i!=j):
                        continue
                    if abs(int2ppoo[i,j,k+ncore,l+ncore]) > 1.e-8 :
                        fout.write(' %17.9e %4d %4d %4d %4d\n' \
                                   % (int2ppoo[i,j,k+ncore,l+ncore], i+1-ncore, j+1-ncore, k+1, l+1))

    tools.fcidump.write_hcore(fout, eris_sp['h1eff'][ncore:,ncore:], eris_sp['h1eff'].shape[0]-ncore, tol=1e-8)
    fout.write(' %17.9e  0  0  0  0\n' %( energy_core-energyE0))
    fout.close()

    nc = ncore+ncas
    fout = open('FCIDUMP_aaac','w')
    tools.fcidump.write_head(fout, nc, nelectron, orbsym= orbsymout[:nc])
    for i in range(nc):
        for j in range(ncore, nc):
            for k in range(ncore, nc):
                for l in range(ncore,k+1):
                    if abs(int2ppoo[i,j,k,l]) > 1.e-8 :
                        fout.write(' %17.9e %4d %4d %4d %4d\n' \
                                   % (int2ppoo[i,j,k,l], i+1, j+1, k+1, l+1))

    dmrge = energyE0-energy_core

    ecore_aaac = 0.0;
    for i in range(ncore):
        ecore_aaac += 2.0*eris_sp['h1eff'][i,i]
    tools.fcidump.write_hcore(fout, eris_sp['h1eff'][:nc,:nc], nc, tol=1e-8)
    fout.write(' %17.9e  0  0  0  0\n' %( -dmrge-ecore_aaac))
    fout.close()


    return norbs, energyE0

def executeMRLCC(nelecas, ncore, ncas, memory=10):
    methods = ['MRLCC_CCVV', 'MRLCC_CCAV', 'MRLCC_ACVV', 'MRLCC_CCAA', 'MRLCC_AAVV', 'MRLCC_CAAV', 'MRLCC_AAAV', 'MRLCC_AAAC']
    totalE = 0.0
    for method in methods:
        f = open("%s.inp"%(method), 'w')
        if (memory is not None):
            f.write('work-space-mb %d\n'%(memory*1000))
        f.write('method %s\n'%(method))
        f.write('orb-type spatial/MO\n')
        f.write('ms2 %d\n'%(abs(nelecas[0]-nelecas[1])))
        f.write('nact %d\n'%(nelecas[0]+nelecas[1]))
        f.write('nelec %d\n'%(nelecas[0]+nelecas[1]+ncore*2))
        f.write('nactorb %d\n'%(ncas))
        f.write('int1e/fock int/int1eff.npy\n')
        f.write('int1e/coreh int/int1.npy\n')
        #f.write('E3  int/E3.npy\n')
        #f.write('E2  int/E2.npy\n')
        #f.write('E1  int/E1.npy\n')
        f.write('thr-den 1.000000e-05\n')
        f.write('thr-var 1.000000e-05\n')
        f.write('thr-trunc 1.000000e-04\n')
        f.close();
        from subprocess import check_call
        try:
            infile="%s.inp"%(method)
            outfile="%s.out"%(method)
            output = check_call("%s  %s  %s > %s"%(mpiprefix, executable, infile, outfile), shell=True)
            f = open(outfile,'r')
            file_text = f.readlines()
            energy = float(file_text[-1].split()[-1])
            totalE += energy
            print "perturer %s --  %18.9e"%(method[-4:], energy)
            f.close()
        except ValueError:
            print(output)
            exit()

        sys.stdout.flush()
    return totalE

def executeNEVPT(nelec, ncore, ncas, memory=10):
    methods = ['NEVPT_CCVV', 'NEVPT_CCAV', 'NEVPT_ACVV', 'NEVPT_CCAA', 'NEVPT_AAVV', 'NEVPT_CAAV', 'NEVPT_AAAV', 'NEVPT_AAAC']
    totalE = 0.0
    for method in methods:
        f = open("%s.inp"%(method), 'w')
        if (memory is not None):
            f.write('work-space-mb %d\n'%(memory*1000))
        f.write('method %s\n'%(method))
        f.write('orb-type spatial/MO\n')
        f.write('nelec %d\n'%(nelecas[0]+nelecas[1]+ncore*2))
        f.write('ms2 %d\n'%(abs(nelecas[0]-nelecas[1])))
        f.write('nact %d\n'%(nelecas[0]+nelecas[1]))
        #f.write('nact %d\n'%(nelec))
        f.write('nactorb %d\n'%(ncas))
        f.write('int1e/fock int/int1eff.npy\n')
        #f.write('E3  int/E3.npy\n')
        #f.write('E2  int/E2.npy\n')
        #f.write('E1  int/E1.npy\n')
        f.write('thr-den 1.000000e-05\n')
        f.write('thr-var 1.000000e-05\n')
        f.write('thr-trunc 1.000000e-03\n')
        f.close();
        from subprocess import check_call
        try:
            infile="%s.inp"%(method)
            outfile="%s.out"%(method)
            output = check_call("%s  %s  %s > %s"%(mpiprefix, executable, infile, outfile), shell=True)
            f = open(outfile,'r')
            file_text = f.readlines()
            energy = float(file_text[-1].split()[-1])
            totalE += energy
            print "perturer %s --  %18.9e"%(method[-4:], energy)
            f.close()
        except ValueError:
            print(output)
            exit()

        sys.stdout.flush()
    return totalE

def kernel(mc, *args, **kwargs):
    return icmpspt(mc, *args, **kwargs)





def writeDMRGConfFile(neleca, nelecb, ncore, ncas, norbs, sym, DMRGCI, maxM, perturber, memory, numthrds, reorder, extraline, approx= False):
    configFile = "response_aaav.conf"
    if (perturber == "AAAC"):
        configFile = "response_aaac.conf"
        r = open("reorder_aaac.txt", 'w')
        for i in range(ncas):
            r.write("%d "%(reorder[i]+1+ncore))
        for i in range(ncore):
            r.write("%d "%(i+1))
        r.close()
    else:
        r = open("reorder_aaav.txt", 'w')
        for i in range(ncas):
            r.write("%d "%(reorder[i]+1))
        for i in range(norbs-ncore-ncas):
            r.write("%d "%(i+1+ncas))
        r.close()
    f = open(configFile, 'w')

    if (memory is not None):
        f.write('memory, %i, g\n'%(memory))
    ncoreE = 0
    if (perturber == "AAAC"):
        ncoreE = 2*ncore
    f.write('nelec %i\n'%(neleca+nelecb+ncoreE))
    f.write('spin %i\n' %(neleca-nelecb))
    if isinstance(DMRGCI.wfnsym, str):
        wfnsym = dmrg_sym.irrep_name2id(DMRGCI.mol.groupname, DMRGCI.wfnsym)
    else:
        wfnsym = DMRGCI.wfnsym
    f.write('irrep %i\n' % wfnsym)

    f.write('schedule\n')
    if (maxM <= DMRGCI.maxM):
        maxM = DMRGCI.maxM+1

    iter = 0
    for M in range(DMRGCI.maxM, maxM, 1000):
        f.write('%6i  %6i  %8.4e  %8.4e \n' %(iter*4, M, 1e-6, 1.0e-5))
        iter += 1

    f.write('%6i  %6i  %8.4e  %8.4e \n' %(iter*4, maxM, 1e-6, 1.0e-5))
    f.write('end\n')
    f.write('twodot \n')

    f.write('sym %s\n' % sym)

    integralFile = "FCIDUMP_aaav"
    if (perturber == "AAAC"):
        integralFile = "FCIDUMP_aaac"
    f.write('orbitals %s\n' % integralFile)

    f.write('maxiter %i\n'%(4*iter+4))
    f.write('sweep_tol %8.4e\n'%DMRGCI.tol)

    f.write('outputlevel %s\n'%DMRGCI.outputlevel)
    f.write('hf_occ integral\n')

    if(DMRGCI.scratchDirectory):
        f.write('prefix  %s\n'%DMRGCI.scratchDirectory)

    f.write('num_thrds %d\n'%DMRGCI.num_thrds)

    for line in extraline:
        f.write('%s\n'%line)

    f.write('occ %d\n'%(10000))
    if (perturber == "AAAC") :
        f.write("reorder reorder_aaac.txt\n")
        f.write("responseaaac\n")
        f.write("baseStates 0\n")
        f.write("projectorStates 0\n")
        f.write("targetState 2\n")
        f.write("partialsweep %d\n"%(ncas))
        f.write("open \n")
        f.write("closed ")
        for i in range(1,ncore+1):
            f.write("%d "%(i))
        f.write("\n")
    else:
        f.write("reorder reorder_aaav.txt\n")
        f.write("responseaaav\n")
        f.write("baseStates 0\n")
        f.write("projectorStates 0\n")
        f.write("targetState 2\n")
        f.write("partialsweep %d\n"%(ncas))
        f.write("open ")
        for i in range(ncas+1,norbs+1-ncore):
            f.write("%d "%(i))
        f.write("\nclosed \n")
    f.close()

def icmpspt(ncore, ncas, nelecas, sym, wfnsym, pttype="NEVPT", mpiprefix="mpiexec.hydra", numthrds=20, memory=10, rdmM=0, PTM=1000, PTincore=False, fciExtraLine=[], have3RDM=False, verbose=None):

    import dmrgcicopy
    dmrgci = dmrgcicopy.DMRGCI(rdmM, num_thrds=numthrds, memory= memory)
    dmrgci.wfnsym = wfnsym
    dmrgci.mpiprefix = mpiprefix
    dmrgci.startM = 100
    dmrgci.maxM = max(rdmM,501)
    dmrgci.restart = False

    if (not have3RDM):
        dmrgci.has_threepdm = False

        #we will redo the calculations so lets get ride of -1 states
        import os
        os.system("rm %s/node0/Rotation-*.state-1.tmp"%(dmrgci.scratchDirectory))
        os.system("rm %s/node0/wave-*.-1.tmp"%(dmrgci.scratchDirectory))
        os.system("rm %s/node0/RestartReorder.dat_1"%(dmrgci.scratchDirectory))
    else:
        dmrgci.has_threepdm = True

    dmrgci.generate_schedule()
    dmrgci.extraline = []
    if (PTincore):
        dmrgci.extraline.append('do_npdm_in_core')
    dmrgci.extraline += fciExtraLine

    #import pdb
    #pdb.set_trace()
    reorder = numpy.loadtxt("reorder.txt", dtype=int)-1
    norb, nelec, int2, int1, coreE, irrep = readIntegrals('FCIDUMP_full', reorder.tolist())

    nc = ncore+ncas
    int2.tofile("int/int2")
    int2 = numpy.zeros(1)
    print "wrote int2 to file"
    fname, fnameout1, fnameout2 = "int/int2", "int/int2popo", "int/int2ppoo"
    libint2unpack.unpackint2(ctypes.c_char_p(fname), ctypes.c_char_p(fnameout1), ctypes.c_char_p(fnameout2), ctypes.c_int(norb), ctypes.c_int(nc))
    print "after unpacking"
    int2popo = numpy.fromfile(fnameout1, dtype=numpy.dtype('Float64'))
    int2popo = numpy.reshape(int2popo, (norb, nc, norb, nc), order='F')
    int2ppoo = numpy.fromfile(fnameout2, dtype=numpy.dtype('Float64'))
    int2ppoo = numpy.reshape(int2ppoo, (norb, norb, nc, nc), order='F')

    h1eff = int1 + 2.0*numpy.einsum('mnii', int2ppoo[:,:, :ncore,:ncore]) - numpy.einsum('mini', int2popo[:,:ncore,:, :ncore])
    print "core E ", coreE + (2.0*numpy.einsum('iijj', int2ppoo[:ncore,:ncore,:ncore,:ncore])-numpy.einsum('ijij', int2popo[:ncore,:ncore,:ncore,:ncore]))+numpy.einsum('ii', int1[:ncore, :ncore])*2.0


    dmrgcicopy.writeIntegralFile(h1eff[ncore:ncore+ncas, ncore:ncore+ncas], int2popo[ncore:nc, ncore:nc, ncore:nc, ncore:nc].transpose(0,2,1,3), ncas, nelecas[0], nelecas[1], irrep[ncore:nc], dmrgci)


    writeInt2Numpy(ncore, ncas, nelec, int2ppoo, int2popo)


    int2ppoo, int2popo = numpy.zeros(1), numpy.zeros(1)
    dm3 = dmrgci.make_rdm3(state=0, norb=ncas, nelec=nelecas, wfnsym=wfnsym, molsym=sym)
    int2popo = numpy.fromfile(fnameout1, dtype=numpy.dtype('Float64'))
    int2popo = numpy.reshape(int2popo, (norb, nc, norb, nc), order='F')
    int2ppoo = numpy.fromfile(fnameout2, dtype=numpy.dtype('Float64'))
    int2ppoo = numpy.reshape(int2ppoo, (norb, norb, nc, nc), order='F')

    writeE3Numpy(dm3)
    nelec = nelecas[0]+nelecas[1]
    dm2 = numpy.einsum('ijklmk', dm3)/(nelec-2)
    dm1 = numpy.einsum('ijkj', dm2)/(nelec-1)
    dm3 = numpy.zeros(1)


    #backup the restartreorder file to -1. this is because responseaaav and responseaaac both overwrite this file
    #this means that when we want to restart a calculation after lets say responseaaav didnt finish, the new calculaitons
    #will use the restartreorder file that was written by the incomplete responseaaav run instead of the original dmrg run.
    reorderf1 = "%s/node0/RestartReorder.dat_1"%(dmrgci.scratchDirectory)
    reorderf = "%s/node0/RestartReorder.dat"%(dmrgci.scratchDirectory)
    import os.path
    reorder1present = os.path.isfile(reorderf1)
    if (reorder1present):
        from subprocess import check_call
        output = check_call("cp %s %s"%(reorderf1, reorderf), shell=True)
    else :
        from subprocess import check_call
        check_call("cp %s %s"%(reorderf, reorderf1), shell=True)
    reorder = numpy.loadtxt("%s/node0/RestartReorder.dat"%(dmrgci.scratchDirectory))


    if (pttype == "NEVPT") :
        #int2 = numpy.load("int/int2.npy")
        norbs, energyE0 = writeNevpt2Integrals(ncore, ncas, nelec, int2ppoo, int2popo, int1, h1eff, irrep, coreE, dm1, dm2, dm3)
        sys.stdout.flush()
        writeDMRGConfFile(nelecas[0], nelecas[1], ncore, ncas,  norbs, sym,
                          dmrgci, PTM, "AAAV", dmrgci.memory, dmrgci.num_thrds, reorder, fciExtraLine)
        writeDMRGConfFile(nelecas[0], nelecas[1], ncore, ncas,  norbs, sym,
                          dmrgci, PTM, "AAAC", dmrgci.memory, dmrgci.num_thrds, reorder, fciExtraLine)
        int2 = numpy.zeros(1)
        sys.stdout.flush()
        totalE = 0.0
        try:
            outfile, infile = "response_aaav.out", "response_aaav.conf"
            output = check_call("%s  %s  %s > %s"%(dmrgci.mpiprefix, dmrgci.executable, infile, outfile), shell=True)
            file1 = open("%s/node0/dmrg.e"%(dmrgci.scratchDirectory),"rb")
            import struct
            energy = struct.unpack('d', file1.read(8))[0]
            file1.close()
            totalE += energy
            print "perturer AAAV --  %18.9e"%(energy)
            sys.stdout.flush()

            if ncore != 0 :
                outfile, infile = "response_aaac.out", "response_aaac.conf"
                output = check_call("%s  %s  %s > %s"%(dmrgci.mpiprefix, dmrgci.executable, infile, outfile), shell=True)
                file1 = open("%s/node0/dmrg.e"%(dmrgci.scratchDirectory),"rb")
                energy = struct.unpack('d', file1.read(8))[0]
                file1.close()
                totalE += energy
                print "perturer AAAC --  %18.9e"%(energy)

        except ValueError:
            print(output)
            #exit()
        from subprocess import check_call
        totalE += executeNEVPT([nelecas[0],nelecas[1]], ncore, ncas, dmrgci.memory)# executeMRLCC(nelec, mc.ncore, mc.ncas)
        return totalE
    else :
        #this is a bad way to do it, the problem is
        #that pyscf works with double precision and
        #
        #energyE0 = writeMRLCCIntegrals(mc, dm1, dm2)
        #sys.stdout.flush()
        #int2 = numpy.load("int/int2.npy")
        energyE0, norbs = writeNumpyforMRLCC(ncore, ncas, nelec, int2ppoo, int2popo, int1, h1eff, irrep, coreE, dm1, dm2, dm3)
        int2popo = numpy.zeros(1)
        int2ppoo = numpy.zeros(1)
        sys.stdout.flush()
        writeDMRGConfFile(nelecas[0], nelecas[1], ncore, ncas,  norbs, sym,
                          dmrgci, PTM, "AAAV", dmrgci.memory, dmrgci.num_thrds, reorder, fciExtraLine)
        writeDMRGConfFile(nelecas[0], nelecas[1], ncore, ncas,  norbs, sym,
                          dmrgci, PTM, "AAAC", dmrgci.memory, dmrgci.num_thrds, reorder, fciExtraLine)

        int2 = numpy.zeros(1)
        from subprocess import check_call
        totalE = 0.0
        totalE +=  executeMRLCC([nelecas[0],nelecas[1]], ncore, ncas, dmrgci.memory)
        try:
            outfile, infile = "response_aaav.out", "response_aaav.conf"
            output = check_call("%s  %s  %s > %s"%(dmrgci.mpiprefix, dmrgci.executable, infile, outfile), shell=True)
            file1 = open("%s/node0/dmrg.e"%(dmrgci.scratchDirectory),"rb")
            import struct
            energy = struct.unpack('d', file1.read(8))[0]
            file1.close()
            totalE += energy
            print "perturer AAAV --  %18.9e"%(energy)

            if ncore != 0 :
                outfile, infile = "response_aaac.out", "response_aaac.conf"
                output = check_call("%s  %s  %s > %s"%(dmrgci.mpiprefix, dmrgci.executable, infile, outfile), shell=True)
                file1 = open("%s/node0/dmrg.e"%(dmrgci.scratchDirectory),"rb")
                energy = struct.unpack('d', file1.read(8))[0]
                file1.close()
                totalE += energy
                print "perturer AAAC --  %18.9e"%(energy)

        except ValueError:
            print(output)
            #exit()

        sys.stdout.flush()
        return totalE

