SINGLE_PRECISION = no
# sharma
LIBS_BASE= -L/home/sharma/apps/forServer/boost_1_53_0/stage/lib -lrt -lboost_program_options 
OMPFLAGS = -I/usr/local/server/IntelStudio_2015/mkl/include/  -I/home/sharma/apps/forServer/boost_1_53_0 -openmp -DUSEOMP -O3 -DNDEBUG -g
LIBS_LA  = -lmkl_intel_ilp64 -lmkl_gnu_thread -lmkl_core
CPP      = icpc
# blanca
LIBS_BASE= -L/projects/bamu3429/softwares/boost_1_53_0/stage/lib -lrt -lboost_program_options -lboost_serialization
INCLUDES = -I${MKLROOT}/include  -I/projects/bamu3429/softwares/boost_1_53_0
OMPFLAGS = ${INCLUDES} -openmp -DUSEOMP -O3 -DNDEBUG -g
LIBS_LA  = -L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core
CPP      = icpc -std=c++11 -w


SRC_FILES= main.cpp HandCodedEqs_woDF.cpp HandCodedEqs_wDF.cpp PerturberDependentCode.cpp BlockContract.cpp CxAlgebra.cpp CxIndentStream.cpp CxNumpyArray.cpp icpt.cpp CxMemoryStack.cpp CxStorageDevice.cpp TensorTranspose.cpp
OBJ_FILES= main.o   HandCodedEqs_woDF.o   HandCodedEqs_wDF.o   PerturberDependentCode.o   BlockContract.o   CxAlgebra.o   CxIndentStream.o   CxNumpyArray.o   icpt.o   CxMemoryStack.o   CxStorageDevice.o   TensorTranspose.o

ifeq ($(SINGLE_PRECISION), no)
	SRC_FILES += CxDiis.cpp
	OBJ_FILES += CxDiis.o
else
	OMPFLAGS += -D_SINGLE_PRECISION
endif

.cpp.o :
	$(CPP) $(OMPFLAGS) -c $< -o $@

icpt:	$(OBJ_FILES)
	$(CPP) $(OMPFLAGS) -o icpt $(OBJ_FILES) $(LIBS_BASE) $(LIBS_LA)

clean:
	rm *.o icpt
